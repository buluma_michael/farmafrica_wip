-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 19, 2017 at 03:28 PM
-- Server version: 5.6.35
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gbc_farmafrica`
--

-- --------------------------------------------------------

--
-- Table structure for table `counties`
--

CREATE TABLE `counties` (
  `id` int(11) NOT NULL,
  `county_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table storing all customers. Holds foreign keys to the address table and the store table where this customer is registered.\n\nBasic information about the customer like first and last name are stored in the table itself. Same for the date the record was created and when the information was last updated.' ROW_FORMAT=COMPACT;

--
-- Dumping data for table `counties`
--

INSERT INTO `counties` (`id`, `county_name`) VALUES
(1, 'MOMBASA'),
(2, 'KWALE'),
(3, 'KILIFI'),
(4, 'TANA RIVER'),
(5, 'LAMU'),
(6, 'TAITA TAVETA'),
(7, 'GARISSA'),
(8, 'WAJIR'),
(9, 'MANDERA'),
(10, 'MARSABIT'),
(11, 'ISIOLO'),
(12, 'MERU'),
(13, 'THARAKA-NITHI'),
(14, 'EMBU'),
(15, 'KITUI'),
(16, 'MACHAKOS'),
(17, 'MAKUENI'),
(18, 'NYANDARUA'),
(19, 'NYERI'),
(20, 'KIRINYAGA'),
(21, 'MURANG\'A'),
(22, 'KIAMBU'),
(23, 'TURKANA'),
(24, 'WEST POKOT'),
(25, 'SAMBURU'),
(26, 'TRANS NZOIA'),
(27, 'UASIN GISHU'),
(28, 'ELGEYO/MARAKWET'),
(29, 'NANDI'),
(30, 'BARINGO'),
(31, 'LAIKIPIA'),
(32, 'NAKURU'),
(33, 'NAROK'),
(34, 'KAJIADO'),
(35, 'KERICHO'),
(36, 'BOMET'),
(37, 'KAKAMEGA'),
(38, 'VIHIGA'),
(39, 'BUNGOMA'),
(40, 'BUSIA'),
(41, 'SIAYA'),
(42, 'KISUMU'),
(43, 'HOMA BAY'),
(44, 'MIGORI'),
(45, 'KISII'),
(46, 'NYAMIRA'),
(47, 'NAIROBI');

-- --------------------------------------------------------

--
-- Table structure for table `farmer_enterprise`
--

CREATE TABLE `farmer_enterprise` (
  `id` int(11) NOT NULL,
  `ent_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `farmer_enterprise`
--

INSERT INTO `farmer_enterprise` (`id`, `ent_name`) VALUES
(1, 'Farmer'),
(2, 'Trader'),
(3, 'Feed Supplier'),
(4, 'Hatchery'),
(5, 'Other Services');

-- --------------------------------------------------------

--
-- Table structure for table `farmer_finance_sources`
--

CREATE TABLE `farmer_finance_sources` (
  `id` int(11) NOT NULL,
  `source_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `farmer_finance_sources`
--

INSERT INTO `farmer_finance_sources` (`id`, `source_name`) VALUES
(1, 'County Government'),
(2, 'Donation'),
(3, 'Grant'),
(4, 'Own Savings'),
(5, 'Loan');

-- --------------------------------------------------------

--
-- Table structure for table `farmer_info`
--

CREATE TABLE `farmer_info` (
  `id` int(11) NOT NULL,
  `farmer_id` varchar(50) NOT NULL,
  `recruitment_no` varchar(50) DEFAULT NULL,
  `recruitment_src` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `reg_status` varchar(50) DEFAULT NULL,
  `date_enrolled` varchar(50) DEFAULT NULL,
  `pond_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `farmer_reg_type`
--

CREATE TABLE `farmer_reg_type` (
  `id` int(11) NOT NULL,
  `type` varchar(50) DEFAULT '0',
  `date_modified` varchar(50) DEFAULT '0',
  `modified_by` varchar(50) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `farmer_reg_type`
--

INSERT INTO `farmer_reg_type` (`id`, `type`, `date_modified`, `modified_by`) VALUES
(1, 'Not registered', '0', '0'),
(2, 'Registered', '0', '0'),
(3, 'LTD', '0', '0'),
(4, 'SHG', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `farmer_training`
--

CREATE TABLE `farmer_training` (
  `id` int(11) NOT NULL,
  `farmer_id` int(11) DEFAULT NULL,
  `training_date` date DEFAULT NULL,
  `training_module` int(11) DEFAULT NULL,
  `venue` varchar(45) DEFAULT NULL,
  `comments` varchar(45) DEFAULT NULL,
  `trainer` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `submitted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `farmer_training`
--

INSERT INTO `farmer_training` (`id`, `farmer_id`, `training_date`, `training_module`, `venue`, `comments`, `trainer`, `date_created`, `submitted_by`) VALUES
(1, 1, '2017-09-23', 1, 'Nairobi', 'none', 1, '2017-09-19 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `farms`
--

CREATE TABLE `farms` (
  `id` int(11) NOT NULL,
  `farmer_id` varchar(50) NOT NULL DEFAULT '0',
  `farm_identifier` varchar(50) DEFAULT NULL COMMENT 'Unique Farm Identifier provided by FarmAfrica',
  `farm_name` varchar(255) DEFAULT NULL COMMENT 'Name of Farm',
  `farm_county` varchar(255) DEFAULT NULL,
  `farm_subcounty` varchar(255) DEFAULT NULL,
  `farm_village` varchar(255) DEFAULT NULL,
  `farm_landmark` varchar(255) DEFAULT NULL,
  `keeps_records` varchar(50) NOT NULL DEFAULT '0' COMMENT '1 or 0 if farm keeps records or not',
  `farm_altitude` varchar(255) DEFAULT NULL,
  `farm_longitude` varchar(255) DEFAULT NULL,
  `farm_latitude` varchar(255) DEFAULT NULL,
  `location_name` varchar(255) DEFAULT NULL,
  `record_keeping` varchar(50) DEFAULT NULL COMMENT 'Manual or Computer',
  `records_kept` varchar(500) DEFAULT NULL COMMENT 'a coma separated array of record types',
  `has_business_plan` varchar(50) DEFAULT '0' COMMENT '1 or 0 if farm has business plan',
  `business_plan_last_update` varchar(50) DEFAULT NULL,
  `ponds_number` varchar(50) DEFAULT NULL,
  `ponds_stocked` varchar(50) DEFAULT NULL,
  `pond_sample_temperature` varchar(50) DEFAULT NULL COMMENT 'temperature of sample pond',
  `pond_sample_time` varchar(50) DEFAULT NULL COMMENT 'time of day sample pond was taken',
  `modified_by` varchar(50) DEFAULT NULL,
  `date_modified` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Farmer recruitment data main table';

--
-- Dumping data for table `farms`
--

INSERT INTO `farms` (`id`, `farmer_id`, `farm_identifier`, `farm_name`, `farm_county`, `farm_subcounty`, `farm_village`, `farm_landmark`, `keeps_records`, `farm_altitude`, `farm_longitude`, `farm_latitude`, `location_name`, `record_keeping`, `records_kept`, `has_business_plan`, `business_plan_last_update`, `ponds_number`, `ponds_stocked`, `pond_sample_temperature`, `pond_sample_time`, `modified_by`, `date_modified`) VALUES
(1, '0', '001', 'Farmer 1', '0', '0', '0', '0', '0', NULL, NULL, NULL, NULL, '0', 'Stocking Records,Feeding Records', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '0', '002', 'Farmer 2', '0', '0', '0', '0', '0', NULL, NULL, NULL, NULL, '0', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '0', '003', 'Farmer 3', '0', '0', '0', '0', '0', NULL, NULL, NULL, NULL, '0', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '0', '004', 'Farmer 4', '0', '0', '0', '0', '0', NULL, NULL, NULL, NULL, '0', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '0', '005', 'Farmer 5', '0', '0', '0', '0', '0', NULL, NULL, NULL, NULL, '0', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'K001', NULL, 'Mto wa mawe', 'Busia', 'Murumba', 'Bumala', 'river', '0', NULL, '34.4557', '23.345655', NULL, 'yes', 'book', 'yes', '2016-07-12', '4', '3', NULL, NULL, '3', NULL),
(7, 'K002', NULL, 'Kitutu chache', 'Kisii', 'Chache north', 'vile', 'rock', 'on', NULL, '44.444545', '15.56655', NULL, 'Manual', 'F', 'on', '2015-07-16', '5', '4', NULL, NULL, '1', NULL),
(8, 'K003', NULL, 'Mtito farm', 'Mombasa', 'Kwale', 'mwaanzooni', 'ocean', 'on', NULL, '23.454545', '12.222344', NULL, 'Select', 'Select', 'on', '', '7', '5', NULL, NULL, '7', NULL),
(9, '5', NULL, 'Mayaku', 'Machakos', 'Athi river', 'Kiito', 'river athi', 'No', NULL, '55.454545', '23.34343444', NULL, '', '', 'Yes', '2017-08-16', '2', '2', NULL, NULL, '1', NULL),
(12, 'T001', NULL, 'Orchard', 'Busia', 'Murumba', 'Bumala', 'river', 'Yes', '34.778874774', '34.45555555', '23.345655', NULL, 'Manually', 'Book', 'Yes', '2017-07-11', '7', '4', '23', 'Noon', '7', NULL),
(13, 'T002', NULL, 'Caveleninha', 'Kisii', 'Kitutu', 'mlolo', 'crying stone', 'Yes', '12.4567952', '34.4557782', '23.5678944', NULL, 'Manually', 'Bank', 'Yes', '2017-08-07', '4', '3', '34', 'Noon', '1', '2017-09-08 12:06:43'),
(15, 'T004', NULL, 'Kaunda', 'Kisumu', 'Homabay', 'Ndicho', 'Crying lake', 'Yes', '43.908754', '34.45555555', '23.34343444', NULL, 'Manually', 'Book and File', 'Yes', '', '4', '3', '45', 'Morning', '1', '2017-09-09 01:59:32'),
(16, 'F048', NULL, 'The Good', 'Kitui', 'Maritini', 'Loki', 'Dry rock', 'No', '2.5892345', '38.78892354', '23.5577880', NULL, 'Manually', 'Book', 'No', '2017-07-11', '6', '4', '40', 'Noon', '1', '2017-09-14 00:43:57'),
(17, 'F049', NULL, 'El Capitolero', 'Kericho', 'Kiptilel', 'Kasoul', 'Tea Farms', 'Yes', '4.678932', '31.78554321', '-1.67855432', NULL, 'Manually', 'Book and File', 'Yes', '2017-09-08', '6', '4', '45', 'Evening', '1', '2017-09-14 13:29:44'),
(19, 'N001', NULL, 'Mikindani', '3', '35', 'Jomvu', 'Ocean', 'No', NULL, '', '', NULL, 'Manually', 'Book', 'Yes', '2017-09-19', '4', '3', '', '', '1', '2017-09-19 09:25:57'),
(20, 'N200', NULL, 'Kiems', '1', '9', 'Mikindani', 'Ocean', 'Yes', NULL, NULL, NULL, NULL, 'Manually', 'Bank', 'Yes', '2017-09-13', '4', '3', NULL, NULL, '1', '2017-09-19 09:40:02');

-- --------------------------------------------------------

--
-- Table structure for table `farms_assessments`
--

CREATE TABLE `farms_assessments` (
  `id` int(11) NOT NULL,
  `farm_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Foreign key to the farms table',
  `assessor_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Foreign key to the assessors table',
  `assessor_observations` varchar(500) DEFAULT NULL COMMENT 'Observations from the assessor',
  `reason_farmer_qualifies` varchar(500) DEFAULT NULL COMMENT 'Why do you think farmer qualifies or not for KMAP',
  `assessor_remarks` varchar(255) DEFAULT NULL COMMENT 'Recommended, Non-recommended or Can be considered later'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `farms_assessments`
--

INSERT INTO `farms_assessments` (`id`, `farm_id`, `assessor_id`, `assessor_observations`, `reason_farmer_qualifies`, `assessor_remarks`) VALUES
(1, 1, 1, 'observation', '', 'remarks'),
(2, 2, 2, 'observation', '', 'remarks'),
(3, 3, 1, 'observation', '', 'remarks'),
(4, 4, 2, 'observation', '', 'remarks'),
(5, 5, 3, 'observation', '', 'remarks'),
(7, 16, 1, 'Impressive', 'Many', 'Keep up'),
(8, 17, 1, 'The farm looks well maintained', 'The farmer qualifies', 'Am pleased'),
(9, 19, 1, 'Yes', 'Good Farm', 'recommended'),
(10, 20, 1, 'Yes', 'Good farm', 'Consider for lator');

-- --------------------------------------------------------

--
-- Table structure for table `farms_assessors`
--

CREATE TABLE `farms_assessors` (
  `id` int(11) NOT NULL,
  `assessor_name` varchar(255) DEFAULT NULL COMMENT 'Name of assessor',
  `assessor_telephone` varchar(255) DEFAULT NULL,
  `assessor_gender` varchar(255) DEFAULT NULL,
  `assessor_email` varchar(255) DEFAULT NULL,
  `assessor_age` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `farms_assessors`
--

INSERT INTO `farms_assessors` (`id`, `assessor_name`, `assessor_telephone`, `assessor_gender`, `assessor_email`, `assessor_age`) VALUES
(1, 'Assessor 1', '', 'Female', '', ''),
(2, 'Assessor 2', '', 'Male', '', ''),
(3, 'Assessor 3', '', 'Male', '', ''),
(4, 'Assessor 4', '', 'Female', '', ''),
(5, 'Assessor 5', '', 'Female', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `farms_contact_persons`
--

CREATE TABLE `farms_contact_persons` (
  `id` int(11) NOT NULL,
  `farm_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Foreign key to the farms table',
  `contact_name` varchar(255) DEFAULT NULL COMMENT 'Name of the contact person',
  `contact_telephone` varchar(255) DEFAULT NULL COMMENT 'Phone Number',
  `contact_email` varchar(255) DEFAULT NULL COMMENT 'Eamil Address of Contact Person',
  `contact_gender` varchar(255) DEFAULT NULL COMMENT 'Gender',
  `contact_position` varchar(255) DEFAULT NULL COMMENT 'Position on Farm'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `farms_contact_persons`
--

INSERT INTO `farms_contact_persons` (`id`, `farm_id`, `contact_name`, `contact_telephone`, `contact_email`, `contact_gender`, `contact_position`) VALUES
(1, 1, 'Farmer 1 Contact Person', '', '', 'Female', 'Caretaker'),
(2, 2, 'Farmer 2 Contact Person', '', '', 'Male', 'Foreman'),
(3, 3, 'Farmer 3 Contact Person', '', '', 'Male', 'Storekeeper'),
(4, 15, '', '', NULL, '', ''),
(5, 15, 'James Achurovitz', '+25475896537', NULL, 'Male', 'Pond cleaner'),
(6, 15, 'Mark Masai', '+257895432986', NULL, 'Male', 'Feeder'),
(7, 15, 'Mary Wambui', '+254726601915', NULL, 'F', 'Accountant'),
(8, 15, 'Faith Njeri', '+254726485941', NULL, 'F', 'CEO'),
(9, 15, 'Duncan Khaemba', '+254678543235', NULL, 'M', 'Retailer'),
(10, 15, 'Nwanko Kanu', '+254678543235', NULL, 'M', 'Accountant'),
(11, 16, 'Zaccharia Musevi', '+254786543289', NULL, 'M', 'Cleaner'),
(12, 17, 'Jack Grisham', '+254678324589', NULL, 'M', 'Analyst'),
(13, 19, 'Peter Wilsons', '07457842', NULL, 'M', 'Cleaner'),
(14, 20, 'Bryan', '+254678543235', NULL, 'M', 'cleaner');

-- --------------------------------------------------------

--
-- Table structure for table `farms_extended_data`
--

CREATE TABLE `farms_extended_data` (
  `id` int(11) NOT NULL,
  `farm_id` int(11) NOT NULL DEFAULT '0',
  `water_availability` varchar(50) NOT NULL DEFAULT '0' COMMENT 'Permanent or Seasonal',
  `water_sources` varchar(500) DEFAULT NULL COMMENT 'comma separated array of water sources',
  `water_mechanism` varchar(255) DEFAULT NULL COMMENT 'Gravity Feed or Pumping',
  `farm_equipments` varchar(500) DEFAULT NULL COMMENT 'comma separated array of farm equipments',
  `farm_greenhouse` char(50) DEFAULT NULL COMMENT '1 or 0 if they farm fish in a greenhouse',
  `staff_total` varchar(255) DEFAULT NULL COMMENT 'total number of staff (more data in farms_staff table)',
  `staff_fulltime` varchar(255) DEFAULT NULL COMMENT 'full time staff number',
  `staff_parttime` varchar(255) DEFAULT NULL COMMENT 'part time staff number',
  `main_feeds` varchar(500) DEFAULT NULL,
  `has_security` varchar(50) DEFAULT NULL COMMENT '1 or 0 if the farm has security',
  `security_types` varchar(255) DEFAULT NULL COMMENT 'Guard or fencing, or both',
  `customers` varchar(500) DEFAULT NULL COMMENT 'comma separated array of cutomers',
  `customers_others` varchar(500) DEFAULT NULL,
  `challenges` varchar(500) DEFAULT NULL,
  `five_year_target` varchar(500) DEFAULT NULL,
  `needs_to_reach_target` varchar(500) DEFAULT NULL COMMENT 'Manual or Computer',
  `receive_updates` char(50) DEFAULT NULL COMMENT '1 or 0 if farmer wants to receive updates',
  `can_host_trainings` char(50) DEFAULT NULL COMMENT '1 or 0 if farm is willing to host trainings',
  `modified_by` char(50) DEFAULT NULL,
  `date_modified` char(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Farmer recruitment data main table' ROW_FORMAT=COMPACT;

--
-- Dumping data for table `farms_extended_data`
--

INSERT INTO `farms_extended_data` (`id`, `farm_id`, `water_availability`, `water_sources`, `water_mechanism`, `farm_equipments`, `farm_greenhouse`, `staff_total`, `staff_fulltime`, `staff_parttime`, `main_feeds`, `has_security`, `security_types`, `customers`, `customers_others`, `challenges`, `five_year_target`, `needs_to_reach_target`, `receive_updates`, `can_host_trainings`, `modified_by`, `date_modified`) VALUES
(1, 0, 'Permanent', 'Piped', 'Pumping', 'Seine Net', 'No', '60', '30', '30', '', 'Yes', 'Fencing', 'Hotels', 'Security force', 'Varying rain fall', 'Make millions', '0', 'No', 'No', '1', '2017-09-12 15:23:09'),
(2, 0, 'Permanent', 'Piped', 'Pumping', 'Seine Net', 'No', '60', '30', '30', '', 'Yes', 'Fencing', 'Hotels', 'Security force', 'Varying rain fall', 'Make millions', '0', 'No', 'No', '1', '2017-09-12 15:23:09'),
(3, 0, 'Permanent', 'Piped', 'Pumping', 'Seine Net', 'No', '60', '30', '30', '', 'Yes', 'Fencing', 'Hotels', 'Security force', 'Varying rain fall', 'Make millions', '0', 'No', 'No', '1', '2017-09-12 15:23:09'),
(4, 0, 'Permanent', 'Piped', 'Pumping', 'Seine Net', 'No', '60', '30', '30', '', 'Yes', 'Fencing', 'Hotels', 'Security force', 'Varying rain fall', 'Make millions', '0', 'No', 'No', '1', '2017-09-12 15:23:09'),
(5, 0, 'Permanent', 'Piped', 'Pumping', 'Seine Net', 'No', '60', '30', '30', '', 'Yes', 'Fencing', 'Hotels', 'Security force', 'Varying rain fall', 'Make millions', '0', 'No', 'No', '1', '2017-09-12 15:23:09'),
(6, 6, 'Permanent', 'Piped', 'Pumping', 'Seine Net', 'No', '60', '30', '30', '', 'Yes', 'Fencing', 'Hotels', 'Security force', 'Varying rain fall', 'Make millions', '', 'No', 'No', '1', '2017-09-12 15:23:09'),
(7, 13, 'Permanent', 'Ground water', 'Pumping', 'Weighing scale', NULL, '45', '12', '34', NULL, '0', 'Guard', 'Traders/Middlemen', 'gvt', 'rain', 'multi empire', 'money', '1', '1', NULL, NULL),
(8, 15, 'Seasonal', 'Piped', 'Pumping', 'Seine Net', 'No', '60', '30', '30', '', 'Yes', 'Fencing', 'Hotels', 'Security force', 'Varying rain fall', 'make millions', 'Money', 'No', 'No', '1', '2017-09-12 15:31:43'),
(9, 16, 'Seasonal', 'Ground water', 'Pumping', 'Seine Net', '', '45', '32', '13', '', 'Yes', 'Fencing', 'Hotels', 'Gvt', 'Many', 'Big', 'Support', 'No', 'Yes', '1', '2017-09-14 00:21:18'),
(10, 17, 'Seasonal', 'River/Stream', 'Pumping', 'Weighing scale', 'Yes', '50', '45', '5', NULL, 'Yes', 'Guard', 'Local Consumers', 'Schools', 'Lack of water', 'Billion certified', '1 billion dollars', 'Yes', 'No', NULL, NULL),
(11, 19, 'Permanent', 'Ground water', 'Pumping', 'Weighing scale', 'Yes', '16', '', '', '', 'Yes', 'Fencing', 'Traders/Middlemen', '', 'Many more', 'Big and large', 'Money more', 'No', 'Yes', '1', '2017-09-19 09:29:03'),
(12, 20, 'Permanent', 'Ground water', 'Gravity Fed', 'Weighing scale', 'Yes', '40', '', '', NULL, 'Yes', 'Guard', 'Local Consumers', '', 'Many', 'Big', 'Money', 'Yes', 'Yes', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `farms_owners`
--

CREATE TABLE `farms_owners` (
  `f_id` int(11) NOT NULL,
  `farmer_id` varchar(50) NOT NULL DEFAULT '0' COMMENT 'Foreign key to the farms table',
  `owner_name` varchar(255) DEFAULT '0' COMMENT 'Name of owner/investor',
  `owner_telephone` varchar(255) NOT NULL DEFAULT '0',
  `owner_gender` varchar(255) NOT NULL DEFAULT '0',
  `owner_email` varchar(255) NOT NULL DEFAULT '0',
  `registration` varchar(255) NOT NULL DEFAULT '0',
  `date_registered` varchar(255) NOT NULL DEFAULT '0',
  `date_enrolled` varchar(255) NOT NULL DEFAULT '0',
  `age` int(11) NOT NULL DEFAULT '0',
  `recruitment_source` int(11) NOT NULL DEFAULT '0',
  `type_of_enterprise` varchar(255) NOT NULL DEFAULT '0',
  `occupation` varchar(255) NOT NULL DEFAULT '0',
  `finance_source` varchar(255) NOT NULL DEFAULT '0',
  `modified_by` varchar(255) NOT NULL DEFAULT '0',
  `date_modified` varchar(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `farms_owners`
--

INSERT INTO `farms_owners` (`f_id`, `farmer_id`, `owner_name`, `owner_telephone`, `owner_gender`, `owner_email`, `registration`, `date_registered`, `date_enrolled`, `age`, `recruitment_source`, `type_of_enterprise`, `occupation`, `finance_source`, `modified_by`, `date_modified`) VALUES
(1, '1', 'Owner 1 Farm 1', '765378', 'Female', '0', '0', '0', '0', 0, 0, '0', '0', '0', '0', '0'),
(2, '1', 'Owner 2 Farm 1', '800009', 'Male', '0', '0', '0', '0', 0, 0, '0', '0', '0', '0', '0'),
(3, '3', 'Farmer 3', '678866', 'Male', '0', '0', '0', '0', 0, 0, '0', '0', '0', '0', '0'),
(4, '4', 'Farmer 4', '234546', 'Female', '0', '0', '0', '0', 0, 0, '0', '0', '0', '0', '0'),
(5, '5', 'Mike pence', '098766', 'Female', 'm@jcloud.com', '4', '2017-09-13', '2017-09-15', 50, 0, 'Farmer', 'Politician', 'Salary', '1', '2017-09-11 13:52:49'),
(7, 'K001', 'Wilson Mvule', '+254706658943', '', 'wil@co.com', '2', '0', '0', 40, 7, 'Farmer', '', 'Teacher', '1', '2017-08-29 13:43:02'),
(8, 'K002', 'NICK THE', '+254706658943', '', 'Nm@g.com', '3', '0', '0', 68, 3, 'Farmer', '', 'Soldier', '1', '2017-08-29 13:59:52'),
(15, 'F001', 'Jack Rudolf', '+254706658943', 'M', 'jack@gmail.com', '1', '0', '0', 54, 3, 'Farmer', 'LTD', 'Grant', '1', '2017-08-30 19:25:03'),
(16, 'T001', 'Mark Zuckerberger', '+256789033489', 'M', 'ma@g.com', '4', '0', '0', 33, 7, 'Farmer', 'LTD', 'Facebook', '1', '2017-08-31 00:21:14'),
(19, 'T002', 'Mwanaisha Chidzuga', '+254706658943', 'F', 'chidzu@ymail.com', '2', '0', '0', 34, 7, 'Farmer', 'Journalist', '', '1', '2017-09-08 11:55:03'),
(20, 'T003', 'mARK Kiilu', '+254706658943', 'M', 'jack@gmail.com', '1', '0', '0', 43, 7, 'Farmer', 'Journalist', '', '1', '2017-09-08 16:43:54'),
(21, 'T004', 'Rodrigo Palacio', '+256709033489', 'Male', 'rodrigo@frm.com', '2', '2017-09-10', '2017-10-28', 43, 3, 'Farmer', 'Doctor', 'County gvt', '1', '2017-09-11 13:54:04'),
(22, 'T007', 'John Kamau', '+254706658943', 'Male', 'rd@gmail.com', '2', '2017-09-11 15:07:55', '0', 77, 1, 'Agriculture', 'Bio Chemist', '', '1', '2017-09-11 15:07:55'),
(23, 'F048', 'Chris Luda', '+254678543123', 'Male', 'chris@l.com', '3', '2017-09-13 08:50:18', '0', 40, 1, 'Fish Farm', 'Musician', '', '1', '2017-09-13 08:50:18'),
(24, 'F049', 'Rodrigo Bentacur', '+254786543897', 'Male', 'benta@ymail.com', '3', '2017-09-14 13:27:15', '0', 22, 1, 'Farmer', 'Soccer player', '', '1', '2017-09-14 13:27:15'),
(45, 'FA001', 'Joshua Kimmich', '+254706658943', '1', 'joshua@kimmich.co.ke', '4', '2017-09-18 20:34:28', '0', 21, 1, '1', 'Bio Chemist', '[\"1\",\"2\",\"3\",\"4\",\"5\"]', '1', '2017-09-18 20:34:28'),
(46, 'N001', 'Jesus Peters', '0723457689', '2', 'jesus@y.com', '3', '2017-07-09', '2017-09-14', 24, 1, '5', 'Teacher', '[\"1\",\"2\"]', '1', '2017-09-19 09:24:42'),
(47, 'N200', 'Jackson Williams', '+254706658943', '1', 'jos@f.com', '2', '2017-08-09', '2017-09-07', 20, 1, '3', 'Bio Chemist', '[\"1\",\"3\",\"4\"]', '1', '2017-09-19 09:53:23');

-- --------------------------------------------------------

--
-- Table structure for table `farms_ponds`
--

CREATE TABLE `farms_ponds` (
  `id` int(11) NOT NULL,
  `farm_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Foreign key to the farms table',
  `pond_number` int(11) NOT NULL COMMENT 'eg Pond 1',
  `pond_area` varchar(255) DEFAULT NULL COMMENT 'in m2',
  `catfish_no` int(11) DEFAULT NULL COMMENT 'catfish fingerlings stocked',
  `tilapia_no` int(11) DEFAULT NULL COMMENT 'tilapia fingerlings stocked',
  `tilapia_fingerling_stocked` int(11) DEFAULT NULL,
  `tilapia_price_per_fingerling` decimal(10,0) DEFAULT NULL,
  `tilapia_fingerling_stocking_weight` decimal(10,0) DEFAULT NULL,
  `tilapia_stocking_date` varchar(50) DEFAULT NULL,
  `catfish_fingerling_stocked` int(11) DEFAULT NULL,
  `catfish_price_per_fingerling` decimal(10,0) DEFAULT NULL,
  `catfish_fingerling_stocking_weight` decimal(10,0) DEFAULT NULL,
  `catfish_stocking_date` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Pond information' ROW_FORMAT=COMPACT;

--
-- Dumping data for table `farms_ponds`
--

INSERT INTO `farms_ponds` (`id`, `farm_id`, `pond_number`, `pond_area`, `catfish_no`, `tilapia_no`, `tilapia_fingerling_stocked`, `tilapia_price_per_fingerling`, `tilapia_fingerling_stocking_weight`, `tilapia_stocking_date`, `catfish_fingerling_stocked`, `catfish_price_per_fingerling`, `catfish_fingerling_stocking_weight`, `catfish_stocking_date`) VALUES
(1, 1, 0, 'LTD', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 1, 0, 'SHG', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 3, 0, 'LTD', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 4, 0, 'Not Registered', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 5, 0, 'LTD', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 12, 1, '100', 500, 400, 600, '40', '7', '2016-08-10', 650, '55', '5', '2016-10-13'),
(7, 13, 1, '300', 400, 300, 660, '0', '0', '2017-08-08', 500, '0', '0', '2017-08-08'),
(8, 13, 45, '300', 500, 400, 600, '0', '0', '2017-09-13', 560, '0', '0', '2017-09-06'),
(9, 15, 1, '400', 400, 600, 800, '0', '0', '2017-03-07', 500, '0', '0', '2017-06-07'),
(11, 16, 2, '300', 450, 400, 600, '0', '0', '2017-08-04', 500, '0', '0', '2017-08-08'),
(12, 17, 1, '500', 500, 650, 700, '0', '0', '2017-08-08', 600, '0', '0', '2017-08-08'),
(13, 12, 2, '200', 300, 500, 700, '40', '7', '2016-08-10', 700, '55', '5', '2016-10-13'),
(14, 19, 1, '500', 150, 100, 126, '0', '0', '2017-09-07', 200, '0', '0', '2017-09-05'),
(15, 20, 2, '500', 300, 50, 66, '0', '0', '2017-08-10', 400, '0', '0', '2017-08-08');

-- --------------------------------------------------------

--
-- Table structure for table `farms_production_cycles`
--

CREATE TABLE `farms_production_cycles` (
  `id` int(11) NOT NULL,
  `farm_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Foreign key to the farms table',
  `year` varchar(255) DEFAULT NULL COMMENT 'Year Cycle Completed',
  `species` varchar(255) DEFAULT NULL COMMENT 'species',
  `length` varchar(255) NOT NULL DEFAULT '0' COMMENT 'length of cycle in months',
  `ponds_harvested_size` varchar(255) NOT NULL DEFAULT '0' COMMENT 'in m2',
  `fingerlings_bought` varchar(255) NOT NULL DEFAULT '0' COMMENT 'number of fingerlings bought in last cycle',
  `fingerlings_source` varchar(255) DEFAULT NULL,
  `feeds_bought` varchar(255) NOT NULL DEFAULT '0' COMMENT 'kg of feeds bought in last cycle',
  `feeds_source` varchar(255) DEFAULT NULL,
  `fish_harvested` varchar(255) NOT NULL DEFAULT '0' COMMENT 'kg of fish harvested in last production cycle'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `farms_production_cycles`
--

INSERT INTO `farms_production_cycles` (`id`, `farm_id`, `year`, `species`, `length`, `ponds_harvested_size`, `fingerlings_bought`, `fingerlings_source`, `feeds_bought`, `feeds_source`, `fish_harvested`) VALUES
(1, 0, 'Farmer 1', 'LTD', '0', 'Female', '0', NULL, '0', NULL, '0'),
(2, 0, 'Farmer 2', 'SHG', '0', 'Male', '0', NULL, '0', NULL, '0'),
(3, 0, 'Farmer 3', 'LTD', '0', 'Male', '0', NULL, '0', NULL, '0'),
(4, 0, 'Farmer 4', 'Not Registered', '0', 'Female', '0', NULL, '0', NULL, '0'),
(5, 0, 'Farmer 5', 'LTD', '0', 'Female', '0', NULL, '0', NULL, '0'),
(6, 16, '2015', 'zpat', '6', '500', '2000', 'Market', '5000', 'Market1', '10000'),
(7, 17, '2010', 'Tilapia', '5', '300', '1000', 'Market', '4000', 'Supermarket', '20000'),
(8, 19, '2012', 'Tilapia', '7', '300', '1000', 'market', '1000', 'market', '1000'),
(9, 20, '2011', 'tilapia', '7', '400', '3000', 'Market', '4000', 'Market1', '20000');

-- --------------------------------------------------------

--
-- Table structure for table `farms_staff`
--

CREATE TABLE `farms_staff` (
  `id` int(11) NOT NULL,
  `farm_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Foreign key to the farms table',
  `staff_name` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Name of the staff person',
  `staff_role` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Role',
  `staff_age` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Age',
  `staff_gender` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Gender'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `farms_staff`
--

INSERT INTO `farms_staff` (`id`, `farm_id`, `staff_name`, `staff_role`, `staff_age`, `staff_gender`) VALUES
(1, 0, 'Farmer 1', 'LTD', '0', 'Female'),
(2, 0, 'Farmer 2', 'SHG', '0', 'Male'),
(3, 0, 'Farmer 3', 'LTD', '0', 'Male'),
(4, 0, 'Farmer 4', 'Not Registered', '0', 'Female'),
(5, 0, 'Farmer 5', 'LTD', '0', 'Female'),
(6, 16, 'Chrispinus Aoul', 'Cleaner', '34', 'M'),
(7, 17, 'Nick Patrone', 'Investigator', '40', 'M'),
(8, 19, 'Nick williams', 'tank cleaner', '50', 'M'),
(9, 20, 'tim', 'cleaner', '57', 'M');

-- --------------------------------------------------------

--
-- Table structure for table `farm_costs`
--

CREATE TABLE `farm_costs` (
  `id` int(11) NOT NULL,
  `farm_id` int(11) DEFAULT NULL COMMENT 'costs incurred during production cycle ',
  `cost_type` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `date_created` varchar(45) DEFAULT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_date` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `farm_costs`
--

INSERT INTO `farm_costs` (`id`, `farm_id`, `cost_type`, `amount`, `date_created`, `year`, `month`, `modified_by`, `modified_date`) VALUES
(1, 1, 1, 1000, '2017', 2016, 6, 0, '2017'),
(2, 1, 1, 1002, '2016', 2017, 5, 0, '2016');

-- --------------------------------------------------------

--
-- Table structure for table `farm_sales`
--

CREATE TABLE `farm_sales` (
  `id` int(11) NOT NULL,
  `farm_id` int(11) DEFAULT NULL,
  `whole_fish_sold` int(11) DEFAULT NULL,
  `whole_fish_avg_price_kg` int(11) DEFAULT NULL,
  `value_added_fish_sold` int(11) DEFAULT NULL,
  `value_added_avg_price_kg` int(11) DEFAULT NULL,
  `fish_type` int(11) DEFAULT NULL,
  `date_created` date NOT NULL,
  `modified_by` int(11) NOT NULL,
  `date_modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `farm_sales`
--

INSERT INTO `farm_sales` (`id`, `farm_id`, `whole_fish_sold`, `whole_fish_avg_price_kg`, `value_added_fish_sold`, `value_added_avg_price_kg`, `fish_type`, `date_created`, `modified_by`, `date_modified`) VALUES
(1, 1, 12, 122, 23, 23, 1, '0000-00-00', 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `gender_type` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `gender_type`) VALUES
(1, 'Male'),
(2, 'Female');

-- --------------------------------------------------------

--
-- Table structure for table `harvest_information`
--

CREATE TABLE `harvest_information` (
  `id` int(11) NOT NULL,
  `havest_since_last_visit` int(11) DEFAULT NULL,
  `farm_id` int(11) DEFAULT NULL,
  `pond_number` int(11) DEFAULT NULL,
  `harvest_date` date DEFAULT NULL,
  `harvest_type` int(11) DEFAULT NULL,
  `pieces_harvested` int(11) DEFAULT NULL,
  `avg_weight_piece` varchar(45) DEFAULT NULL COMMENT 'Kgs of feed fed (from stocking to last sample date)',
  `total_weight_kg` int(11) DEFAULT NULL COMMENT 'FCR (weight/ feed)',
  `feed_type` int(11) DEFAULT NULL,
  `production_cycle` varchar(45) DEFAULT NULL,
  `fcr` varchar(45) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `harvest_information`
--

INSERT INTO `harvest_information` (`id`, `havest_since_last_visit`, `farm_id`, `pond_number`, `harvest_date`, `harvest_type`, `pieces_harvested`, `avg_weight_piece`, `total_weight_kg`, `feed_type`, `production_cycle`, `fcr`, `date_created`, `modified_by`, `modified_date`) VALUES
(1, 123, 1, 1, '2017-09-03', 1, 2, '23', 33, 2, 'none', 'FCR', '2017-09-27 00:00:00', 2, '2017-09-20');

-- --------------------------------------------------------

--
-- Table structure for table `sampling_feeding`
--

CREATE TABLE `sampling_feeding` (
  `id` int(11) NOT NULL,
  `pond_number` varchar(45) DEFAULT NULL,
  `remaining_pieces` varchar(45) DEFAULT NULL,
  `last_sample_date` date DEFAULT NULL,
  `sample_weight` int(11) DEFAULT NULL,
  `main_feed_type` int(11) DEFAULT NULL,
  `kgs_feed` varchar(45) DEFAULT NULL COMMENT 'Kgs of feed fed (from stocking to last sample date)',
  `fcr` int(11) DEFAULT NULL COMMENT 'FCR (weight/ feed)',
  `date_created` date NOT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sampling_feeding`
--

INSERT INTO `sampling_feeding` (`id`, `pond_number`, `remaining_pieces`, `last_sample_date`, `sample_weight`, `main_feed_type`, `kgs_feed`, `fcr`, `date_created`, `modified_by`, `modified_date`) VALUES
(1, '1', '12', '2017-09-19', 12, 12, '2', 22, '2017-09-03', 2, '2017-09-19');

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `session_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `guest` tinyint(4) UNSIGNED DEFAULT '1',
  `time` varchar(14) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `data` mediumtext COLLATE utf8mb4_unicode_ci,
  `userid` int(11) DEFAULT '0',
  `username` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`session_id`, `client_id`, `guest`, `time`, `data`, `userid`, `username`) VALUES
('0vd34pquvkori8bjsmej5tbfo2', 0, 1, '1500375654', NULL, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `subcounties`
--

CREATE TABLE `subcounties` (
  `id` int(11) NOT NULL,
  `county_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sub_counties`
--

CREATE TABLE `sub_counties` (
  `id` int(20) NOT NULL,
  `county_id` int(20) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `sub_counties`
--

INSERT INTO `sub_counties` (`id`, `county_id`, `name`) VALUES
(1, 1, 'jomvu'),
(2, 1, 'changamwe'),
(3, 1, 'kisauni'),
(4, 1, 'nyali'),
(5, 1, 'likoni'),
(6, 1, 'mvita'),
(7, 2, 'msambweni'),
(8, 2, 'lungalunga'),
(9, 2, 'matuga'),
(10, 2, 'kinango'),
(11, 3, 'kilifi north'),
(12, 3, 'kilifi south'),
(13, 3, 'kaloleni'),
(14, 3, 'rabai'),
(15, 3, 'ganze'),
(16, 3, 'malindi'),
(17, 3, 'magarini'),
(18, 4, 'garsen'),
(19, 4, 'galole'),
(20, 4, 'bura'),
(21, 5, 'lamu east'),
(22, 5, 'lamu west'),
(23, 6, 'taveta'),
(24, 6, 'wundanyi'),
(25, 6, 'mwatate'),
(26, 6, 'voi'),
(27, 7, 'garissa township'),
(28, 7, 'balambala'),
(29, 7, 'lagdera'),
(30, 7, 'dadaab'),
(31, 7, 'fafi'),
(32, 7, 'ijara'),
(33, 8, 'wajir north'),
(34, 8, 'wajir east'),
(35, 8, 'tarbaj'),
(36, 8, 'wajir west'),
(37, 8, 'eldas'),
(38, 8, 'wajir south'),
(39, 9, 'mandera west'),
(40, 9, 'banissa'),
(41, 9, 'mandera north'),
(42, 9, 'mandera south'),
(43, 9, 'mandera east'),
(44, 9, 'lafey'),
(45, 10, 'moyale'),
(46, 10, 'north horr'),
(47, 10, 'saku'),
(48, 10, 'laisamis'),
(49, 11, 'isiolo north'),
(50, 11, 'isiolo south'),
(51, 12, 'igembe south'),
(52, 12, 'igembe central'),
(53, 12, 'igembe north'),
(54, 12, 'tigania west'),
(55, 12, 'tigania east'),
(56, 12, 'north imenti'),
(57, 12, 'buuri'),
(58, 12, 'central imenti'),
(59, 12, 'south imenti'),
(60, 13, 'maara'),
(61, 13, 'chuka/igambang\'om'),
(62, 13, 'tharaka'),
(63, 14, 'manyatta'),
(64, 14, 'runyenjes'),
(65, 14, 'mbeere south'),
(66, 14, 'mbeere north'),
(67, 15, 'mwingi north'),
(68, 15, 'mwingi west'),
(69, 15, 'mwingi central'),
(70, 15, 'kitui west'),
(71, 15, 'kitui rural'),
(72, 15, 'kitui central'),
(73, 15, 'kitui east'),
(74, 15, 'kitui south'),
(75, 16, 'masinga'),
(76, 16, 'yatta'),
(77, 16, 'kangundo'),
(78, 16, 'matungulu'),
(79, 16, 'kathiani'),
(80, 16, 'mavoko'),
(81, 16, 'machakos town'),
(82, 16, 'mwala'),
(83, 17, 'mbooni'),
(84, 17, 'kilome'),
(85, 17, 'kaiti'),
(86, 17, 'makueni'),
(87, 17, 'kibwezi west'),
(88, 17, 'kibwezi east'),
(89, 18, 'kinangop'),
(90, 18, 'kipipiri'),
(91, 18, 'ol kalou'),
(92, 18, 'ol jorok'),
(93, 18, 'ndaragwa'),
(94, 19, 'tetu'),
(95, 19, 'kieni'),
(96, 19, 'mathira'),
(97, 19, 'othaya'),
(98, 19, 'mukurweini'),
(99, 19, 'nyeri town'),
(100, 20, 'mwea'),
(101, 20, 'gichugu'),
(102, 20, 'ndia'),
(103, 20, 'kirinyaga central'),
(104, 21, 'kangema'),
(105, 21, 'mathioya'),
(106, 21, 'kiharu'),
(107, 21, 'kigumo'),
(108, 21, 'maragwa'),
(109, 21, 'kandara'),
(110, 21, 'gatanga'),
(111, 22, 'gatundu south'),
(112, 22, 'gatundu north'),
(113, 22, 'juja'),
(114, 22, 'thika town'),
(115, 22, 'ruiru'),
(116, 22, 'githunguri'),
(117, 22, 'kiambu'),
(118, 22, 'kiambaa'),
(119, 22, 'kabete'),
(120, 22, 'kikuyu'),
(121, 22, 'limuru'),
(122, 22, 'lari'),
(123, 23, 'turkana north'),
(124, 23, 'turkana west'),
(125, 23, 'turkana central'),
(126, 23, 'loima'),
(127, 23, 'turkana south'),
(128, 23, 'turkana east'),
(129, 24, 'kapenguria'),
(130, 24, 'sigor'),
(131, 24, 'kacheliba'),
(132, 24, 'pokot south'),
(133, 25, 'samburu west'),
(134, 25, 'samburu north'),
(135, 25, 'samburu east'),
(136, 26, 'kwanza'),
(137, 26, 'endebess'),
(138, 26, 'saboti'),
(139, 26, 'kiminini'),
(140, 26, 'cherangany'),
(141, 27, 'soy'),
(142, 27, 'turbo'),
(143, 27, 'moiben'),
(144, 27, 'ainabkoi'),
(145, 27, 'kapseret'),
(146, 27, 'kesses'),
(147, 28, 'marakwet east'),
(148, 28, 'marakwet west'),
(149, 28, 'keiyo north'),
(150, 28, 'keiyo south'),
(151, 29, 'tinderet'),
(152, 29, 'aldai'),
(153, 29, 'nandi hills'),
(154, 29, 'chesumei'),
(155, 29, 'emgwen'),
(156, 29, 'mosop'),
(157, 30, 'tiaty'),
(158, 30, 'baringo  north'),
(159, 30, 'baringo central'),
(160, 30, 'baringo south'),
(161, 30, 'mogotio'),
(162, 30, 'eldama ravine'),
(163, 31, 'laikipia west'),
(164, 31, 'laikipia east'),
(165, 31, 'laikipia north'),
(166, 32, 'molo'),
(167, 32, 'njoro'),
(168, 32, 'naivasha'),
(169, 32, 'gilgil'),
(170, 32, 'kuresoi south'),
(171, 32, 'kuresoi north'),
(172, 32, 'subukia'),
(173, 32, 'rongai'),
(174, 32, 'bahati'),
(175, 32, 'nakuru town west'),
(176, 32, 'nakuru town east'),
(177, 33, 'kilgoris'),
(178, 33, 'emurua dikirr'),
(179, 33, 'narok north'),
(180, 33, 'narok east'),
(181, 33, 'narok south'),
(182, 33, 'narok west'),
(183, 34, 'kajiado north'),
(184, 34, 'kajiado central'),
(185, 34, 'kajiado east'),
(186, 34, 'kajiado west'),
(187, 34, 'kajiado south'),
(188, 35, 'kipkelion east'),
(189, 35, 'kipkelion west'),
(190, 35, 'ainamoi'),
(191, 35, 'bureti'),
(192, 35, 'belgut'),
(193, 35, 'sigowet/soin'),
(194, 36, 'sotik'),
(195, 36, 'chepalungu'),
(196, 36, 'bomet east'),
(197, 36, 'bomet central'),
(198, 36, 'konoin'),
(199, 37, 'lugari'),
(200, 37, 'likuyani'),
(201, 37, 'malava'),
(202, 37, 'lurambi'),
(203, 37, 'navakholo'),
(204, 37, 'mumias west'),
(205, 37, 'mumias east'),
(206, 37, 'matungu'),
(207, 37, 'butere'),
(208, 37, 'khwisero'),
(209, 37, 'shinyalu'),
(210, 37, 'ikolomani'),
(211, 38, 'vihiga'),
(212, 38, 'sabatia'),
(213, 38, 'hamisi'),
(214, 38, 'luanda'),
(215, 38, 'emuhaya'),
(216, 39, 'mt.elgon'),
(217, 39, 'sirisia'),
(218, 39, 'kabuchai'),
(219, 39, 'bumula'),
(220, 39, 'kanduyi'),
(221, 39, 'webuye east'),
(222, 39, 'webuye west'),
(223, 39, 'kimilili'),
(224, 39, 'tongaren'),
(225, 40, 'teso north'),
(226, 40, 'teso south'),
(227, 40, 'nambale'),
(228, 40, 'matayos'),
(229, 40, 'butula'),
(230, 40, 'funyula'),
(231, 40, 'budalangi'),
(232, 41, 'ugenya'),
(233, 41, 'ugunja'),
(234, 41, 'alego usonga'),
(235, 41, 'gem'),
(236, 41, 'bondo'),
(237, 41, 'rarieda'),
(238, 42, 'kisumu east'),
(239, 42, 'kisumu west'),
(240, 42, 'kisumu central'),
(241, 42, 'seme'),
(242, 42, 'nyando'),
(243, 42, 'muhoroni'),
(244, 42, 'nyakach'),
(245, 43, 'kasipul'),
(246, 43, 'kabondo kasipul'),
(247, 43, 'karachuonyo'),
(248, 43, 'rangwe'),
(249, 43, 'homa bay town'),
(250, 43, 'ndhiwa'),
(251, 43, 'mbita'),
(252, 43, 'suba'),
(253, 44, 'rongo'),
(254, 44, 'awendo'),
(255, 44, 'suna east'),
(256, 44, 'suna west'),
(257, 44, 'uriri'),
(258, 44, 'nyatike'),
(259, 44, 'kuria west'),
(260, 44, 'kuria east'),
(261, 45, 'bonchari'),
(262, 45, 'south mugirango'),
(263, 45, 'bomachoge borabu'),
(264, 45, 'bobasi'),
(265, 45, 'bomachoge chache'),
(266, 45, 'nyaribari masaba'),
(267, 45, 'nyaribari chache'),
(268, 45, 'kitutu chache north'),
(269, 45, 'kitutu chache south'),
(270, 46, 'kitutu masaba'),
(271, 46, 'west mugirango'),
(272, 46, 'north mugirango'),
(273, 46, 'borabu'),
(274, 47, 'westlands'),
(275, 47, 'dagoretti north'),
(276, 47, 'dagoretti south'),
(277, 47, 'langata'),
(278, 47, 'kibra'),
(279, 47, 'roysambu'),
(280, 47, 'kasarani'),
(281, 47, 'ruaraka'),
(282, 47, 'embakasi south'),
(283, 47, 'embakasi north'),
(284, 47, 'embakasi central'),
(285, 47, 'embakasi east'),
(286, 47, 'embakasi west'),
(287, 47, 'makadara'),
(288, 47, 'kamukunji'),
(289, 47, 'starehe'),
(290, 47, 'mathare');

-- --------------------------------------------------------

--
-- Table structure for table `training_modules`
--

CREATE TABLE `training_modules` (
  `id` int(11) NOT NULL,
  `training_module` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `training_modules`
--

INSERT INTO `training_modules` (`id`, `training_module`) VALUES
(1, 'Module 1');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `user_role` int(11) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(1000) DEFAULT NULL,
  `date_registered` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `user_role`, `email`, `password`, `date_registered`) VALUES
(1, 'Steve Muganda', 1, 'smuganda@gbc.co.ke', 'a61f27ab2165df0e18cc9433bd7f27c5', '2017-08-26'),
(3, 'Caleb', 2, 'cmuganda@gbc.co.ke', '$2y$10$szl0DX.Nkbe0//mRWHCBsuG3m0a82wp/15FxtxDUaHJeIR6aqiT.e', '2017-08-26'),
(7, 'MIke Mark', 2, 'mark@g.com', '4914516fe31ee3ada94d2bc3d6e0fa41', '2017-08-26'),
(8, 'James Mwangi', 3, 'mbuluma@gbc.co.ke', 'f0c56cde806e671e9bbcbdf7f327e14f', '2017-09-19');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `usr_role_id` int(11) NOT NULL,
  `usr_role_title` varchar(50) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`usr_role_id`, `usr_role_title`) VALUES
(1, 'Super User'),
(2, 'Administrator'),
(3, 'Field Agent'),
(4, 'Communication manager');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `counties`
--
ALTER TABLE `counties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `farmer_enterprise`
--
ALTER TABLE `farmer_enterprise`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `farmer_finance_sources`
--
ALTER TABLE `farmer_finance_sources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `farmer_info`
--
ALTER TABLE `farmer_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `farmer_id` (`farmer_id`);

--
-- Indexes for table `farmer_reg_type`
--
ALTER TABLE `farmer_reg_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `farmer_training`
--
ALTER TABLE `farmer_training`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `farms`
--
ALTER TABLE `farms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `farms_assessments`
--
ALTER TABLE `farms_assessments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `farms_assessors`
--
ALTER TABLE `farms_assessors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `farms_contact_persons`
--
ALTER TABLE `farms_contact_persons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `farms_extended_data`
--
ALTER TABLE `farms_extended_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `farms_owners`
--
ALTER TABLE `farms_owners`
  ADD PRIMARY KEY (`f_id`);

--
-- Indexes for table `farms_ponds`
--
ALTER TABLE `farms_ponds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `farms_production_cycles`
--
ALTER TABLE `farms_production_cycles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `farms_staff`
--
ALTER TABLE `farms_staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `farm_costs`
--
ALTER TABLE `farm_costs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `farm_sales`
--
ALTER TABLE `farm_sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `harvest_information`
--
ALTER TABLE `harvest_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sampling_feeding`
--
ALTER TABLE `sampling_feeding`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `time` (`time`);

--
-- Indexes for table `subcounties`
--
ALTER TABLE `subcounties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_counties`
--
ALTER TABLE `sub_counties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `training_modules`
--
ALTER TABLE `training_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`usr_role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `counties`
--
ALTER TABLE `counties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `farmer_enterprise`
--
ALTER TABLE `farmer_enterprise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `farmer_finance_sources`
--
ALTER TABLE `farmer_finance_sources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `farmer_info`
--
ALTER TABLE `farmer_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `farmer_reg_type`
--
ALTER TABLE `farmer_reg_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `farms`
--
ALTER TABLE `farms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `farms_assessments`
--
ALTER TABLE `farms_assessments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `farms_assessors`
--
ALTER TABLE `farms_assessors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `farms_contact_persons`
--
ALTER TABLE `farms_contact_persons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `farms_extended_data`
--
ALTER TABLE `farms_extended_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `farms_owners`
--
ALTER TABLE `farms_owners`
  MODIFY `f_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `farms_ponds`
--
ALTER TABLE `farms_ponds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `farms_production_cycles`
--
ALTER TABLE `farms_production_cycles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `farms_staff`
--
ALTER TABLE `farms_staff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `subcounties`
--
ALTER TABLE `subcounties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sub_counties`
--
ALTER TABLE `sub_counties`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=291;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `usr_role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
