<?php
if (!defined('WEB_ROOT')) {
  exit;
}

$errorMessage = (isset($_GET['error']) && $_GET['error'] != '') ? $_GET['error'] : '&nbsp;';

$sql = "SELECT usr_role_id, usr_role_title FROM user_roles";
$result = dbQuery($sql);

?> 
<h2 class="catHead">Add user</h2>
<div class="prepend-1 span-12">
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-md-9">
<table class="table table-striped table-bordered">
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>user/processUser.php?action=add" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
  <div class="form-group row" >
  <label for="usr_role_id" class="col-2 col-form-label">Role</label>
  <select name="usr_role_id">
  <?php
  while($row = dbFetchAssoc($result)) {
    extract($row);
  ?>
  <option value="<?php echo $usr_role_id; ?>"><?php echo $usr_role_title; ?></option>
  <?php
  }
  ?>
  </select>
</div>

<div class="form-group row">
  <label for="txtFname" class="col-2 col-form-label">First Name</label>
  <div class="col-10">
    <input class="form-control" name="txtFname" type="text" id="txtFname" value="" required >
  </div>
</div>
<div class="form-group row">
  <label for="txtLname" class="col-2 col-form-label">Last Name</label>
  <div class="col-10">
    <input class="form-control" name="txtLname" type="text" id="txtLname" value="" required >
  </div>
</div>
<div class="form-group row" >
  <label for="txtEmail" class="col-2 col-form-label">Email</label>
  <div class="col-10">
    <input class="form-control" name="txtEmail" type="email" id="txtEmail" value="" required="" >
  </div>
</div>
  
 
 <p align="center"> 
  <input name="btnAddUser" type="button"   class="button" id="btnAddUser" value="Save (✔)" onClick="checkAddUserForm()" class="box">
  &nbsp;&nbsp;<input name="btnCancel" type="button" id="btnCancel" class="button"  value="Cancel" onClick="window.location.href='view.php?v=USER';" class="box">  
 </p>
</form>
 </tbody>
</table>
</div>


</div>