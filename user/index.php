<?php
require_once '../library/config.php';
require_once '../library/functions.php';

$_SESSION['login_return_url'] = $_SERVER['REQUEST_URI'];
checkUser();

$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';

switch ($view) {
	case 'list' :
		$content 	= 'list.php';		
		$pageTitle 	= 'Farm Africa  - View Users';
		break;

	case 'add' :
		$content 	= 'add.php';		
		$pageTitle 	= 'Farm Africa - Add Users';
		break;

	case 'edit' :
		$content 	= 'edit.php';		
		$pageTitle 	= 'Farm Africa - Edit User';
		break;

	default :
		$content 	= 'list.php';		
		$pageTitle 	= 'Farm Africa - View Users';
}

$script    = array('user.js');

require_once 'template.php';
?>
