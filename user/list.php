<?php
if (!defined('WEB_ROOT')) {
	exit;
}

$sql = "SELECT u.id, u.user_role, u.email, u.name,u.date_registered, d.usr_role_title, d.usr_role_id 
        FROM users u,user_roles d WHERE u.user_role = d.usr_role_id ORDER BY name";
$result = dbQuery($sql);

?> 
<div class="prepend-1 span-17">
<p>&nbsp;</p>
<h2 class="catHead">User Management</h2>

<form action="processUser.php?action=addUser" method="post"  name="frmListUser" id="frmListUser">
<div class="col-md-12">
<div class="table-responsive">
<table class="table table-striped table-bordered">
<thead>
  <tr>
    
   <td>Full Name</td>
   <td>E-mail</td>
   <td>Role</td>
   <td>Date registered</td>
   <td>Delete&nbsp;/&nbsp;Edit</td>
  
  </tr>
</thead>
<tbody>
<!--  <table  border="0" align="center" cellpadding="2" cellspacing="1" class="text"> -->
  
<?php
while($row = dbFetchAssoc($result)) {
	extract($row);
	
	if ($i%2) {
		$class = 'row1';
	} else {
		$class = 'row2';
	}
	
	$i += 1;

   
?>
  <tr class="<?php echo $class; ?>"> 
  <td ><?php echo ucfirst($name); ?></td>
  <td ><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></td>
  <td ><?php echo $usr_role_title; ?></td>
  <td ><?php echo $date_registered; ?></td>
   <!-- <td><?php echo ucfirst($uname); ?></td> -->
   <!-- <?php
   echo $id;
   ?> -->
   
   
   <td ><a  style="font-weight:normal;" href="javascript:deleteUser(<?php echo $id; ?>);">Delete</a>/<a  style="font-weight:normal;" href="javascript:editUser(<?php echo $id; ?>);">Edit</a></td>
  </tr>
<?php
} // end while

?>
  <tr> 
   <td colspan="12">&nbsp;</td>
  </tr>
  <tr> 
   <td colspan="12" align="right"><input name="btnAddUser" type="button" id="btnAddUser" value="Add New User"  class="button" onClick="addUser()"></td>
  </tr>
 </tbody>
</table>
</div>
</div>
</form>
</div>