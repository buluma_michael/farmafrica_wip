<h1>Edit Farmer</h1>

<hr>
<?php

//$item = $data['dataset']['items'][0];
//$item = (array)$item;
//$labels = $data['dataset']['labels'];
//$item_fields = array_keys($item);
//$item_values = array_values($item);
//echo '<pre>';
//print_r($data);
//print_r($item_fields);
//print_r($item_values);
//echo '</pre>';

?>
<div class="col-md-12" id="messagebox">
	<?php
		if(!empty($data['messages'])){
			$messages = $data['messages'];
			foreach ($messages as $key => $message) {
				echo '<div class="alert alert-'.$message['type'].'" role="alert">'.$message['text'].' </div>';
			}

		}
	?>
</div>
<div class="col-md-12">

	<div>
  		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
		    <li role="presentation" class="active"><a href="#basic" aria-controls="basic" role="tab" data-toggle="tab">Farm Basic Info</a></li>
		    <li role="presentation"><a href="#extended" aria-controls="extended" role="tab" data-toggle="tab">Farm Extended Info</a></li>
		    <li role="presentation"><a href="#owners" aria-controls="owners" role="tab" data-toggle="tab">Farm Owners</a></li>
		    <li role="presentation"><a href="#contact_persons" aria-controls="contact_info" role="tab" data-toggle="tab">Contact Persons</a></li>
		    <li role="presentation"><a href="#pond_info" aria-controls="pond_info" role="tab" data-toggle="tab">Pond Information</a></li>
		    <li role="presentation"><a href="#staff" aria-controls="staff" role="tab" data-toggle="tab">Farm Staff</a></li>
		    <li role="presentation"><a href="#production" aria-controls="production" role="tab" data-toggle="tab">Farm Production Cycles</a></li>
		    <li role="presentation"><a href="#assessments" aria-controls="assessments" role="tab" data-toggle="tab">Farm Assessments</a></li>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">
		    <div role="tabpanel" class="tab-pane active" id="basic"><?php echo $data['edit_farms']; ?></div>
		    <div role="tabpanel" class="tab-pane" id="extended"><?php echo $data['edit_extended_data']; ?></div>
		    <div role="tabpanel" class="tab-pane" id="owners"><?php echo $data['edit_owners']; ?></div>
		    <div role="tabpanel" class="tab-pane" id="contact_persons"><?php echo $data['edit_contact_persons']; ?></div>
		    <div role="tabpanel" class="tab-pane" id="pond_info"><?php echo $data['edit_ponds']; ?></div>
		    <div role="tabpanel" class="tab-pane" id="staff"><?php echo $data['edit_staff']; ?></div>
		    <div role="tabpanel" class="tab-pane" id="production"><?php echo $data['edit_production_cycles']; ?></div>
		    <div role="tabpanel" class="tab-pane" id="assessments"><?php echo $data['edit_assessments']; ?></div>
		</div>
	</div>
</div>

<!--<div class="col-md-6">
<form method="post" action="index.php?view=farmers&action=edit&id=<?php echo $item['id']; ?>">
	<div class="col-md-12">
		<a class="btn button btn-small btn-default" href="index.php?view=farmers&action=view&id=<?php echo $item['id']; ?>"> Back </a>
		<button type="submit" class="btn btn-primary">Save</button>
	</div>
	
	<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php /*
			foreach ($labels as $key => $label) {
				echo '<tr>';
				echo '<th>'.$label.'</th>';
				if ($item_fields[$key] == 'id') {
					// id field is not editable
					echo '<td><input class="form-control" disabled="disabled" name="Form['.$item_fields[$key].']" type="text" value="'.$item_values[$key].'" /></td>';
				}
				else {
					echo '<td><input class="form-control" name="Form['.$item_fields[$key].']" type="text" value="'.$item_values[$key].'" /></td>';		
				}
				echo '</tr>';
			}*/
		?>
	</tbody>
	</table>
</form>
</div>-->