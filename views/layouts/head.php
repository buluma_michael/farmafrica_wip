<!doctype html>
<html lang=en>
<head>
	<title><?php echo $config['site_title']; ?></title>
	<link rel="stylesheet" href="././assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="././assets/css/style.css" />
	<!--<link rel="stylesheet" href="././assets/css/template.css" />-->
	<link rel="stylesheet" href="././media/jui/css/chosen.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="././assets/js/jquery2.1.1.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="././assets/js/bootstrap3.2.0.min.js"></script>
	<script src="././media/jui/js/chosen.jquery.min.js"></script>
	<script src="././media/jui/js/ajax-chosen.min.js"></script>
	<script src="././media/jui/js/jquery.autocomplete.min.js"></script>
	<script src="././media/jui/js/bootstrap-tooltip-extended.min.js"></script>
	<script src="././media/jui/js/jquery.searchtools.min.js"></script>
	<script src="././assets/js/app.js"></script>
	<script>
		jQuery(document).ready(function (){
			jQuery('select').chosen({
				"disable_search_threshold":10,
				"search_contains":true,
				"allow_single_deselect":true,
				"placeholder_text_multiple":"Type or select some options",
				"placeholder_text_single":"Select an option",
				"no_results_text":"No results match"
			});
		});
	</script>
</head>
<body>