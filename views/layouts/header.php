<header class="navbar navbar-static-top navbar-inverse" id="top">
    <div class="container">
      	<div class="navbar-header">
		    <button aria-controls="bs-navbar" aria-expanded="false" class="collapsed navbar-toggle" data-target="#bs-navbar" data-toggle="collapse" type="button"> 
		    	<span class="sr-only">Toggle navigation</span> 
		    	<span class="icon-bar"></span> 
		    	<span class="icon-bar"></span> 
		    	<span class="icon-bar"></span> 
		    </button> 
			<a href="index.php" class="navbar-brand">Farm Africa</a> 
    	</div>
      	<nav class="collapse navbar-collapse" id="bs-navbar">
	        <ul class="nav navbar-nav">
	          	<!--<li><a href="index.php?view=data">Data</a></li>-->
	        </ul>
	        <ul class="nav navbar-nav navbar-right">	
	        	<li><a href="index.php?view=farmers">Farmers</a></li>
	          	<li><a href="index.php?view=site&action=about">About</a></li>
	          	<li><a href="index.php?view=site&action=contact">Contact</a></li>
	          	<li><a href="index.php?view=site&action=help">Help</a></li>
	        </ul>
      	</nav>
    </div>
</header>