<?php
require_once 'library/config.php';
require_once 'library/functions.php';

$errorMessage = '&nbsp;';

if (isset($_POST['txt_email'])) {
  $result = doLogin();
  
  if ($result != '') {
    $errorMessage = $result;
  }
}

?>
<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>Farm Africa | Login</title>

  <link href="assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/css/font-awesome/css/font-awesome.css" rel="stylesheet">

  <link href="assets/css/animate.css" rel="stylesheet">
  <link href="assets/css/style.css" rel="stylesheet">
  <link href="assets/css/custom.css" rel="stylesheet">

</head>

<body class="gray-bg">

  <div class="middle-box text-center loginscreen  animated fadeInDown">
    <div>
      <div class="logo">
        <img alt="image" src="./assets/img/logo-green.png">

      </div>
      <h3>Data & Communications Platform</h3>
      <p>Login in. To see it in action.</p>
      <form class="m-t" method="post" name="frmLogin" id="frmLogin">
      <div class="errorMessage" align="center"><?php echo $errorMessage; ?></div>
        <div class="form-group">
          <input type="email" name="txt_email" id="txt_email" class="form-control" placeholder="" required="" >
        </div>
        <div class="form-group">
          <input type="password" name="txt_password" id="txt_password" class="form-control" placeholder=""  required="">
        </div>
        
        <button type="submit" name="btnlogin" id="btnLogin" value="login" class="btn btn-primary block full-width m-b">Login</button>

        <p class="text-muted text-center"><small>Forgot your password?</small></p>
        <a class="btn btn-sm btn-white btn-block" href="#">Reset password</a>
      </form>
      <p class="m-t"> <small>Farm Africa Data & Communications &copy; 2017</small> </p>
    </div>
  </div>

  <!-- Mainly scripts -->
  <script src="assets/js/jquery-2.1.1.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>

</body>

</html>
