<?php
require_once 'db.php';
require_once './joomla.php';
//require_once './vendor/autoload.php';
//require_once 'framework.php';

defined('_JEXEC') or die;

class DataModel extends JModellist{

	//public $db = mysqliConnect();

	public static function getData($tablename, $userfields = array(), $userfilters = array(), $ids = array()){
		$db = JFactory::getDbo();
		// $table e.g farmers
		$table = self::getTableInstance($tablename);
		// get the table name in the database
		$dbtable = $table->dbtable; //
		// get the dataFields from table
		$datafields = $table->dataFields();

		//print_r($datafields);
		$filterfields = $table->filterFields();
		$dataModelFields 	= array_keys($table->dataFields()); // returns array keys
		$dataModelLabels	= array_values($table->dataFields());		
		$dataModelFKs 		= $table->dataFKFields();

		if (empty($dataModelFields)){
			// do something 
		}

		$dataModelFields2 	= array();
		// append main table alias `a` to fields
		foreach ($dataModelFields as $key => $value) {
			array_push($dataModelFields2, 'a.'.$value);
		}

		// choose only the columns user has selected
		if (!empty($userfields)) {
			// reset $dataModelFields2 array and populate it with the selected columns
			$dataModelFields2 = array();
			// reset $dataModelLabels and get the labels of the user selected columns
			$dataModelLabels = array();

			foreach ($userfields as $key => $column) {
				array_push($dataModelFields2, 'a.'.$column);
				$column_label = $table->dataFields()[$column];
				array_push($dataModelLabels, $column_label);
			}

			$foreignkeyfields = array();

			// we do this so as to later on remove unwanted columns in $dataModelFKs in the next loop
			foreach ($dataModelFKs as $k => $field) {
				//array_push($foreignkeyfields, $field['replaces'] => $k); 
				$foreignkeyfields[$field['replaces']] = $k;
				// county_govt_sector_id => sector_name, county_id => county_name, subsector_id => subsector_name
			}
			
			// if a defined foreign key is not in the selected columns, remove it from our list of Foreign Keys to be considered
			// county_govt_sector_id => sector_name, county_id => county_name, subsector_id => subsector_name
			foreach ($foreignkeyfields as $field => $fk) {
				if (!in_array($field, $userfields) ) {
					unset($dataModelFKs[$fk]);
				}
			}
		}

		// get our data
		$query = $db->getQuery(true);
		$query->select($dataModelFields2);
		$query->from($db->quoteName($dbtable).' AS a');
		//$select = $dataModelFields2;
		//$query = 'SELECT '. implode(',', $dataModelFields2);
		//$query .= 'FROM ' .$dbtable. ' AS a';

		// choose only the columns user has selected

		// query the filtered data/
		if (!empty($userfilters)){
			foreach ($userfilters as $field => $values) {
				if (!empty($values)){
					$query_values = implode(',', $db->quote($values));
					$query->where($db->quoteName('a.'.$field) . ' IN (' .$query_values.')');
					//$query .= 'WHERE a.' .$field. ' IN (' .$query_values. ')';
				}
			}
		}
		
		// loop through the fields and generate a join query for each
		if (!empty($dataModelFKs)){
			foreach ($dataModelFKs as $key => $field) {
				// check if field alias exists/ is specified
				if (array_key_exists('alias', $field)){
					$query->select(array($field['table_alias'].'.'.$key.' AS '.$field['alias']));
					//$query .= 'SELECT ';
				}
				else {
					$query->select(array($field['table_alias'].'.'.$key));
				}
				$query->join('LEFT', $db->quoteName($field['table'], $field['table_alias']) . ' ON (' . $db->quoteName($field['table_alias'].'.id') . ' = ' . $db->quoteName('a.'.$field['replaces']) . ')');
			}
		}	

		// execute the chained query and get the items from db
		$db->setQuery($query);
		$items = $db->loadObjectList();

		 // echo '<pre>';
		 // print_r($query->__toString());
		 // echo '</pre>';
		$totalRecords = count($items);

		foreach ($dataModelFKs as $key => $f) {	
			foreach ($items as $item) {	
				// if the field has an alias, replace the aliased field instead of the field itself
				if (array_key_exists('alias', $f)){
					$item->$f['replaces'] = $item->$f['alias'];
					// do not unset the key
					unset($item->$f['alias']);
				}
				else {
					if ($item->$f['replaces'] !== $item->$key) {
						$item->$f['replaces'] = $item->$key;
						unset($item->$key);
					}
				}				
			}
		}

		$response['labels'] = $dataModelLabels;
		$response['items'] = $items;
		//$response['filters'] = self::renderFilters($tablename);

		return $response;

	}

	public static function getTableInstance($tablename){
		if (isset($tablename)){
			// load the table
			require_once "./tables/$tablename.php";
			// class name is similar to table name
			$tbl = new $tablename;
		}

		return $tbl;
	}

	public static function getDbTableName($tablename){

	}

	public static function renderFilters($tablename){
		//$app = JFactory::getApplication();
		///$document = JFactory::getDocument();
		//JHtml::_('formbehavior.chosen', 'select');
		//print_r($app);
		$view = $_GET['view'];
		$userfilter = isset($_POST['filter']) ? $_POST['filter'] : array();

		//print_r($userfilter);
		
		$table = self::getTableInstance($tablename);
		$datafields = $table->dataFields();
		$filterfields = $table->filterFields();
		$totalfilters = count($filterfields);

		$html = '<div id="filterOptions" class="filters container-fluid">';
		$html .= '<form action="index.php?view='.$view.'" method="post">';	
			$html .= '<div class="filterdiv">';
			//$html .= '<h4>Filters</h4>';
			if (!empty($filterfields)){
				foreach ($filterfields as $colname => $field) {
					$label = $datafields[$colname];
					$options = $field['options'];

					$html .= '<div class="control-group by_'.$colname.' col-md-3">';
					$html .= '<div class="control-label"><label>'.$label.'</label></div>';
					$html .= '<div class="controls">';
					$html .= '<select width="200px" onchange="this.form.submit()" class="selectdropdown inputbox" name="filter['.$colname.'][]" id="filter_'.$colname.'" multiple>';
					// TODO: use session to get the filtered value, 
					// TODO DONE
						$html .= JHtml::_('select.options', $options, 'value', 'text', isset($userfilter[$colname]) ? $userfilter[$colname] : '');
					$html .= '</select>';
					$html .= '</div>'; // end div .controls
					$html .= '</div>'; // end div .control-group
				}
			}
			else {
				$html .= '<div class="alert alert-warning" style="padding:20px">Filter Fields have not being set for this dataset</div>';
			}
			
			$html .= '</div>'; // end div .filterdiv
			//$html .= '<div class="span12 statusbar preview"></div>';
		$html .= '</form>';
		$html .= '</div>'; // end div #filterOptions
		$html .= '<hr>';

		return $html;

	}
}

