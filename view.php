<?php
require_once 'library/config.php';
require_once 'library/functions.php';

$_SESSION['login_return_url'] = $_SERVER['REQUEST_URI'];
checkUser();

$view = (isset($_GET['v']) && $_GET['v'] != '') ? $_GET['v'] : '';

switch ($view) {
	case 'USER' :
		$content 	= 'user/list.php';		
		$pageTitle 	= 'Farm Africa - View Users';
		break;

	case 'Farm' :
		$content 	= 'farm/list.php';		
		$pageTitle 	= 'Farm Africa - View Farms';
		break;

	case 'Farmer' :
		$content 	= 'farmer/list.php';		
		$pageTitle 	= 'Farm Africa - View Farmer Profile';
		break;

	case 'Profile' :
		$content 	= 'farmer/profile.php';		
		$pageTitle 	= 'Farm Africa - View Farmers';
		break;
		
	case 'adduser' :
		$content 	= 'user/add.php';		
		$pageTitle 	= 'Farm Africa - Add Users';
		break;

	case 'Costs' :
		$content 	= 'farm/costs.php';		
		$pageTitle 	= 'Farm Africa - View Farm Costs';
		break;

	case 'Sales' :
		$content 	= 'farm/sales.php';		
		$pageTitle 	= 'Farm Africa - View Farm Sales';
		break;

	case 'Sampling' :
		$content 	= 'farm/sampling.php';		
		$pageTitle 	= 'Farm Africa - View Farm Sampling';
		break;

	case 'Feed' :
		$content 	= 'farm/feeds.php';		
		$pageTitle 	= 'Farm Africa - View Farm Feed';
		break;

	case 'Harvest' :
		$content 	= 'farm/harvest.php';		
		$pageTitle 	= 'Farm Africa - View Farm Harvest';
	break;

	case 'Market' :
		$content 	= 'farm/market.php';		
		$pageTitle 	= 'Farm Africa - View Farm Market';
		break;

	case 'Modules' :
		$content 	= 'training/modules.php';		
		$pageTitle 	= 'Farm Africa - View Training Modules';
		break;

	case 'Training' :
		$content 	= 'training/training.php';		
		$pageTitle 	= 'Farm Africa - View Training Information';
		break;

	

	default :
		$content 	= 'list.php';		
		$pageTitle 	= 'Farm Africa - View Users';
}

$script    = array('user.js','farm.js','farmer.js');

require_once 'template.php';

?>