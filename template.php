<?php

if (!defined('WEB_ROOT')) {
	exit;
}
$self = WEB_ROOT . 'index.php';
//checkTimeout();
?>
<html>
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $pageTitle; ?></title>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/bootstrap-select.min.js"></script>
	<link rel="stylesheet" href="<?php echo WEB_ROOT;?>assets/css/style.css" />
	<link href="<?php echo WEB_ROOT;?>assets/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo WEB_ROOT;?>assets/css/bootstrap-3.3.2.min.css" type="text/css">
  <link rel="stylesheet" href="<?php echo WEB_ROOT;?>assets/css/bootstrap-multiselect.css" type="text/css">
  <link href="<?php echo WEB_ROOT;?>assets/css/custom.css" rel="stylesheet">
  <link href="<?php echo WEB_ROOT;?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
	<script src="<?php echo WEB_ROOT;?>assets/js/jquery2.1.1.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript" src="<?php echo WEB_ROOT;?>assets/js/bootstrap-3.3.2.min.js"></script>
  <script type="text/javascript" src="<?php echo WEB_ROOT;?>assets/js/bootstrap-multiselect.js"></script>
  <script src="<?php echo WEB_ROOT;?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
  <script src="<?php echo WEB_ROOT;?>assets/js/inspinia.js"></script>
  <script language="JavaScript" type="text/javascript" src="<?php echo WEB_ROOT;?>library/common.js"></script>
    <?php
$n = count($script);
for ($i = 0; $i < $n; $i++) {
  if ($script[$i] != '') {
    echo '<script language="JavaScript" type="text/javascript" src="' . WEB_ROOT. 'library/' . $script[$i]. '"></script>';
  }
}
?>
    
	<script>
	
    function yesnoCheck() {
    if (document.getElementById('yesCheck').checked) {
        document.getElementById('ifYes').style.visibility = 'visible';
    }
    else document.getElementById('ifYes').style.visibility = 'hidden';

    };
    function businessplan() {
    if (document.getElementById('yes').checked) {
        document.getElementById('iftrue').style.visibility = 'visible';
    }
    else document.getElementById('iftrue').style.visibility = 'hidden';

    };
	</script>
  
</head>
<body>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
      <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
          <li class="nav-header">
            <div class="dropdown profile-element">
              <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">SM</strong>
                </span>  </span>
              </a>
              
            </div>
            <div class="logo-element">
              IN+
            </div>
          </li>
          <li class="active">
            <a href="index.php"><i class="fa fa-th-large"></i> <span class="nav-label">Home</span> <span class="fa arrow"></span></a>
            
          </li>
          <li>
            <a href="#"><i class="fa fa-database"></i> <span class="nav-label">Farm Profiles</span>  <span class="pull-right label label-primary hidden">SPECIAL</span> <span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li><a href="index.php?view=farmers">Farm Profiles</a></li>
              <li><a href="view.php?v=Costs">Farm Costs</a></li>
              <li><a href="view.php?v=Sales">Farm Sales</a></li>
              <li><a href="view.php?v=Sampling">Sampling Feed</a></li>
              <li><a href="view.php?v=Harvest">Harvest Information</a></li>
              <li><a href="view.php?v=Market">Fish Market</a></li>
            </ul>
          </li>
          <li>
            <a href="#"><i class="fa fa-envelope"></i> <span class="nav-label">SMS Messages </span><span class="label label-warning pull-right">0/0</span> </a>
            <ul class="nav nav-second-level">
              <li><a href="messages.html">Inbox</a></li>
              <li><a href="sms_detail.html">Message view</a></li>
              <li><a href="sms_compose.html">Compose Message</a></li>
              <li><a href="#">Surveys & Polls</a></li>
              <li><a href="#">Groups Management</a></li>
              <li><a href="#">Contact Management</a></li>
              <li><a href="#">SMS templates</a></li>
            </ul>
          </li>
          <li>
            <a href="#"><i class="fa fa-calendar"></i> <span class="nav-label">Training</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li><a href="view.php?v=Modules">Training Modules</a></li>
              <li><a href="view.php?v=Training">Training Calendar</a></li>
            </ul>
          </li>
          <li>
            <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Reports</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li><a href="#">Pond Information</a></li>
              <li><a href="#">Farm Harvests</a></li>
              <li><a href="#">Farm Feeds</a></li>
              <li><a href="#">Farmer Training</a></li>
              <li><a href="#">Product Cycle Report</a></li>
            </ul>
          </li>
          <li>
            <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Users</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li><a href="#">User Levels</a></li>
              <li><a href="view.php?v=USER">Users</a></li>
              <li><a href="library/logout.php">Logout</a></li>
            </ul>
          </li>
        </ul>

      </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
      <div class="row border-bottom">
        <!-- <nav class="navbar navbar-inverse navbar-static-top white-bg " role="navigation" style="margin-bottom: 0">
         
       
           
            

        </nav> -->
        <div class="container-fluid content">
	   <?php require_once $content;	?>
        </div>
      </div>

      <nav class="navbar navbar-default navbar-fixed-bottom">
      <div class="footer">
        <div class="pull-right">
          
        </div>
        <div align="center">
          <strong>Copyright</strong> Farm Africa &copy; 2017
        </div>
      </div>
      </nav>

    </div>
  </div>


</body>
</html>
