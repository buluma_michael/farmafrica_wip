<?php
require_once './config/config.db.php';

function mysqliConnect(){
	$host = DB_HOST;
	$user = DB_USER;
	$pass = DB_PASS;
	$dbname = DB_DB;
	$connection = mysqli_connect($host,$user,$pass,$dbname) or die("Error " . mysqli_error($db));
	return $connection;
}

//$db = mysqliConnect();

class DB {
	private $host;
	private $user;
	private $pass;
	private $dbname;
	private $connection;
	private $query;

	public function __construct($query = ''){
		$this->connection = mysqliConnect();
		$this->query = $query;
	}

	public function loadObjectList(){
		$result = mysqli_query($this->connection,$this->query) or die(mysqli_error($this->connection));
		while($res = mysqli_fetch_object($result)){ 
			$results[] = $res;
		}
		return $results;
	}

	public function loadObject(){
		$result = mysqli_query($this->connection,$this->query) or die(mysqli_error($this->connection));
		$res = mysqli_fetch_object($result);
		return $res;
	}
}