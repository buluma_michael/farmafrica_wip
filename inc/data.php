<?php
require_once 'db.php';
require_once './joomla.php';
require_once './vendor/autoload.php';
//require_once 'framework.php';

class Data {

	//public $db = mysqliConnect();

	public static function getData($tablename, $userfields = array(), $userfilters = array(), $ids = array()){
		$db = JFactory::getDbo();
		// $table e.g farmers
		$table = self::getTableInstance($tablename);
		//$table = self::getTable($tablename);
		// get the table name in the database
		$dbtable = $table->dbtable; //
		// get the dataFields from table
		$datafields = $table->dataFields();

		//print_r($datafields);
		$filterfields = $table->filterFields();
		$dataModelFields 	= array_keys($table->dataFields()); // returns array keys
		//$dataModelLabels	= array_values($table->dataFields());		
		$dataModelFKs 		= $table->dataFKFields();

		if (empty($dataModelFields)){
			// do something 
		}

		foreach ($datafields as $col => $field) {
			$dataModelLabels[] =  $field['label'];
		}
		// echo '<pre>';
		// print_r($datafields);
		// echo '</pre>';

		$dataModelFields2 	= array();
		// append main table alias `a` to fields
		foreach ($dataModelFields as $key => $value) {
			array_push($dataModelFields2, 'a.'.$value);
		}

		// choose only the columns user has selected
		if (!empty($userfields)) {
			// reset $dataModelFields2 array and populate it with the selected columns
			$dataModelFields2 = array();
			// reset $dataModelLabels and get the labels of the user selected columns
			$dataModelLabels = array();

			foreach ($userfields as $key => $column) {
				array_push($dataModelFields2, 'a.'.$column);
				$column_label = $table->dataFields()[$column];
				array_push($dataModelLabels, $column_label);
			}

			$foreignkeyfields = array();

			// we do this so as to later on remove unwanted columns in $dataModelFKs in the next loop
			foreach ($dataModelFKs as $k => $field) {
				//array_push($foreignkeyfields, $field['replaces'] => $k); 
				$foreignkeyfields[$field['replaces']] = $k;
				// county_govt_sector_id => sector_name, county_id => county_name, subsector_id => subsector_name
			}
			
			// if a defined foreign key is not in the selected columns, remove it from our list of Foreign Keys to be considered
			// county_govt_sector_id => sector_name, county_id => county_name, subsector_id => subsector_name
			foreach ($foreignkeyfields as $field => $fk) {
				if (!in_array($field, $userfields) ) {
					unset($dataModelFKs[$fk]);
				}
			}
		}

		// get our data
		$query = $db->getQuery(true);
		$query->select($dataModelFields2);
		$query->from($db->quoteName($dbtable).' AS a');
		//$select = $dataModelFields2;
		//$query = 'SELECT '. implode(',', $dataModelFields2);
		//$query .= 'FROM ' .$dbtable. ' AS a';

		// query the filtered data ids/
		if (!empty($ids)){
			//[1,2,3]
			$query_values = implode(',', $db->quote($ids));
			$query->where($db->quoteName('a.id') . ' IN (' .$query_values.')');
		}
		// choose only the columns user has selected

		// query the filtered data/
		if (!empty($userfilters)){
			foreach ($userfilters as $field => $values) {
				if (!empty($values)){
					$query_values = implode(',', $db->quote($values));
					$query->where($db->quoteName('a.'.$field) . ' IN (' .$query_values.')');
					//$query .= 'WHERE a.' .$field. ' IN (' .$query_values. ')';
				}
			}
		}
		
		// loop through the fields and generate a join query for each
		if (!empty($dataModelFKs)){
			foreach ($dataModelFKs as $key => $field) {
				// check if field alias exists/ is specified
				if (array_key_exists('alias', $field)){
					$query->select(array($field['table_alias'].'.'.$key.' AS '.$field['alias']));
					//$query .= 'SELECT ';
				}
				else {
					$query->select(array($field['table_alias'].'.'.$key));
				}
				$query->join('LEFT', $db->quoteName($field['table'], $field['table_alias']) . ' ON (' . $db->quoteName($field['table_alias'].'.id') . ' = ' . $db->quoteName('a.'.$field['replaces']) . ')');
			}
		}	

		// execute the chained query and get the items from db
		$db->setQuery($query);
		$items = $db->loadObjectList();

		 // echo '<pre>';
		 // print_r($query->__toString());
		 // echo '</pre>';
		$totalRecords = count($items);

		foreach ($dataModelFKs as $key => $f) {	
			foreach ($items as $item) {	
				// if the field has an alias, replace the aliased field instead of the field itself
				if (array_key_exists('alias', $f)){
					$item->$f['replaces'] = $item->$f['alias'];
					// do not unset the key
					unset($item->$f['alias']);
				}
				else {
					if ($item->$f['replaces'] !== $item->$key) {
						$item->$f['replaces'] = $item->$key;
						unset($item->$key);
					}
				}				
			}
		}

		$response['labels'] = $dataModelLabels;
		$response['items'] = $items;
		//$response['filters'] = self::renderFilters($tablename);

		// echo '<pre>';
		// print_r($response);
		// echo '</pre>';


		return $response;

	}

	public static function getTableInstance($tablename){
		if (isset($tablename)){
			// load the table
			require_once "./tables/$tablename.php";
			// class name is similar to table name
			$classname = "Table$tablename";
			$tbl = new $classname;

		}

		return $tbl;
	}

	public static function getDbTableName($tablename){

	}

	public static function renderFilters($tablename){
		$view = $_GET['view']; 
		$userfilter = isset($_POST['filter']) ? $_POST['filter'] : [];

		//print_r($userfilter);
		
		$table = self::getTableInstance($tablename);
		$datafields = $table->dataFields();
		$filterfields = $table->filterFields();
		$totalfilters = count($filterfields);

		$html = '<div id="filterOptions" class="filters container-fluid">';
		$html .= '<form action="index.php?view='.$view.'" method="post">';	
			$html .= '<div class="filterdiv">';
			//$html .= '<h4>Filters</h4>';
			if (!empty($filterfields)){
				foreach ($filterfields as $colname => $field) {
					$label = $datafields[$colname]['label'];
					$options = $field['options'];

					$html .= '<div class="control-group by_'.$colname.' col-md-3">';
					$html .= '<div class="control-label"><label>'.$label.'</label></div>';
					$html .= '<div class="controls">';
					$html .= '<select width="200px" onchange="this.form.submit()" class="selectdropdown inputbox" name="filter['.$colname.'][]" id="filter_'.$colname.'" multiple>';
						$html .= JHtml::_('select.options', $options, 'value', 'text', isset($userfilter[$colname]) ? $userfilter[$colname] : '');
					$html .= '</select>';
					$html .= '</div>'; // end div .controls
					$html .= '</div>'; // end div .control-group
				}
			}
			else {
				$html .= '<div class="alert alert-warning" style="padding:20px">Filter Fields have not being set for this dataset</div>';
			}
			
			$html .= '</div>'; // end div .filterdiv
			//$html .= '<div class="span12 statusbar preview"></div>';
		$html .= '</form>';
		$html .= '</div>'; // end div #filterOptions
		$html .= '<hr>';

		return $html;

	}

	public static function renderEditForm($tablename, $userfilters = [], $ids = []){
		$table = self::getTableInstance($tablename);
		$datafields = $table->dataFields();

		$data = self::getData($tablename, $userfields = [], $userfilters, $ids);
		$items = $data['items'];
		
		// echo '<pre>';
		// print_r($items);
		// echo '</pre>';
		$view = $_GET['view'];
		$action = $_GET['action'];
		$id = $_GET['id'];

		// render form for each item
		$html = '<div class="container-fluid">';

		//TODO: check if there are items in the db, otherwise show blank fields
		// this checks if the $items array is empty and adds an empty array item, takes care of displaying blank fields
		if (empty($items)){
			$items[] = [];
		};

		foreach ($items as $item) {
			$item = (array)$item;

			$html .= '<form action="index.php?view='.$view.'&action='.$action.'&id='.$id.'" method="post">';	
			$html .= '<div class="container">';
			$html .= '<div class="col-md-6 pull-right" style="text-align:right;">';
			$html .= '<a class="btn btn-default" href="index.php?view=farmers&action=view&id='.$id.'"> Back </a>';
			$html .= '<button class="btn btn-primary" type="submit" >Save</button>';
			$html .= '</div>'; // end col-md-6
			$html .= '</div><hr>';
				$html .= '<div class="filterdiv">';
				
				foreach ($datafields as $colname => $field) {
					// $field is an array
					
					$label = $field['label'];
					$type = $field['type'];
					$enabled = $field['state'];
					$dbvalue = array_key_exists($colname, $item) ? $item[$colname] : '';
					$inputname = $tablename.'['.$colname.']';	
					$element_id = $tablename.'_'.$colname;
					$html .= '<div class="form-group by_'.$colname.' col-md-3">';
					$html .= '<div class="form-label"><label>'.$label.'</label></div>';
					$html .= '<div class="controls">';
					switch ($type) {
						case 'select':
							if ($field['multiple']){
								$multiple = 'multiple';
								$inputname = $inputname.'[]';
							}
							else {
								$multiple = '';
								$inputname = $inputname;
							}
							$options = $field['options']; // only available for select field						
							$html .= '<select width="200px" class="selectdropdown form-control" name="'.$inputname.'" id="'.$element_id.'" '.$multiple.' '. $enabled.'>';
								$html .= JHtml::_('select.options', $options, 'value', 'text', isset($dbvalue) ? explode(',', $dbvalue) : '');
							$html .= '</select>';
							
						break;
						case 'date':
						case 'calendar':
							
							//$html .= JHtml::calendar($dbvalue, $inputname, 'date', '%Y-%m-%d',array('size'=>'8','maxlength'=>'10','class'=>' validate[\'required\']',));
							$html .= '<input type="text" name="'.$inputname.'" class="form-control" value ="'.$dbvalue.'" id="'.$element_id.'">';
							$html .= '
								<script>
								  	jQuery( function() {
								    	jQuery( "#'.$element_id.'" ).datepicker();
								  	});
								</script>
								';
						break;

						case 'textarea':
							$html .= '<textarea name="'.$inputname.'" id="'.$element_id.'" width="200px" class="inputbox form-control" '.$enabled.'>'.$dbvalue.'</textarea>';
						
						break;
						default:
							$html .= '<input type="'.$type.'" name="'.$inputname.'" id="'.$element_id.'" value="'.$dbvalue.'" width="200px" class="inputbox form-control" '.$enabled.'/>';
						break;
					}
					$html .= '</div>'; // end div .controls
					$html .= '</div>'; // end div .form-group

					
				}
				
				
				$html .= '</div>'; // end div .filterdiv
				//$html .= '<div class="span12 statusbar preview"></div>';
			$html .= '</form>';
			
				
		}// end foreach
		$html .= '</div>'; // end div #container-group
		$html .= '<hr>';

		

		return $html;

	}

}

