<?php
require_once './joomla.php';
//require './vendor/autoload.php';

defined('_JEXEC') or die('Restricted access');

require_once './helpers/datahelper.php';

class Tablefarms_staff{

    var $dbtable = 'farms_staff';

	function __construct() {
    }

    public function create(){}

    public function update($id){}

    public function delete($pk = null){}

    public function dataFieldsOld(){
        $array = [
            'id'               => 'Record ID',
            'farm_id'          => 'Farmer ID',
            'staff_name'       => 'Staff Name',
            'staff_role'       => 'Staff Role',
            'staff_gender'     => 'Staff Gender',
            'staff_age'        => 'Staff Age',

        ];

        return $array;
    }

    public function dataFields(){
        $array = [
            'id'                => [ 
                                    'type' => 'text',
                                    'state' => 'disabled',
                                    'label' => 'Record ID',
                                    ],
            'farm_id'           => [ 
                                    'type' => 'text',
                                    'state' => 'enabled',
                                    'label' => 'Farmer ID',
                                ],
            'staff_name'       => [ 
                                    'type' => 'text',
                                    'state' => 'enabled',
                                    'label' => 'Staff Name',
                                ],
            'staff_role'       => [ 
                                    'type' => 'text',
                                    'state' => 'enabled',
                                    'label' => 'Staff Role',
                                ],
            'staff_gender'     => [ 
                                    'type' => 'text',
                                    'state' => 'enabled',
                                    'label' => 'Staff Gender',
                                ],
            'staff_age'        => [ 
                                    'type' => 'text',
                                    'state' => 'enabled',
                                    'label' => 'Staff Age',
                                ],

        ];

        return $array;
    }

    public function dataFKFields(){
        // the default table alias for the main table is `a`
        $fields = [];
        
        return $fields;
    }

    public function filterFields(){
        // field options should be in format: value => text
        // or define a function which will return a list of options : the function should also return text and value
        // value => text
        $array = [
            'staff_gender' => array(
                'options' => DataHelper::getGenders()
            ),
        ];

        return $array;
    }


	
}