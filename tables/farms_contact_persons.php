<?php
require_once './joomla.php';
//require './vendor/autoload.php';

defined('_JEXEC') or die('Restricted access');

require_once './helpers/datahelper.php';

class Tablefarms_contact_persons{

    var $dbtable = 'farms_contact_persons';

	function __construct() {
    }

    public function create(){}

    public function update($id){}

    public function delete($pk = null){}

    public function dataFieldsold(){
        $array = [
            'id'                      => 'Record ID',
            'farm_id'                 => 'Farmer ID',
            'contact_name'            => 'Contact Name',
            'contact_telephone'       => 'Contact Telephone',
            'contact_gender'          => 'Contact Gender',
            'contact_email'           => 'Contact Email',
            'contact_position'        => 'Contact Position',

        ];

        return $array;
    }
    public function dataFields(){
        $array = [
            'id'                      => [ 
                                            'type' => 'text',
                                            'state' => 'disabled',
                                            'label' => 'Record ID',
                                            ],
            'farm_id'                 => [ 
                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'Farmer ID',
                                            ],
            'contact_name'            => [ 
                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'Contact Name',
                                            ],
            'contact_telephone'       => [ 
                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'Contact Telephone',
                                            ],
            'contact_gender'          => [ 
                                            'type' => 'select',
                                            'state' => 'enabled',
                                            'multiple' => false,
                                            'options' => DataHelper::getGenders(),
                                            'label' => 'Contact Gender',
                                        ],
            'contact_email'           => [ 
                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'Contact Email',
                                        ],
            'contact_position'        => [ 
                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'Contact Position',
                                        ],

        ];

        return $array;
    }

    public function dataFKFields(){
        // the default table alias for the main table is `a`
        $fields = [];
        
        return $fields;
    }

    public function filterFields(){
        // field options should be in format: value => text
        // or define a function which will return a list of options : the function should also return text and value
        // value => text
        $array = [
            'contact_gender' => array(
                'options' => DataHelper::getGenders()
            ),
        ];

        return $array;
    }


	
}