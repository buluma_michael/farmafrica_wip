<?php
require_once './joomla.php';
//require './vendor/autoload.php';

defined('_JEXEC') or die('Restricted access');

jimport('joomla.database.table');
//require_once './libs/adodb/adodb.inc.php';
//require_once './libs/adodb/adodb-active-record.inc.php';
require_once './helpers/datahelper.php';

class farmers extends JTable{

	private $table;
	public $list;
	var $dbtable = 'farmers';

	public function __construct() {

        $this->table = 'farms';
        $this->list = ['farmer1', 'farmer2'];

    }

    public function create(){}

    public function update($id){}

    public function delete($pk = null){}

    public static function all(){
    	$list = ['farmer1', 'farmer2'];
    	return $list;
    }

    public static function _find($id){
    	return static::all()[$id];
    	//return self::find($id);
    }

    public function dataFields(){
        $array = array(
            'id'                => 'ID',
            'date_registered'   => 'Registration Date',
            'name'              => 'Name',
            'registration'      => 'Registration Type',
            'gender'            => 'Gender',

        );

        return $array;
    }

    public function dataFKFields(){
        // the default table alias for the main table is `a`
        $fields = array();
        
        return $fields;
    }

    public function filterFields(){
        // field options should be in format: value => text
        // or define a function which will return a list of options : the function should also return text and value
        // value => text
        $array = array(
            'registration' => array(
                'options' => DataHelper::getFarmerRegistrationTypes()
            ),
            'gender' => array(
                'options' => DataHelper::getGenders()
            ),
        );

        return $array;
    }


	
}