<?php
require_once './joomla.php';
//require './vendor/autoload.php';

defined('_JEXEC') or die('Restricted access');

require_once './helpers/datahelper.php';

class Tablefarms_assessments{

    var $dbtable = 'farms_assessments';

	function __construct() {
    }

    public function create(){}

    public function update($id){}

    public function delete($pk = null){}

    public function dataFieldsOld(){
        $array = [
            'id'                        => 'Record ID',
            'farm_id'                   => 'Farmer ID',
            'assessor_id'               => 'Assessor ID',
            'assessor_observations'     => 'Assessor Observations',
            'reason_farmer_qualifies'   => 'Reason Farmer qualifies or does not qualify',
            'assessor_remarks'          => 'Remarks',

        ];

        return $array;
    }
     public function dataFields(){
        $array = [
            'id'                        => [ 
                                            'type' => 'text',
                                            'state' => 'disabled',
                                            'label' => 'Record ID',
                                            ],
            'farm_id'                   => [ 
                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'Farmer ID',
                                            ],
            'assessor_id'               => [ 
                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'Assessor ID',
                                            ],
            'assessor_observations'     => [ 
                                            'type' => 'textarea',
                                            'state' => 'enabled',
                                            'label' => 'Assessor Observations',
                                            ],
            'reason_farmer_qualifies'   => [ 
                                            'type' => 'textarea',
                                            'state' => 'enabled',
                                            'label' => 'Reason Farmer qualifies or does not qualify',
                                            ],
            'assessor_remarks'          => [ 
                                            'type' => 'textarea',
                                            'state' => 'enabled',
                                            'label' => 'Remarks',
                                            ],

        ];

        return $array;
    }

    public function dataFKFields(){
        // the default table alias for the main table is `a`
        $fields = [];
        
        return $fields;
    }

    public function filterFields(){
        // field options should be in format: value => text
        // or define a function which will return a list of options : the function should also return text and value
        // value => text
        $array = [
            /*'farm_registration' => array(
                'options' => DataHelper::getFarmerRegistrationTypes()
            ),
            /*'gender' => array(
                'options' => DataHelper::getGenders()
            ),*/
        ];

        return $array;
    }


	
}