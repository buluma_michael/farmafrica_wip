<?php
require_once './joomla.php';
//require './vendor/autoload.php';

defined('_JEXEC') or die('Restricted access');

require_once './helpers/datahelper.php';

class Tablefarms_ponds{

    var $dbtable = 'farms_ponds';

	function __construct() {
    }

    public function create(){}

    public function update($id){}

    public function delete($pk = null){}

    public function dataFieldsOld(){
        $array = [
            'id'                => 'Record ID',
            'farm_id'           => 'Farmer ID',
            'pond_number'       => 'Pond Number',
            'pond_area'         => 'Pond Dimension',
            'pond_catfish'      => 'Fingerlings Stocked (Catfish)',
            'pond_tilapia'      => 'Fingerlings Stocked (Tilapia)',
            'stocking_date'     => 'Date of Stocking',

        ];

        return $array;
    }

    public function dataFields(){
        $array = [
            'id'                => [ 
                                    'type' => 'text',
                                    'state' => 'disabled',
                                    'label' => 'Record ID',
                                    ],
            'farm_id'           => [ 
                                    'type' => 'text',
                                    'state' => 'enabled',
                                    'label' => 'Farmer ID',
                                ],
            'pond_number'       => [ 
                                    'type' => 'text',
                                    'state' => 'enabled',
                                    'label' => 'Pond Number',
                                ],
            'pond_area'         => [ 
                                    'type' => 'text',
                                    'state' => 'enabled',
                                    'label' => 'Pond Dimension',
                                ],
            'pond_catfish'      => [ 
                                    'type' => 'number',
                                    'state' => 'enabled',
                                    'label' => 'Fingerlings Stocked (Catfish)',
                                ],
            'pond_tilapia'      => [ 
                                    'type' => 'number',
                                    'state' => 'enabled',
                                    'label' => 'Fingerlings Stocked (Tilapia)',
                                ],
            'stocking_date'     => [ 
                                    'type' => 'date',
                                    'state' => 'enabled',
                                    'label' => 'Date of Stocking',
                                ],

        ];

        return $array;
    }

    public function dataFKFields(){
        // the default table alias for the main table is `a`
        $fields = [];
        
        return $fields;
    }

    public function filterFields(){
        // field options should be in format: value => text
        // or define a function which will return a list of options : the function should also return text and value
        // value => text
        $array = [
            /*'owner_gender' => array(
                'options' => DataHelper::getGenders()
            ),*/
        ];

        return $array;
    }


	
}