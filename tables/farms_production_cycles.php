<?php
require_once './joomla.php';
//require './vendor/autoload.php';

defined('_JEXEC') or die('Restricted access');

require_once './helpers/datahelper.php';

class Tablefarms_production_cycles{

    var $dbtable = 'farms_production_cycles';

	function __construct() {
    }

    public function create(){}

    public function update($id){}

    public function delete($pk = null){}

    public function dataFieldsOld(){
        $array = [
            'id'                    => 'Record ID',
            'farm_id'               => 'Farmer ID',
            'year'                  => 'Year Cycle Completed',
            'species'               => 'Species',
            'length'                => 'Length of Cycle (months)',
            'ponds_harvested_size'  => 'size of ponds harvested (m2)',
            'fingerlings_bought'    => 'Number of Fingerlings bought',
            'fingerlings_source'    => 'Source of Fingerlings',
            'feeds_bought'          => 'Kgs of Feeds bought in last cycle',
            'feeds_source'          => 'Source of Feeds',
            'fish_harvested'        => 'Kgs of fish hasrvested in last production cycle',

        ];

        return $array;
    }

    public function dataFields(){
        $array = [
            'id'                => [ 
                                    'type' => 'text',
                                    'state' => 'disabled',
                                    'label' => 'Record ID',
                                    ],
            'farm_id'           => [ 
                                    'type' => 'text',
                                    'state' => 'enabled',
                                    'label' => 'Farmer ID',
                                ],
            'year'                  => [ 
                                    'type' => 'text',
                                    'state' => 'enabled',
                                    'label' => 'Year Cycle Completed',
                                ],
            'species'               => [ 
                                    'type' => 'text',
                                    'state' => 'enabled',
                                    'label' => 'Species',
                                ],
            'length'                => [ 
                                    'type' => 'number',
                                    'state' => 'enabled',
                                    'label' => 'Length of Cycle (months)',
                                ],
            'ponds_harvested_size'  => [ 
                                    'type' => 'text',
                                    'state' => 'enabled',
                                    'label' => 'size of ponds harvested (m2)',
                                    ],
            'fingerlings_bought'    => [ 
                                    'type' => 'number',
                                    'state' => 'enabled',
                                    'label' => 'Number of Fingerlings bought',
                                    ],
            'fingerlings_source'    => [ 
                                    'type' => 'text',
                                    'state' => 'enabled',
                                    'label' => 'Source of Fingerlings',
                                    ],
            'feeds_bought'          => [ 
                                    'type' => 'number',
                                    'state' => 'enabled',
                                    'label' => 'Kgs of Feeds bought in last cycle',
                                    ],
            'feeds_source'          => [ 
                                    'type' => 'text',
                                    'state' => 'enabled',
                                    'label' => 'Source of Feeds',
                                    ],
            'fish_harvested'        => [ 
                                    'type' => 'number',
                                    'state' => 'enabled',
                                    'label' => 'Kgs of fish hasrvested in last production cycle',
                                    ],

        ];

        return $array;
    }

    public function dataFKFields(){
        // the default table alias for the main table is `a`
        $fields = [];
        
        return $fields;
    }

    public function filterFields(){
        // field options should be in format: value => text
        // or define a function which will return a list of options : the function should also return text and value
        // value => text
        $array = [
            /*'owner_gender' => array(
                'options' => DataHelper::getGenders()
            ),*/
        ];

        return $array;
    }


	
}