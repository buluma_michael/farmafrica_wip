<?php
require_once './joomla.php';

defined('_JEXEC') or die('Restricted access');

jimport('joomla.database.table');
require_once './helpers/datahelper.php';

class Tablefarms{

    var $dbtable = 'farms';

	function __construct() {
    }

    public function create(){}

    public function update($id){}

    public function delete($pk = null){}

    public function dataFieldsOld(){
        $array = [
            'id'                        => 'Record ID',
            'farm_identifier'           => 'Farmer ID',
            'farm_date_registered'      => 'Registration Date',
            'farm_name'                 => 'Name',
            'farm_registration'         => 'Registration Type',
            'farm_contact_person'       => 'Contact Person',
            'farm_county'               => 'County',
            'farm_subcounty'            => 'Subcounty',
            'farm_village'              => 'Village',
            'farm_landmark'             => 'Landmark',
            'farm_altitude'             => 'Altitude',
            'farm_longitude'            => 'Longitude',
            'farm_latitude'             => 'Latitude',
            'keeps_records'             => 'Keeps Records',
            'records_kept'              => 'Type of Records Kept',
            'finance_sources'           => 'Sources of Finance',
            'has_business_plan'         => 'Has Business Plan',
            'business_plan_last_update' => 'Business Plan Last Updated',
            'ponds_number'              => 'Number of Ponds',
            'ponds_stocked'             => 'Number of Ponds Stocked',
            'ponds_total_area'          => 'Total Ponds Area',
            'pond_sample_temperature'               => 'Temperature of Sampled Pond',
            'pond_sample_time'                      => 'Time of Day',
            'tilapia_price_per_fingerling'          => 'Price per Fingerling (Tilapia)',
            'catfish_price_per_fingerling'          => 'Price per Fingerling (Catfish)',
            'tilapia_fingerling_stocking_weight'    => 'Weight of Fingerlings at Socking (Tilapia)',
            'catfish_fingerling_stocking_weight'    => 'Weight of Fingerlings at Socking (Tilapia)',

        ];

        return $array;
    }
    public function dataFields(){
        $array = [
            'id'                        => [ 
                                            'type' => 'text',
                                            'state' => 'disabled',
                                            'label' => 'Record ID',
                                            ],
            'farm_identifier'           => [ 

                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'Farmer ID',
                                            ],
            'farm_date_registered'      => [ 
                                            'type' => 'calendar',
                                            'state' => 'enabled',
                                            'label' => 'Registration Date',
                                            ],
            'farm_name'                 => [ 
                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'Farm Name',
                                            ],
            'farm_registration'         => [ 
                                            'type' => 'select',
                                            'state' => 'enabled',
                                            'multiple' => false,
                                            'options' => DataHelper::getFarmerRegistrationTypes(),
                                            'label' => 'Registration Type',
                                            ],
            'farm_contact_person'       => [ 
                                            'type' => 'select',
                                            'state' => 'enabled',
                                            'multiple' => false,
                                            'options' => [  
                                                            ['value' => 'Owner', 'text' => 'Owner'], 
                                                            ['value' => 'Other', 'text' => 'Other']
                                                        ],
                                            'label' => 'Contact Person',
                                            ],
            'farm_county'               => [ 
                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'County',
                                            ],
            'farm_subcounty'            => [ 
                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'Subcounty',
                                            ],
            'farm_village'              => [ 
                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'Village',
                                            ],
            'farm_landmark'             => [ 
                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'Landmark',
                                            ],
            'farm_altitude'             => [ 
                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'Altitude',
                                            ],
            'farm_longitude'            => [ 
                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'Longitude',
                                            ],
            'farm_latitude'             => [ 
                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'Latitude',
                                            ],
            'keeps_records'             => [ 
                                            'type' => 'select',
                                            'state' => 'enabled',
                                            'multiple' => false,
                                            'options' => DataHelper::getYesNo(),
                                            'label' => 'Keeps Records',
                                            ],
            'records_kept'              => [ 
                                            'type' => 'select',
                                            'state' => 'enabled',
                                            'multiple' => true,
                                            'options' => DataHelper::getTypesofRecords(),
                                            'label' => 'Type of Records Kept',
                                            ],
            'finance_sources'           => [ 
                                            'type' => 'select',
                                            'state' => 'enabled',
                                            'multiple' => true,
                                            'options' => DataHelper::getFinanceSources(),
                                            'label' => 'Sources of Finance',
                                            ],
            'has_business_plan'         => [ 
                                            'type' => 'select',
                                            'state' => 'enabled',
                                            'multiple' => false,
                                            'options' => DataHelper::getYesNo(),
                                            'label' => 'Has Business Plan',
                                            ],
            'business_plan_last_update' => [ 
                                            'type' => 'calendar',
                                            'state' => 'enabled',
                                            'label' => 'Business Plan Last Updated',
                                            ],
            'ponds_number'              => [ 
                                            'type' => 'number',
                                            'state' => 'enabled',
                                            'label' => 'Number of Ponds',
                                            ],
            'ponds_stocked'             => [ 
                                            'type' => 'number',
                                            'state' => 'enabled',
                                            'label' => 'Number of Ponds Stocked',
                                            ],
            'ponds_total_area'          => [ 
                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'Total Ponds Area',
                                            ],
            'pond_sample_temperature'               => [ 
                                                        'type' => 'text',
                                                        'state' => 'enabled',
                                                        'label' => 'Temperature of Sampled Pond',
                                                        ],
            'pond_sample_time'                      => [ 
                                                        'type' => 'text',
                                                        'state' => 'enabled',
                                                        'label' => 'Time of Day',
                                                        ],
            'tilapia_price_per_fingerling'          => [ 
                                                        'type' => 'number',
                                                        'state' => 'enabled',
                                                        'label' => 'Price per Fingerling (Tilapia)',
                                                        ],
            'catfish_price_per_fingerling'          => [ 
                                                        'type' => 'number',
                                                        'state' => 'enabled',
                                                        'label' => 'Price per Fingerling (Catfish)',
                                                        ],
            'tilapia_fingerling_stocking_weight'    => [ 
                                                        'type' => 'number',
                                                        'state' => 'enabled',
                                                        'label' => 'Weight of Fingerlings at Socking (Tilapia)',
                                                        ],
            'catfish_fingerling_stocking_weight'    => [ 
                                                        'type' => 'number',
                                                        'state' => 'enabled',
                                                        'label' => 'Weight of Fingerlings at Socking (Catfish)',
                                                        ],

        ];

        return $array;
    }

    public function dataFKFields(){
        // the default table alias for the main table is `a`
        $fields = [];
        
        return $fields;
    }

    public function filterFields(){
        // field options should be in format: value => text
        // or define a function which will return a list of options : the function should also return text and value
        // value => text
        $array = [
            'farm_registration' => array(
                'options' => DataHelper::getFarmerRegistrationTypes()
            ),
            /*'gender' => array(
                'options' => DataHelper::getGenders()
            ),*/
        ];

        return $array;
    }


	
}