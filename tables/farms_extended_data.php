<?php
require_once './joomla.php';
//require './vendor/autoload.php';

defined('_JEXEC') or die('Restricted access');

require_once './helpers/datahelper.php';

class Tablefarms_extended_data{

    var $dbtable = 'farms_extended_data';

	function __construct() {
    }

    public function create(){}

    public function update($id){}

    public function delete($pk = null){}

    public function dataFieldsOld(){
        $array = [
            'id'                        => 'Record ID',
            'farm_id'                   => 'Farmer ID',
            'water_availability'        => 'Water Availability',  // 
            'water_sources'             => 'Sources of water', // options 
            'water_mechanism'           => 'How water gets to pond', // options
            'farm_equipments'           => 'Essential equipments on farm',
            'farm_greenhouse'           => 'Do you farm fish in a greenhouse',
            'staff_total'               => 'Total number of staff',
            'staff_fulltime'            => 'Number of Fulltime Staff',
            'staff_parttime'            => 'Number of Parttime Staff',
            'has_security'              => 'Is there security on the farm', // Yes, No
            'security_types'            => 'What Type of Security is there', // options
            'customers'                 => 'Whom do you sell your fish to', // options
            'customers_others'          => 'Other Fish Customers',
            'challenges'                => 'Challenges faced in fish farming',
            'five_year_target'          => 'Target of your fish farming enterprise in 5 years in size and turnover',
            'needs_to_reach_target'     => 'What do you need to accomplish this target',
            'receive_updates'           => 'Would you like to receive updates from Farm Africa Aquaculture project',
            'can_host_trainings'        => 'Are you willing to host practical trainings on your farm',
        ];

        return $array;
    }

    public function dataFields(){
        $array = [
            'id'                        => [ 
                                            'type' => 'text',
                                            'state' => 'disabled',
                                            'label' => 'Record ID',
                                        ],
            'farm_id'                   => [ 
                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'Farmer ID',
                                        ],
            'water_availability'        => [ 
                                            'type' => 'select',
                                            'state' => 'enabled',
                                            'multiple' => false,
                                            'options' => DataHelper::getWaterAvailability(),
                                            'label' => 'Water Availability',
                                        ], 
            'water_sources'             => [ 
                                            'type' => 'select',
                                            'state' => 'enabled',
                                            'multiple' => true,
                                            'options' => DataHelper::getWaterSources(),
                                            'label' => 'Sources of Water',
                                        ],  
            'water_mechanism'           => [ 
                                            'type' => 'select',
                                            'state' => 'enabled',
                                            'multiple' => true,
                                            'options' => DataHelper::getWaterMechanism(),
                                            'label' => 'How water gets to pond',
                                        ], 
            'farm_equipments'           => [ 
                                            'type' => 'select',
                                            'state' => 'enabled',
                                            'multiple' => true,
                                            'options' => DataHelper::getFarmEquipments(),
                                            'label' => 'Essential equipments on farm',
                                        ], 
            'farm_greenhouse'           => [ 
                                            'type' => 'select',
                                            'state' => 'enabled',
                                            'multiple' => false,
                                            'options' => DataHelper::getYesNo(),
                                            'label' => 'Do you farm fish in a greenhouse',
                                        ],
            'staff_total'               => [ 
                                            'type' => 'number',
                                            'state' => 'enabled',
                                            'label' => 'Total number of staff',
                                        ],
            'staff_fulltime'            => [ 
                                            'type' => 'number',
                                            'state' => 'enabled',
                                            'label' => 'Number of Fulltime Staff',
                                        ],
            'staff_parttime'            => [ 
                                            'type' => 'number',
                                            'state' => 'enabled',
                                            'label' => 'Number of Parttime Staff',
                                        ],
            'has_security'              => [ 
                                            'type' => 'select',
                                            'state' => 'enabled',
                                            'multiple' => false,
                                            'options' => DataHelper::getYesNo(),
                                            'label' => 'Is there security on the farm',
                                        ], 
            'security_types'            => [ 
                                            'type' => 'select',
                                            'state' => 'enabled',
                                            'multiple' => true,
                                            'options' => DataHelper::getSecurityTypes(),
                                            'label' => 'What Type of Security is there',
                                        ], 
            'customers'                 => [ 
                                            'type' => 'select',
                                            'state' => 'enabled',
                                            'multiple' => true,
                                            'options' => DataHelper::getCustomerTypes(),
                                            'label' => 'Whom do you sell your fish to',
                                        ], 
            'customers_others'          => [ 
                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'Other Fish Customers',
                                        ],
            'challenges'                => [ 
                                            'type' => 'textarea',
                                            'state' => 'enabled',
                                            'label' => 'Challenges faced in fish farming',
                                        ],
            'five_year_target'          => [ 
                                            'type' => 'textarea',
                                            'state' => 'enabled',
                                            'label' => 'Target of your fish farming enterprise in 5 years in size and turnover',
                                        ],
            'needs_to_reach_target'     => [ 
                                            'type' => 'textarea',
                                            'state' => 'enabled',
                                            'label' => 'What do you need to accomplish this target',
                                        ],
            'receive_updates'           => [ 
                                            'type' => 'select',
                                            'state' => 'enabled',
                                            'multiple' => false,
                                            'options' => DataHelper::getYesNo(),
                                            'label' => 'Would you like to receive updates from Farm Africa Aquaculture project',
                                        ],
            'can_host_trainings'        => [ 
                                            'type' => 'select',
                                            'state' => 'enabled',
                                            'multiple' => false,
                                            'options' => DataHelper::getYesNo(),
                                            'label' => 'Are you willing to host practical trainings on your farm',
                                        ],
        ];

        return $array;
    }

    public function dataFKFields(){
        // the default table alias for the main table is `a`
        $fields = [];
        
        return $fields;
    }

    public function filterFields(){
        // field options should be in format: value => text
        // or define a function which will return a list of options : the function should also return text and value
        // value => text
        $array = [
            /*'farm_registration' => array(
                'options' => DataHelper::getFarmerRegistrationTypes()
            ),
            'gender' => array(
                'options' => DataHelper::getGenders()
            ),*/
        ];

        return $array;
    }


	
}