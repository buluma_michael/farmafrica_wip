<?php
require_once './joomla.php';
//require './vendor/autoload.php';

defined('_JEXEC') or die('Restricted access');

require_once './helpers/datahelper.php';

class Tablefarms_assessors{

    var $dbtable = 'farms_assessors';

	function __construct() {
    }

    public function create(){}

    public function update($id){}

    public function delete($pk = null){}

    public function dataFieldsOld(){
        $array = [
            'id'                       => 'Record ID',
            'assessor_name'            => 'Assessor Name',
            'assessor_telephone'       => 'Assessor Telephone',
            'assessor_gender'          => 'Assessor Gender',
            'assessor_email'           => 'Assessor Email',
            'assessor_age'             => 'Assessor Age',

        ];

        return $array;
    }
    public function dataFields(){
        $array = [
            'id'                       => [ 
                                            'type' => 'text',
                                            'state' => 'disabled',
                                            'label' => 'Record ID',
                                            ],
            'assessor_name'            => [ 
                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'Assessor Name',
                                            ],
            'assessor_telephone'       => [ 
                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'Assessor Telephone',
                                            ],
            'assessor_gender'          => [ 
                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'Assessor Gender',
                                            ],
            'assessor_email'           => [ 
                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'Assessor Email',
                                            ],
            'assessor_age'             => [ 
                                            'type' => 'text',
                                            'state' => 'enabled',
                                            'label' => 'Assessor Age',
                                            ],

        ];

        return $array;
    }

    public function dataFKFields(){
        // the default table alias for the main table is `a`
        $fields = [];
        
        return $fields;
    }

    public function filterFields(){
        // field options should be in format: value => text
        // or define a function which will return a list of options : the function should also return text and value
        // value => text
        $array = [
            /*'farm_registration' => array(
                'options' => DataHelper::getFarmerRegistrationTypes()
            ),
            /*'gender' => array(
                'options' => DataHelper::getGenders()
            ),*/
        ];

        return $array;
    }


	
}