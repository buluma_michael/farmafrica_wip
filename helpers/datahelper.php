<?php

class DataHelper{

	public static function getFarmerRegistrationTypes(){
		$types = ['Not Registered','LTD','SHG'];
		$types_array = [];
		foreach ($types as $key => $type) {
			$types_array[] = ['value' => $type, 'text' => $type];
		}

		return $types_array;
	}

	public static function getGenders(){
		$types = [
			['value' => 'Female', 'text' => 'Female'], 
			['value' => 'Male', 'text' => 'Male'],
		];

		return $types;
	}

	public static function getYesNo(){
		$types = [
			['value' => 'Yes', 'text' => 'Yes'], 
			['value' => 'No', 'text' => 'No'],
		];

		return $types;
	}

	public static function getTypesofRecords(){
		$types = ['Stocking Records','Feeding Records','Sampling Records', 
				'Inventory Records','Water Quality Records','Financial Records', 'Produce Sample Records'];
		$types_array = [];
		foreach ($types as $key => $type) {
			$types_array[] = ['value' => $type, 'text' => $type];
		}

		return $types_array;
	}
	public static function getFinanceSources(){
		$types = ['Donation/Grant','Own Savings','Loan (MFI,VSLA)'];
		$types_array = [];
		foreach ($types as $key => $type) {
			$types_array[] = ['value' => $type, 'text' => $type];
		}

		return $types_array;
	}
	public static function getWaterAvailability(){
		$types = ['Permanent','Seasonal'];
		$types_array = [];
		foreach ($types as $key => $type) {
			$types_array[] = ['value' => $type, 'text' => $type];
		}

		return $types_array;
	}
	public static function getWaterSources(){
		$types = ['River/Stream','Ground Water','Piped','Borehole','Other'];
		$types_array = [];
		foreach ($types as $key => $type) {
			$types_array[] = ['value' => $type, 'text' => $type];
		}

		return $types_array;
	}
	public static function getWaterMechanism(){
		$types = ['Gravity Fed','Pumping'];
		$types_array = [];
		foreach ($types as $key => $type) {
			$types_array[] = ['value' => $type, 'text' => $type];
		}

		return $types_array;
	}
	public static function getFarmEquipments(){
		$types = ['Weighing Scale','Thermometer','Seine Net','Others'];
		$types_array = [];
		foreach ($types as $key => $type) {
			$types_array[] = ['value' => $type, 'text' => $type];
		}

		return $types_array;
	}
	public static function getMainFeeds(){
		$types = ['Local Feeds','Commercial Feeds','Green Water', 'Homemade Feeds'];
		$types_array = [];
		foreach ($types as $key => $type) {
			$types_array[] = ['value' => $type, 'text' => $type];
		}

		return $types_array;
	}
	public static function getSecurityTypes(){
		$types = ['Guard','Fencing'];
		$types_array = [];
		foreach ($types as $key => $type) {
			$types_array[] = ['value' => $type, 'text' => $type];
		}

		return $types_array;
	}
	public static function getCustomerTypes(){
		$types = ['Local Consumers','Traders/Middlemen','Hotels','Institutions','Others'];
		$types_array = [];
		foreach ($types as $key => $type) {
			$types_array[] = ['value' => $type, 'text' => $type];
		}

		return $types_array;
	}
	public static function getRecommendationTypes(){
		$types = ['Recommended','Non-recommended','Can be Considered later'];
		$types_array = [];
		foreach ($types as $key => $type) {
			$types_array[] = ['value' => $type, 'text' => $type];
		}

		return $types_array;
	}




}
