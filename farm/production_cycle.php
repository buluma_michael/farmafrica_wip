<?php
if (!defined('WEB_ROOT')) {
  exit;
}



$errorMessage = (isset($_GET['error']) && $_GET['error'] != '') ? $_GET['error'] : '&nbsp;';

$sql = "SELECT id, farm_name FROM farms order by id desc
limit 1";
$result = dbQuery($sql);
while($row = dbFetchAssoc($result)) {
    extract($row);
   }


?> 

<div class="prepend-1 span-12">
<h4>Production cycle for:<?php echo $farm_name; ?> Farm</h4>
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-md-12">
<table class="table table-striped table-bordered">
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=cycle" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
    <div class="form-group row">
    <input class="form-control" name="id" type="hidden" id="id" value="<?php echo $id; ?>" readonly>
    <input class="form-control" name="farm_name" type="hidden" id="farm_name" value="<?php echo $farm_name; ?>" readonly >
  
    </div>
    <div class="form-group row" >
    <label for="year" class="col-md-3 col-form-label">Year Cycle Completed:<input class="form-control" name="year" type="year" id="year" value="" required="" ></label>
    <label for="species" class="col-md-3 col-form-label">Species:<input class="form-control" name="species" type="text" id="species" value="" required="" ></label>
    <label for="length" class="col-md-3 col-form-label">Length of Cycle (months):<input class="form-control" name="length" type="number" id="length" value="" required="" ></label>
 
    </div>
    <div class="form-group row" >
    <label for="ponds_harvested_size" class="col-md-3 col-form-label">Size of ponds harvested (m2):<input class="form-control" name="ponds_harvested_size" type="text" id="ponds_harvested_size" value="" required="" ></label>
    <label for="fingerlings_bought" class="col-md-3 col-form-label">Number of Fingerlings bought:<input class="form-control" name="fingerlings_bought" type="text" id="fingerlings_bought" value="" required="" ></label>
    <label for="fingerlings_source" class="col-md-3 col-form-label">Source of Fingerlings:<input class="form-control" name="fingerlings_source" type="text" id="fingerlings_source" value="" required="" ></label>
    </div>
    <div class="form-group row" >
    <label for="feeds_bought" class="col-md-3 col-form-label">Kgs of Feeds bought in last cycle:<input class="form-control" name="feeds_bought" type="text" id="feeds_bought" value="" required="" ></label>
    <label for="feeds_source" class="col-md-3 col-form-label">Source of Feeds:<input class="form-control" name="feeds_source" type="text" id="feeds_source" value="" required="" ></label>
    <label for="fish_harvested" class="col-md-3 col-form-label">Kgs of fish hasrvested in last production cycle:<input class="form-control" name="fish_harvested" type="text" id="fish_harvested" value="" required="" ></label>
    </div>

    <p align="left"> 
    <input name="btnAddUser" type="button"   class="button" id="btnAddUser" value="Save (✔)" onClick="checkAddcycleForm()" class="box">  
    </p>
   </form>
  </tbody>
  </table>
</div>


</div>