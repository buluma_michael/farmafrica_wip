<?php
if (!defined('WEB_ROOT')) {
	exit;
}

$sql = "SELECT * FROM farms ORDER BY farm_identifier";
$result = dbQuery($sql);

?> 
<div class="prepend-1 span-17">
<form action="processfarm.php?action=add" method="post"  name="frmListUser" id="frmListUser">
<div class="col-md-12">
<div class="table-responsive ">
<table class="table table-striped table-bordered table-sm">
  <thead>
  <tr>
   <td>Farm Identifier</td>
   <td>Name</td>
   <td>Registration</td>
   <td>Date Registered</td>
   <td>Contact Person</td>
   <td>County</td>
   <td>Sub County</td>
   <td>Village</td>
   <td>Landmarks</td>
   <td>Keeps Records(?)</td>
   <td>Records Kept</td>
   <td>Finance Sources</td>
   <td>Business Plan</td>
   <td>Last Time Business Plan updated</td>
   <td>No. of Ponds</td>
   <td>Pond stocked</td>
   <td>Ponds total area</td>
   <td>Sample temp.</td>
   <td>Sample time</td>
   <td>Price/fingerling(tilapia)</td>
   <td>Price/fingerling(catfish)</td>
   <td>Stocking weight(tilapia fingerling)</td>
   <td>Stocking weight(catfish fingerling)</td>
   <td>Delete&nbsp;/&nbsp;Update</td>
  </tr>
</thead>
<tbody>
<?php
while($row = dbFetchAssoc($result)) {
	extract($row);
	
	if ($i%2) {
		$class = 'row1';
	} else {
		$class = 'row2';
	}
	
	$i += 1;
?>
  <tr class="<?php echo $class; ?>"> 
   <td><?php echo $farm_identifier; ?></td>
   <td><?php echo $farm_name; ?></td>
   <td><?php echo $farm_registration; ?></td>
   <td><?php echo $farm_date_registered; ?></td>
   <td><?php echo $farm_contact_person; ?></td>
   <td><?php echo $farm_county; ?></td>
   <td><?php echo $farm_subcounty; ?></td>
   <td><?php echo $farm_village; ?></td>
   <td><?php echo $farm_landmark; ?></td>
   <td><?php echo $record_keeping; ?></td>
   <td><?php echo $records_kept; ?></td>
   <td><?php echo $finance_sources; ?></td>
   <td><?php echo $has_business_plan; ?></td>
   <td><?php echo $business_plan_last_update; ?></td>
   <td><?php echo $ponds_number; ?></td>
   <td><?php echo $ponds_stocked; ?></td>
   <td><?php echo $ponds_total_area; ?></td>
   <td><?php echo $pond_sample_temperature; ?></td>
   <td><?php echo $pond_sample_time; ?></td>
   <td><?php echo $tilapia_price_per_fingerling; ?></td>
   <td><?php echo $catfish_price_per_fingerling; ?></td>
   <td><?php echo $tilapia_fingerling_stocking_weight; ?></td>
   <td><?php echo $catfish_fingerling_stocking_weight; ?></td>
   
   
   <td align="center"><a  style="font-weight:normal;" href="javascript:deleteLab(<?php echo $id; ?>);">Delete</a>&nbsp;/&nbsp;<a  style="font-weight:normal;" href="javascript:editLab(<?php echo $id; ?>);">Edit</a></td>
  </tr>
<?php
} // end while

?>
  <tr> 
   <td colspan="24">&nbsp;</td>
  </tr>
  <tr> 
   <td colspan="24" align="center"><input name="btnAddUser" type="button" id="btnAddUser" value="Add Farm(+)"  class="button" onClick="addfarm()"></td>
  </tr>
 </tbody>
</table>
</div>
</div>

</form>

</div>