<?php
require_once 'farmer/edittab.php';
if (!defined('WEB_ROOT')) {
  exit;
}

$errorMessage = "";

$fid = "SELECT farmer_id  FROM farms_owners where f_id='$f_id'";
$result1 = dbQuery($fid);
while($row = dbFetchAssoc($result1)) {
  extract($row);

  }
  $sql="SELECT p.farm_id,p.year,p.species,p.length,p.ponds_harvested_size,p.fingerlings_bought,p.fingerlings_source,p.feeds_bought,p.feeds_source,p.fish_harvested,
  f.farmer_id, f.id,f.farm_name,o.farmer_id, o.f_id FROM farms_owners o inner join farms f on o.farmer_id=f.farmer_id inner join farms_production_cycles p on f.id=p.farm_id where f_id='$f_id' ";

$result = dbQuery($sql);

?> 

<div class="prepend-1 span-12">
<!--  -->
<h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Edit Additional information for:<?php echo $farm_name; ?> Farm</h6>
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-md-12">
<table class="table table-striped table-bordered table-sm">
                <thead>
               
                <th>Cycle Year</th>
                <th>Species</th>
                <th>Length of Cycle</th>
                <th>Size of ponds harvested (m2)</th>
                <th>Number of Fingerlings bought</th>
                <th>Source of Fingerlings</th>
                <th>Kgs of Feeds bought</th>
                <th>Source of Feeds</th>
                <th>Kgs of fish hasrvested</th>
                </thead>
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=pond" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
         <?php
while($row = dbFetchAssoc($result)) {
  extract($row);
  
  if ($i%2) {
    $class = 'row1';
  } else {
    $class = 'row2';
  }
  
?>
  <tr class="<?php echo $class; ?>"> 
   <td><?php echo $year; ?></td>
   <td><?php echo $species;?></td>
   <td><?php echo $length;?></td>
   <td><?php echo $ponds_harvested_size;?></td>
   <td><?php echo $fingerlings_bought;?></td>
   <td><?php echo $fingerlings_source; ?></td>
   <td><?php echo $feeds_bought;?></td>
   <td><?php echo $feeds_source;?></td>
   <td><?php echo $fish_harvested;?></td>
  </tr>
<?php
} // end while

?>
    </form>
        </form>
        </tbody></table>
<div class="form-group row">
 <p align="center"> 
  <input name="btnAddUser" type="button"   class="button" id="btnAddUser" value="Save (✔)" onClick="" class="box">
  &nbsp;&nbsp;<input name="btnCancel" type="button" id="btnCancel" class="button"  value="Back (x)" onClick="window.location.href='view.php?v=Farmer';" class="box">  
 </p>
 </div>
</div>


</div>

