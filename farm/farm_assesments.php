<?php
if (!defined('WEB_ROOT')) {
  exit;
}



$errorMessage = (isset($_GET['error']) && $_GET['error'] != '') ? $_GET['error'] : '&nbsp;';

$sql = "SELECT id, farm_name FROM farms order by id desc
limit 1";
$result = dbQuery($sql);
while($row = dbFetchAssoc($result)) {
    extract($row);
   }


?> 


<div class="prepend-1 span-12">
<h4>Assessment Remarks for:<?php echo $farm_name; ?> Farm</h4>
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-md-12">
<table class="table table-striped table-bordered">
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=assesments" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
    <div class="form-group row">
    <input class="form-control" name="id" type="hidden" id="id" value="<?php echo $id; ?>" readonly>
    <input class="form-control" name="farm_name" type="hidden" id="farm_name" value="<?php echo $farm_name; ?>" readonly >
    </div>
    <div class="form-group row" >
    <label for="assessor_observations" class="col-md-3 col-form-label">Does the farmer Qualify?
    Yes <input type="radio"  name="assessor_observations" value="Yes"> No <input type="radio"  name="assessor_observations" value="No"></label>
    </div>
    <div class="form-group row" >
    <label for="reason_farmer_qualifies" class="col-md-3 col-form-label">Reasons farmer qualifies or does not qualify :<textarea class="form-control" name="reason_farmer_qualifies" type="text" id="reason_farmer_qualifies" value="" required="" ></textarea> </label>
    </div>
    <div class="form-group row" >
      <label for="assessor_remarks" class="col-md-3 col-form-label"><b>Remarks</b><br>
   Recommended&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio"  name="assessor_remarks" value="recommended"><br> Not Recommended &nbsp;&nbsp;<input type="radio"  name="assessor_remarks" value="Not recommended"><br>Consider for lator&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio"  name="assessor_remarks" value="Consider for lator"></label>
    
    </div>

    <p align="left"> 
    <input name="btnAddUser" type="button" class="button" id="btnAddUser" value="Save (✔)" onClick="checkAssessmentForm()" class="box">
     
    </p>
   </form>
  </tbody>
  </table>
</div>


</div>