<?php
require_once '../library/config.php';
require_once '../library/functions.php';

checkUser();

$action = isset($_GET['action']) ? $_GET['action'] : '';

switch ($action) {
	
	case 'add' :
		addFarm();
		break;
		
	case 'pond' :
		addPond();
		break;

	case 'contact' :
		contactPerson();
		break;

	case 'staff' :
		farmStaff();
		break;

	case 'farminfo' :
		extFarminfo();
		break;

	case 'cycle' :
		prodCycle();
		break;

	case 'assesments' :
		assesments();
		break;

	case 'extrainfo' :
		editextfarminfo();
		break;

	case 'basicinfo' :
		editbasicfarminfo();
		break;
		
		
	case 'delete' :
		deleteLab();
		break;
    
	default :
	    // if action is not defined or unknown
		// move to main user page
		header('Location: ../index.php');
}

/*
this function will add farm entry 
*/
function addFarm()
{   
   $farmer_id =$_POST['farmer_id']; 
   $farm_name=$_POST['farm_name']; 
   $farm_county=$_POST['farm_county']; 
   $farm_subcounty=$_POST['farm_subcounty']; 
   $farm_village=$_POST['farm_village'];
   //$farm_longitude=$_POST['farm_longitude'];
   //$farm_latitude=$_POST['farm_latitude'];
   $farm_landmark=$_POST['farm_landmark'];
   //$pond_sample_temperature=$_POST['pond_sample_temperature'];
   //$pond_sample_time=$_POST['pond_sample_time'];
   $ponds_number=$_POST['ponds_number']; 
   $ponds_stocked=$_POST['ponds_stocked']; 
   $keeps_records=$_POST['keeps_records'];
   $records_kept=$_POST['records_kept']; 
   $record_keeping=$_POST['record_keeping'];  
   $has_business_plan=$_POST['has_business_plan']; 
   $business_plan_last_update=$_POST['business_plan_last_update'];
   $modified_by=$_SESSION['user_id'];
   


	$sql = "SELECT farmer_id, farm_name
	        FROM farms
			WHERE farmer_id = '$farmer_id' and farm_name = '$farm_name' ";
	$result = dbQuery($sql);
	
	if (dbNumRows($result) == 1) {
		header('Location: ../add.php?v=addfarm&error=' . urlencode('There exists another farm with that ID and Name'));	
	} else {	
	            		
		$sql   = "INSERT INTO farms (farmer_id, farm_name,farm_county, farm_subcounty, farm_village, farm_landmark,ponds_number, keeps_records, ponds_stocked, record_keeping, records_kept, has_business_plan, business_plan_last_update,modified_by,date_modified)
		          VALUES ('$farmer_id', '$farm_name', '$farm_county', '$farm_subcounty', '$farm_village', '$farm_landmark', '$ponds_number', '$keeps_records', '$ponds_stocked','$record_keeping', '$records_kept', '$has_business_plan', '$business_plan_last_update','$modified_by',NOW())";
	
		dbQuery($sql);
		header('Location: ../add.php?v=contactperson');	
	}
}
function contactPerson()
{   
  if(isset($_POST)==true && empty($_POST)==false): 
   $chkbox = $_POST['chk'];	
   $farm_id =$_POST['id']; 
   $contact_name=$_POST['contact_name'];
   $contact_telephone=$_POST['contact_telephone']; 
   $contact_gender=$_POST['contact_gender']; 
   $contact_position=$_POST['contact_position'];
    foreach($contact_name as $a => $b){ 
		$sql   = "INSERT INTO farms_contact_persons (farm_id, contact_name, contact_telephone, contact_gender, contact_position)
		          VALUES ('$farm_id[$a]', '$contact_name[$a]', '$contact_telephone[$a]', '$contact_gender[$a]', '$contact_position[$a]')";
	
		dbQuery($sql);
		header('Location: ../add.php?v=addpond');	
	}
	else:

	 endif;
	
}

/*
this function will add pond entry 
*/
function addPond()
{   
   if(isset($_POST)==true && empty($_POST)==false):
   $chkbox = $_POST['chk'];	
   $farm_id =$_POST['id']; 
   $pond_number=$_POST['pond_number'];
   $pond_area=$_POST['pond_area']; 
   $catfish_no=$_POST['catfish_no']; 
   $tilapia_no=$_POST['tilapia_no']; 
   $tilapia_fingerling_stocked=$_POST['tilapia_fingerling_stocked']; 
   $tilapia_price_per_fingerling=$_POST['tilapia_price_per_fingerling'];
   $tilapia_fingerling_stocking_weight=$_POST['tilapia_fingerling_stocking_weight'];
   $tilapia_stocking_date=$_POST['tilapia_stocking_date'];
   $catfish_fingerling_stocked=$_POST['catfish_fingerling_stocked']; 
   $catfish_price_per_fingerling=$_POST['catfish_price_per_fingerling']; 
   $catfish_fingerling_stocking_weight=$_POST['catfish_fingerling_stocking_weight'];
   $catfish_stocking_date=$_POST['catfish_stocking_date'];
   foreach($pond_number as $a => $b){ 

	$sql = "SELECT farm_id, pond_number
	        FROM farms_ponds
			WHERE farm_id = '$farm_id' and pond_number = '$pond_number' ";
	$result = dbQuery($sql);
	
	if (dbNumRows($result) == 1) {
		header('Location: ../add.php?v=addpond&error=' . urlencode('There exists another Pond with that ID and Name'));	
	} else {			
		$sql   = "INSERT INTO farms_ponds (farm_id, pond_number, pond_area, catfish_no, tilapia_no, tilapia_fingerling_stocked, tilapia_price_per_fingerling, tilapia_fingerling_stocking_weight, tilapia_stocking_date, catfish_fingerling_stocked, catfish_fingerling_stocking_weight, catfish_price_per_fingerling, catfish_stocking_date )
		          VALUES ('$farm_id[$a]', '$pond_number[$a]', '$pond_area[$a]', '$catfish_no[$a]', '$tilapia_no[$a]', '$tilapia_fingerling_stocked[$a]', '$tilapia_price_per_fingerling[$a]', '$tilapia_fingerling_stocking_weight[$a]', '$tilapia_stocking_date[$a]','$catfish_fingerling_stocked[$a]', '$catfish_fingerling_stocking_weight[$a]', '$catfish_price_per_fingerling[$a]', '$catfish_stocking_date[$a]')";
	
		dbQuery($sql);
		header('Location: ../add.php?v=farmstaff');	
    
	}

}
else:
endif;

}

function farmStaff()
{   
   if(isset($_POST)==true && empty($_POST)==false): 
   $chkbox = $_POST['chk'];	
   $farm_id =$_POST['id']; 
   $staff_name=$_POST['staff_name'];
   $staff_gender=$_POST['staff_gender']; 
   $staff_age=$_POST['staff_age']; 
   $staff_role=$_POST['staff_role'];
    foreach($staff_name as $a => $b){ 
		$sql   = "INSERT INTO farms_staff (farm_id, staff_name, staff_gender, staff_age, staff_role)
		          VALUES ('$farm_id[$a]', '$staff_name[$a]', '$staff_gender[$a]', '$staff_age[$a]', '$staff_role[$a]')";
	
		dbQuery($sql);
		header('Location: ../add.php?v=extfarminfo');	
	}
	else:

	 endif;
	
}
function extFarminfo()
{   
   $farm_id =$_POST['id']; 
   $water_availability=$_POST['water_availability'];
   $water_sources=$_POST['water_sources']; 
   $water_mechanism=$_POST['water_mechanism']; 
   $farm_equipments=$_POST['farm_equipments']; 
   $staff_total=$_POST['staff_total']; 
   $staff_fulltime=$_POST['staff_fulltime'];
   $staff_parttime=$_POST['staff_parttime'];
   $farm_greenhouse=$_POST['farm_greenhouse'];
   $has_security=$_POST['has_security'];
   $security_types=$_POST['security_types']; 
   $customers=$_POST['customers']; 
   $customers_others=$_POST['customers_others'];
   $challenges=$_POST['challenges'];
   $five_year_target=$_POST['five_year_target']; 
   $needs_to_reach_target=$_POST['needs_to_reach_target']; 
   $receive_updates=$_POST['receive_updates'];
   $can_host_trainings=$_POST['can_host_trainings'];

	$sql = "SELECT farm_id
	        FROM farms_extended_data
			WHERE farm_id = '$farm_id' ";
	$result = dbQuery($sql);
	
	if (dbNumRows($result) == 1) {
		header('Location: ../add.php?v=addfarm&error=' . urlencode('There exists another Farm record with that ID '));	
	} else {			
		$sql   = "INSERT INTO farms_extended_data (farm_id, water_availability, water_sources, water_mechanism, 
		farm_equipments, staff_total, staff_fulltime, staff_parttime,farm_greenhouse, has_security, security_types, customers_others, customers, challenges,five_year_target,needs_to_reach_target,receive_updates,can_host_trainings)
		          VALUES ('$farm_id', '$water_availability', '$water_sources', '$water_mechanism', '$farm_equipments', '$staff_total', '$staff_fulltime','$staff_parttime','$farm_greenhouse', '$has_security','$security_types', '$customers_others', '$customers','$challenges','$five_year_target','$needs_to_reach_target','$receive_updates','$can_host_trainings')";
	
		dbQuery($sql);
		header('Location: ../add.php?v=prodcycle');	
	}
}
function prodCycle()
{   
   $farm_id =$_POST['id']; 
   $year=$_POST['year'];
   $species=$_POST['species']; 
   $length=$_POST['length']; 
   $ponds_harvested_size=$_POST['ponds_harvested_size']; 
   $fingerlings_bought=$_POST['fingerlings_bought']; 
   $fingerlings_source=$_POST['fingerlings_source'];
   $feeds_bought=$_POST['feeds_bought'];
   $feeds_source=$_POST['feeds_source'];
   $fish_harvested=$_POST['fish_harvested']; 

	$sql = "SELECT farm_id, year
	        FROM farms_production_cycles
			WHERE farm_id = '$farm_id' and year = '$year' ";
	$result = dbQuery($sql);
	
	if (dbNumRows($result) == 1) {
		header('Location: ../add.php?v=prodcycle&error=' . urlencode('Production Cycle for that year already exists For the farm selected '));	
	} else {			
		$sql   = "INSERT INTO farms_production_cycles (farm_id, year, species, length, ponds_harvested_size, fingerlings_bought, fingerlings_source, feeds_bought, feeds_source, fish_harvested)
		          VALUES ('$farm_id', '$year', '$species', '$length', '$ponds_harvested_size', '$fingerlings_bought', '$fingerlings_source', '$feeds_bought', '$feeds_source','$fish_harvested')";
	
		dbQuery($sql);
		header('Location: ../add.php?v=assesments');	
	}
}
function assesments()
{   
   $farm_id =$_POST['id']; 
   $assessor_id=$_SESSION['user_id'];
   $assessor_observations=$_POST['assessor_observations']; 
   $reason_farmer_qualifies=$_POST['reason_farmer_qualifies']; 
   $assessor_remarks=$_POST['assessor_remarks'];

	$sql = "SELECT farm_id
	        FROM farms_assessments
			WHERE farm_id = '$farm_id' ";
	$result = dbQuery($sql);
	
	if (dbNumRows($result) == 1) {
		header('Location: ../add.php?v=addfarm&error=' . urlencode('Farm already assessed'));	
	} else {			
		$sql   = "INSERT INTO farms_assessments (farm_id, assessor_id, assessor_observations, reason_farmer_qualifies, assessor_remarks)
		          VALUES ('$farm_id', '$assessor_id', '$assessor_observations', '$reason_farmer_qualifies', '$assessor_remarks')";
	
		dbQuery($sql);
		header('Location: ../View.php?v=Farmer');	
	}
}

function editextfarminfo()
{   
	$farm_id = $_POST["farm_id"];
	$water_availability = $_POST['water_availability'];
	$water_sources = $_POST['water_sources'];
    $water_mechanism = $_POST['water_mechanism'];
	$date_registered = $_POST['date_registered'];
	$farm_equipments = $_POST['farm_equipments'];
	$farm_greenhouse = $_POST['farm_greenhouse'];
	$staff_total = $_POST['staff_total'];
	$staff_fulltime = $_POST['staff_fulltime'];
	$staff_parttime = $_POST["staff_parttime"];
    $main_feeds = $_POST['main_feeds'];
	$has_security = $_POST['has_security'];
	$security_types = $_POST['security_types'];
	$customers = $_POST['customers'];
    $customers_others = $_POST['customers_others'];
	$challenges = $_POST['challenges'];
	$five_year_target = $_POST['five_year_target'];
	$needs_to_reach_target=$_POST['needs_to_reach_target']; 
	$receive_updates = $_POST['receive_updates'];
	$can_host_trainings = $_POST['can_host_trainings'];
	$modified_by=$_SESSION['user_id']; 
	
	
		$sql   = "UPDATE farms_extended_data

		   SET  water_availability = '$water_availability',
	            water_sources = '$water_sources',
                water_mechanism = '$water_mechanism',
	            farm_equipments = '$farm_equipments',
	            farm_greenhouse = '$farm_greenhouse',
	            staff_total = '$staff_total',
	            staff_fulltime = '$staff_fulltime',
	            staff_parttime = '$staff_parttime',
                main_feeds = '$main_feeds',
	            has_security = '$has_security',
	            security_types = '$security_types',
	            customers = '$customers',
                customers_others = '$customers_others',
	            challenges = '$challenges',
	            five_year_target = '$five_year_target',
	            needs_to_reach_target='$needs_to_reach_target',
	            receive_updates = '$receive_updates',
	            can_host_trainings = '$can_host_trainings',
	            modified_by='$modified_by', 
	            date_modified=NOW()


				WHERE farm_id = '$farm_id'";
	
		dbQuery($sql);
		header('Location: ../view.php?v=Farmer');	
	
}
function editbasicfarminfo()
{   
	$farm_id = $_POST["id"];
	$farm_name = $_POST['farm_name'];
	$farm_county = $_POST['farm_county'];
    $farm_subcounty = $_POST['farm_subcounty'];
	$farm_village = $_POST['farm_village'];
	$farm_longitude = $_POST['farm_longitude'];
	$farm_latitude = $_POST['farm_latitude'];
	$farm_landmark = $_POST['farm_landmark'];
	$pond_sample_temperature = $_POST["pond_sample_temperature"];
    $pond_sample_time = $_POST['pond_sample_time'];
	$ponds_number = $_POST['ponds_number'];
	$keeps_records = $_POST['keeps_records'];
    $record_keeping = $_POST['record_keeping'];
	$records_kept = $_POST['records_kept'];
	$has_business_plan = $_POST['has_business_plan'];
	$business_plan_last_update=$_POST['business_plan_last_update'];
	$modified_by=$_SESSION['user_id']; 
	
	
		$sql   = "UPDATE farms

		   SET  farm_name = '$farm_name',
	            farm_county = '$farm_county',
                farm_subcounty = '$farm_subcounty',
	            farm_village = '$farm_village',
	            farm_longitude = '$farm_longitude',
	            farm_latitude = '$farm_latitude',
	            farm_landmark = '$farm_landmark',
	            pond_sample_temperature = '$pond_sample_temperature',
                pond_sample_time = '$pond_sample_time',
	            ponds_number = '$ponds_number',
	            keeps_records = '$keeps_records',
                record_keeping = '$record_keeping',
	            records_kept = '$records_kept',
	            has_business_plan = '$has_business_plan',
	            business_plan_last_update='$business_plan_last_update',
	            modified_by='$modified_by', 
	            date_modified=NOW()


				WHERE id = '$farm_id'";
	
		dbQuery($sql);
		header('Location: ../view.php?v=Farmer');	
	
}


/*
function used to delete farm
*/
function deleteLab()
{
	if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
		$id = (int)$_GET['id'];
	} else {
		header('Location: index.php');
	}
	
	$sql = "DELETE FROM tbl_depts 
	        WHERE id = $id";
	dbQuery($sql);
	
	header('Location: ../menu.php?v=LABS');
}
?>