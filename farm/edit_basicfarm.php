<?php
require_once 'farmer/edittab.php';
if (!defined('WEB_ROOT')) {
  exit;
}

$errorMessage = "";

// $fid = "SELECT farmer_id  FROM farms_owners where f_id='$f_id'";
// $result1 = dbQuery($fid);
// while($row = dbFetchAssoc($result1)) {
//   extract($row);

//   }
  $sql="SELECT f.farm_name,f.farm_county,f.farm_subcounty,f.farm_village,f.farm_landmark,f.keeps_records,f.farm_longitude,f.farm_latitude,f.record_keeping,f.records_kept,f.has_business_plan,f.business_plan_last_update,f.ponds_number,f.ponds_stocked,f.pond_sample_temperature,f.pond_sample_time,f.farmer_id, f.id,o.farmer_id, o.f_id FROM farms f inner join farms_owners o on f.farmer_id=o.farmer_id where f_id='$f_id' ";

$result = dbQuery($sql);
?> 

<div class="prepend-1 span-12">
<?php
if(dbAffectedRows() == 1){
while($row = dbFetchAssoc($result)){
extract($row);

?>
<h4>&nbsp;&nbsp;&nbsp;Edit Basic information</h4>
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-md-12">
<table class="table table-striped table-bordered">
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=basicinfo" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
 
   <div class="form-group row">
   <input class="form-control" name="id" type="hidden" id="id" value="<?php echo $id; ?>" readonly></label>
   <input class="form-control" name="farmer_id" type="hidden" id="farmer_id" value="<?php echo $farmer_id; ?>" readonly></label>
   </div>
    <!-- <div class="form-group row">
        <p onclick="geoFindMe()" type="button" class="btn btn-default btn-lg">
      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span></p>
    <div id="out"></div>
  </div> -->
   <div class="form-group row">
   <label for="farm_name" class="col-sm-3 col-form-label">Name of Farm:<input class="form-control" name="farm_name" type="text" id="farm_name" value="<?php echo $farm_name; ?>" required ></label>
   </div>
   <div class="form-group row" >
    <input class="form-control" name="farm_longitude" type="hidden" id="farm_longitude" value="<?php echo $farm_longitude; ?>" required="" >
   <input class="form-control" name="farm_latitude" type="hidden" id="farm_latitude" value="<?php echo $farm_latitude; ?>" required="" >
   <label for="farm_county" class="col-sm-3 col-form-label">County :<select class="form-control" name="farm_county" type="text" id="farm_county"  required="" ><option  value="<?php echo $farm_county; ?>"><?php echo $farm_county; ?></option><?php getCounties();?></select></label></label>
   <label for="farm_subcounty" class="col-sm-3 col-form-label">SubCounty::<select class="form-control" name="farm_subcounty" type="text" id="farm_subcounty"  required="" ><option  value="<?php echo $farm_subcounty; ?>"><?php echo $farm_subcounty; ?></option><?php getSubcounties();?></select></label></label>
   <label for="farm_village" class="col-sm-3 col-form-label">Village:<input class="form-control" name="farm_village" type="text" id="farm_village" value="<?php echo $farm_village; ?>" required="" ></label> 
  </div>

   <div class="form-group row" >
   <!-- <label for="pond_sample_temperature" class="col-sm-3 col-form-label">Temperature of sampled pond:<input class="form-control" name="pond_sample_temperature" type="hidden" id="pond_sample_temperature" value="<?php echo $pond_sample_temperature; ?>" required="" ></label>
   <label for="pond_sample_time" class="col-sm-3 col-form-label">Time of day:<select class="form-control" name="pond_sample_time" type="hidden" id="pond_sample_time" value="<?php echo $pond_sample_time; ?>" required="" >
    <option value="<?php echo $pond_sample_time; ?>" ><?php echo $pond_sample_time; ?></option>
    <option value="Morning">Morning</option>
    <option value="Noon">Noon</option>
    <option value="Evening">Evening</option>
    </select></label> -->
    <label for="farm_landmark" class="col-sm-3 col-form-label">Landmark:<input class="form-control" name="farm_landmark" type="text" id="farm_landmark" value="<?php echo $farm_landmark; ?>" required="" ></label>
    <label for="ponds_number" class="col-sm-3 col-form-label">Total No. of Ponds:<input class="form-control" name="ponds_number" type="text" id="ponds_number" value="<?php echo $ponds_number; ?>" required="" ></label>
   <label for="ponds_stocked" class="col-sm-3 col-form-label">Ponds Stocked:<input class="form-control" name="ponds_stocked" type="text" id="ponds_stocked" value="<?php echo $ponds_stocked; ?>" required="" ></label>
   </div>
  <div class="form-group row" >
  <label for="keeps_records" class="col-sm-3 col-form-label">Do they keep records?</label>
    Yes <input type="radio" onclick="javascript:yesnoCheck();" name="keeps_records" value="Yes" id="yesCheck"> No <input type="radio" onclick="javascript:yesnoCheck();" name="keeps_records" value="No" id="noCheck">
    <div id="ifYes" style="visibility:hidden">
    <label for="record_keeping" class="col-sm-3 col-form-label">How are records kept:<select class="form-control" id ="record_keeping" name="record_keeping">
    <option value="<?php echo $record_keeping; ?>" ><?php echo $record_keeping; ?></option>
    <option value="Manually">Manually</option>
    <option value="Computer">Computer</option>
    </select></label>
        
     <label for="records_kept" class="col-sm-3 col-form-label">What types of records are kept:<select class="form-control" name="records_kept">
    <option value="<?php echo $records_kept; ?>" ><?php echo $records_kept; ?></option>
    <option value="Bank">Bank</option>
    <option value="Book">Book</option>
    <option value="Book and File">Book and File</option>
    <option value="Book/Computer">Book/Computer</option>
    </select></label>
  </div>

  </div>
  <div class="form-group row" >
  <label for="has_business_plan" class="col-sm-3 col-form-label">Does he/she have a business plan?</label>
    Yes <input type="radio" onclick="javascript:businessplan();" name="has_business_plan" value="Yes" id="yes"> No <input type="radio" onclick="javascript:businessplan();" name="has_business_plan" value="No" id="no">
    <div id="iftrue" style="visibility:hidden">
    <label for="business_plan_last_update" class="col-sm-3 col-form-label">When was the Last time of business plan update:<input class="form-control" name="business_plan_last_update" type="date" id="business_plan_last_update" value="<?php echo $business_plan_last_update; ?>" required="" ></label>
        
  </div>
  
  </div>

</form>
 </tbody>

</table>
<div class="form-group row">
 <p align="center"> 
  <input name="btnAddUser" type="button"   class="button" id="btnAddUser" value="Save (✔)" onClick="checkeditFarmForm()" class="box">
  &nbsp;&nbsp;<input name="btnCancel" type="button" id="btnCancel" class="button"  value="Cancel (x)" onClick="window.location.href='edit.php?v=editfarmer';" class="box">  
 </p>
 </div>
</div>
<?php 

}//while
}else {
?>
<p> There was an error in processing the request.</p>
<div class="form-group " >
 <p align="center"> 
  &nbsp;&nbsp;<input name="btnCancel" type="button" id="btnCancel" class="button"  value="Back" onClick="window.location.href='view.php?v=Farmer';" class="box">  
 </p>
 </div>
<?php 
} 
?>
</div>

