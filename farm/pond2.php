<?php
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = (isset($_GET['error']) && $_GET['error'] != '') ? $_GET['error'] : '&nbsp;';
$sql = "SELECT id, farm_name FROM farms order by id desc
limit 1";
$result = dbQuery($sql);

   while($row = dbFetchAssoc($result)) {
    extract($row);
   
   
   
   }


?> 
 <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pond information for <?php echo $farm_name; ?> Farm</h6>
<div class="prepend-1 span-12">
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-md-12">
<table class="table table-striped table-bordered">
   <tbody>
      <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=pond" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
         <thead>
                <!-- <th>Farm ID</th> -->
                <!-- <th>Name of Farm</th> -->
                <th>Pond No</th>
                <th>Pond area(M2)</th>
                <th>Catfish Stocking date</th>
                <th>Catfish Stocked</th>
                <th>No of Catfish</th>
                <th>Tilapia stocking date</th>
                <th>Tilapia Stocked</th>
                <th>No of Tilapia</th>
                
                <th></th>
          </thead>
          <div class="input-group control-group after-add-more row" ">
            <tr>
                <input type="hidden" class="form-control " name="id"  id="id" value="<?php echo $id; ?>"/>
                <!-- <td><input type="text" class="form-control" value=""/></td> -->
                <td><input type="text" class="form-control" name="pond_number"  id="pond_number" value=""/></td>
                <td><input type="text" class="form-control" name="pond_area"  id="pond_area" value=" "/></td>
                <td><input type="date" class="form-control" name="catfish_stocking_date" id="catfish_stocking_date" value=" "/></td>
                <td><input type="text" class="form-control" name="catfish_fingerling_stocked"  id="catfish_fingerling_stocked" value=" "/></td>
                <td><input type="text" class="form-control" name="catfish_no"  id="catfish_no" value=" "/></td>
                <td><input type="date" class="form-control" name="tilapia_stocking_date" type="date" id="tilapia_stocking_date" value=" "/></td>
                <td><input type="text" class="form-control" name="tilapia_fingerling_stocked" id="tilapia_fingerling_stocked" value=" "/></td>
                <td><input type="text" class="form-control" name="tilapia_no" id="tilapia_no" value=" "/></td>
                
                <td><button class="btn glyphicon glyphicon-plus btn-success add-more form-control" type="button"></button></td>
           </tr>
          </div>
         
         <!-- Copy Fields -->
         <div class="copy hide">
           <div class="control-group input-group row" style="margin-top:1px">
            <tr>
                <input type="hidden" class="form-control " name="id" id="id" value="<?php echo $id; ?>"/>
                <!-- <td><input type="text" class="form-control " name="pond_number" type="text" id="pond_number" value="<?php echo $farm_name; ?>"/></td> -->
                <td><input type="text" class="form-control " name="pond_number"  id="pond_number" value=" "/></td>
                <td><input type="text" class="form-control " name="pond_area"  id="pond_area" value=" "/></td>
                <td><input type="text" class="form-control " name="catfish_stocking_date"  id="catfish_stocking_date" value=" "/></td>
                <td><input type="text" class="form-control " name="catfish_fingerling_stocked"  id="catfish_fingerling_stocked" value=" "/></td>
                <td><input type="text" class="form-control " name="catfish_no"  id="catfish_no" value=" "/></td>
                <td><input type="text" class="form-control " name="tilapia_stocking_date"  id="tilapia_stocking_date" value=" "/></td>
                <td><input type="text" class="form-control " name="tilapia_fingerling_stocked"  id="tilapia_fingerling_stocked" value=" "/></td>
                <td><input type="text" class="form-control " name="tilapia_no"  id="tilapia_no" value=" "/></td>
                <td><button class="btn btn-danger remove glyphicon glyphicon-remove" type="button"></button></td>
            </tr>
           </div>
         </div>
    
    
    </form>
    </tbody>
    </table>
    
    <p align="center"> 
          <input name="btnAddUser" type="button"   class="button" id="btnAddUser" value="Save (✔)" onClick="checkAddPondForm();" class="box">
          &nbsp;&nbsp;<input name="btnCancel" type="button" id="btnCancel" class="button"  value="Cancel (x)" onClick="window.location.href='add.php?v=addfarmer';" class="box">  
    </p>
    </div>
</div>
