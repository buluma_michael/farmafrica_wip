<?php
require_once 'farmer/edittab.php';
if (!defined('WEB_ROOT')) {
  exit;
}

$errorMessage = "";

$fid = "SELECT farmer_id  FROM farms_owners where f_id='$f_id'";
$result1 = dbQuery($fid);
while($row = dbFetchAssoc($result1)) {
  extract($row);

  }
  $sql="SELECT p.farm_id,p.pond_number,p.pond_area,p.catfish_no,p.tilapia_no,p.tilapia_fingerling_stocked,p.tilapia_stocking_date,p.catfish_fingerling_stocked,p.catfish_stocking_date,
  f.farmer_id, f.id,f.farm_name,o.farmer_id, o.f_id FROM farms_owners o inner join farms f on o.farmer_id=f.farmer_id inner join farms_ponds p on f.id=p.farm_id where f_id='$f_id' ";

$result = dbQuery($sql);

?> 

<div class="prepend-1 span-12">
<!--  -->
<h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Edit Additional information for:<?php echo $farm_name; ?> Farm</h6>
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-md-12">
<table class="table table-striped table-bordered table-sm">
                <thead>
               
                <th>Pond Identifier</th>
                <th>Pond area(M2)</th>
                <th>Catfish Stocking date</th>
                <th>Catfish Stocked</th>
                <th>Current No of Catfish</th>
                <th>Tilapia stocking date</th>
                <th>Tilapia Stocked</th>
                <th>Current No of Tilapia</th>
                </thead>
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=pond" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
         <?php
while($row = dbFetchAssoc($result)) {
  extract($row);
  
  if ($i%2) {
    $class = 'row1';
  } else {
    $class = 'row2';
  }
  
?>
  <tr class="<?php echo $class; ?>"> 
   <td><?php echo $pond_number; ?></td>
   <td><?php echo $pond_area;?></td>
   <td><?php echo $catfish_stocking_date;?></td>
   <td><?php echo $catfish_fingerling_stocked;?></td>
   <td><?php echo $catfish_no;?></td>
   <td><?php echo $tilapia_stocking_date; ?></td>
   <td><?php echo $tilapia_fingerling_stocked;?></td>
   <td><?php echo $tilapia_no;?></td>
  </tr>
<?php
} // end while

?>
    </form>
        </form>
        </tbody></table>
<div class="form-group row">
 <p align="center"> 
  <input name="btnAddUser" type="button"   class="button" id="btnAddUser" value="Save (✔)" onClick="" class="box">
  &nbsp;&nbsp;<input name="btnCancel" type="button" id="btnCancel" class="button"  value="Back (x)" onClick="window.location.href='view.php?v=Farmer';" class="box">  
 </p>
 </div>
</div>


</div>

