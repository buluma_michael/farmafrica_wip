<?php
if (!defined('WEB_ROOT')) {
	exit;
}



$errorMessage = (isset($_GET['error']) && $_GET['error'] != '') ? $_GET['error'] : '&nbsp;';

$sql = "SELECT id, farm_name FROM farms order by id desc
limit 1";
$result = dbQuery($sql);
while($row = dbFetchAssoc($result)) {
    extract($row);
   }


?> 
 
<div class="prepend-1 span-12">
<h4>Additional information for:<?php echo $farm_name; ?> Farm</h4>
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-md-12">
<table class="table table-striped table-bordered">
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=farminfo" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
 
   <div class="form-group row">
   <input class="form-control" name="id" type="hidden" id="id" value="<?php echo $id; ?>" readonly></label>
   <input class="form-control" name="farm_name" type="hidden" id="farm_name" value="<?php echo $farm_name; ?>" readonly ></label>
   </div>
   <div class="form-group row">
   <label for="water_availability" class="col-md-3 col-form-label">Water availability:<select class="form-control" name="water_availability">
    <option ></option>
    <option value="Permanent">Permanent</option>
    <option value="Seasonal">Seasonal</option>
    </select></label>

    <label for="water_sources" class="col-md-3 col-form-label">Sources of Water:<select class="form-control" name="water_sources">
    <option ></option>
    <option value="River/Stream">River/Stream</option>
    <option value="Ground water">Ground water</option>
    <option value="Piped">Piped</option>
    <option value="Borehole">Borehole</option>
    <option value="Other">Other</option>
    </select></label>

    <label for="water_mechanism" class="col-md-3 col-form-label">How water gets to pond:<select class="form-control" name="water_mechanism">
    <option ></option>
    <option value="Gravity Fed">Gravity Fed</option>
    <option value="Pumping">Pumping</option>
    </select></label>

    <label for="farm_equipments" class="col-md-3 col-form-label">Essential equipments on farm:<select class="form-control" name="farm_equipments">
    <option ></option>
    <option value="Weighing scale">Weighing scale</option>
    <option value="Thermometre">Thermometre</option>
    <option value="Seine Net">Seine Net</option>
    <option value="Other">Other</option>
    </select></label>
    </div>

    <div class="form-group row">
    <label for="staff_total" class="col-md-3 col-form-label">Total No. of Staff :<input class="form-control" name="staff_total" type="number" id="staff_total" value="" required="" ></label>
    <input class="form-control" name="staff_fulltime" type="hidden" id="staff_fulltime" value="" required="" >
   <input class="form-control" name="staff_parttime" type="hidden" id="staff_parttime" value="" required="" >
    <label for="farm_greenhouse" class="col-md-3 col-form-label">Do you farm fish in a greenhouse?<select class="form-control" name="farm_greenhouse">
    <option ></option>
    <option value="Yes">Yes</option>
    <option value="No">No</option>
    </select></label>
    </div>
    <div class="form-group row">
    <label for="has_security" class="col-md-3 col-form-label">Is there security on the farm?..........
    Yes <input type="radio" onclick="javascript:yesnoCheck();" name="has_security" value="Yes" id="yesCheck"> No <input type="radio" onclick="javascript:yesnoCheck();" name="has_security" value="No" id="noCheck"></label>
    <div id="ifYes" style="visibility:hidden">
    <label for="security_types" class="col-md-3 col-form-label">What type of security?<select class="form-control" id ="security_types" name="security_types">
    <option></option>
    <option value="Guard">Guard</option>
    <option value="Fencing">Fencing</option>
    </select></label>
    </div>

    <label for="customers" class="col-md-3 col-form-label">Whom do you sell the fish to?<select class="form-control" name="customers">
    <option ></option>
    <option value="Local Consumers">Local Consumers</option>
    <option value="Traders/Middlemen">Traders/Middlemen</option>
    <option value="Hotels">Hotels</option>
    <option value="Others">Others</option>
    </select></label>

   <input class="form-control" name="customers_others" type="hidden" id="customers_others" value="" required="" ></label>
    
   </div>

   <div class="form-group row" >
   <label for="challenges" class="col-md-6 col-form-label">Challenges faced in fish farming:<textarea class="form-control" name="challenges" type="text" id="challenges" value="" required="" ></textarea> </label>
   </div>
   <div class="form-group row" >
   <label for="five_year_target" class="col-md-6 col-form-label">Target of your fish farming enterprise in 5 years in size and turnover:<textarea class="form-control" name="five_year_target" type="text" id="five_year_target" value="" required="" ></textarea> </label>
   <label for="needs_to_reach_target" class="col-md-6 col-form-label">What do you need to accomplish this target?<textarea class="form-control" name="needs_to_reach_target" type="text" id="needs_to_reach_target" value="" required="" ></textarea> </label>
   </div>
   <div class="form-group row" >
   <label for="receive_updates" class="col-md-3 col-form-label">Would you like to receive updates from Farm Africa Aquaculture project?<select class="form-control" name="receive_updates">
  <option ></option>
  <option value="Yes">Yes</option>
  <option value="No">No</option>
  </select></label>
  <label for="can_host_trainings" class="col-md-3 col-form-label">Are you willing to host practical trainings on your farm?<select class="form-control" name="can_host_trainings">
  <option ></option>
  <option value="Yes">Yes</option>
  <option value="No">No</option>
  </select></label>

  </div>
 <p align="center"> 
  <input name="btnAddUser" type="button"   class="button" id="btnAddUser" value="Save (✔)" onClick="checkAddextdataForm();" class="box">
   
 </p>
</form>
 </tbody>
</table>
</div>


</div>