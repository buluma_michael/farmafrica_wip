<?php
if (!defined('WEB_ROOT')) {
  exit;
}



$$errorMessage = (isset($_GET['error']) && $_GET['error'] != '') ? $_GET['error'] : '&nbsp;';
$sql = "SELECT id, farm_name FROM farms order by id desc
limit 1";
$result = dbQuery($sql);

   while($row = dbFetchAssoc($result)) {
    extract($row);
   
   
   
   }
?>
 
<div class="prepend-1 span-12">
<h4>Add Pond information for:<?php echo $farm_name; ?> Farm</h4>
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-sm-12">
<table class="table table-striped table-bordered table-sm">
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=pond" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">

                <thead>
                <th></th>
                <th>Pond Identifier</th>
                <th>Pond area(M2)</th>
                <th>Catfish Stocking date</th>
                <th>Catfish Stocked</th>
                <th>Current No of Catfish</th>
                <th>Tilapia stocking date</th>
                <th>Tilapia Stocked</th>
                <th>Current No of Tilapia</th>
                </thead>
            <fieldset class="row2">
                <p> 
                    <!-- <p>(All actions apply only to entries with check marked check boxes only.)</p> -->
                </p>
               <table id="dataTable" class="form-group input-group control-group row" >
                  <tbody>
                
                       <div class="form-group row col-md-12" style="margin-top:-30px">
                       <tr>
                        <td><input type="checkbox" required="required" name="chk[]" checked="checked" /></td>
                        <td>
                           
                            <input type="hidden" required="required" name="farm_name[]" value="<?php echo $farm_name; ?>">
                         </td>
                         <td>
                            
                            <input type="hidden" class="form-control" required="required" name="id[]" value="<?php echo $id; ?>">
                         </td>
                        <td>
                          
                            <input type="text" class="form-control" required="required" name="pond_number[]">
                         </td>
                         <td>
                           
                            <input type="text" class="form-control" required="required"  name="pond_area[]">
                         </td>
                         <td>
                           
                            <input type="date" class="form-control" required="required"  name="catfish_stocking_date[]">
                         </td>
                         <td>
                          
                            <input type="number" class="form-control" required="required" name="catfish_fingerling_stocked[]">
                         </td>
                        <td>
                          
                            <input type="number" class="form-control" required="required" name="catfish_no[]">
                         </td>
                         <td>
                           
                            <input type="date" class="form-control" required="required"  name="tilapia_stocking_date[]">
                         </td>
                         <td>
                           
                            <input type="number" class="form-control" required="required"  name="tilapia_fingerling_stocked[]">
                         </td>
                         <td>
                          
                            <input type="number" class="form-control" required="required" name="tilapia_no[]">
                         </td>
                            
                    </tr>
                    </div>
               
                    </tbody>
                </table>
                <div class="clear"></div>
            </fieldset>
            
            <td><button class="btn glyphicon glyphicon-plus btn-success " type="button" onClick="addRow('dataTable')"></button></td>
                        <td><button class="btn btn-danger remove glyphicon glyphicon-remove" type="button" onClick="deleteRow('dataTable')"></button></td> 
            <input class="submit" type="submit" onClick="checkAddPondForm();" &raquo;" />
            
            
            <div class="clear"></div>
        </form>
        </tbody></table></div>
</div>
