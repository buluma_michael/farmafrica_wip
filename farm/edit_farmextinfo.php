<?php
require_once 'farmer/edittab.php';
if (!defined('WEB_ROOT')) {
  exit;
}

$errorMessage = "";

// $fid = "SELECT farmer_id  FROM farms_owners where f_id='$f_id'";
// $result1 = dbQuery($fid);
// while($row = dbFetchAssoc($result1)) {
//   extract($row);

//   }

$sql = "SELECT e.farm_id,e.water_availability,e.water_sources, e.water_mechanism,e.farm_greenhouse, e.farm_equipments,e.staff_total,
      e.staff_fulltime,e.staff_parttime,e.has_security,e.security_types,e.customers,e.customers_others,e.challenges,e.five_year_target,e.needs_to_reach_target,e.receive_updates,e.can_host_trainings,o.f_id,f.id,o.farmer_id,f.farmer_id,f.farm_name
      FROM farms_owners o inner join farms f on o.farmer_id = f.farmer_id inner join farms_extended_data e on f.id=e.farm_id  where f_id='$f_id'";
$result = dbQuery($sql);
?> 

<div class="prepend-1 span-12">
<?php
if(dbAffectedRows() == 1){
while($row = dbFetchAssoc($result)){
extract($row);

?>
<h4>&nbsp;&nbsp;&nbsp;Edit Extended Farm information</h4>
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-md-12">
<table class="table table-striped table-bordered">
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=extrainfo" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
    <input class="form-control" name="farm_id" type="hidden" id="farm_id" value="<?php echo $farm_id; ?>" readonly >
    <div class="form-group row">
    <label for="water_availability" class="col-md-3 col-form-label">Water availability:<select class="form-control" name="water_availability" value="<?php echo $water_availability; ?>">
    <option value="<?php echo $water_availability; ?>"><?php echo $water_availability; ?></option>
    <option value="Permanent">Permanent</option>
    <option value="Seasonal">Seasonal</option>
    </select></label>

    <label for="water_sources" class="col-md-3 col-form-label">Sources of Water:<select class="form-control" name="water_sources" value="<?php echo $water_sources; ?>">
    <option value="<?php echo $water_sources; ?>"><?php echo $water_sources; ?></option>
    <option value="River/Stream">River/Stream</option>
    <option value="Ground water">Ground water</option>
    <option value="Piped">Piped</option>
    <option value="Borehole">Borehole</option>
    <option value="Other">Other</option>
    </select></label>

    <label for="water_mechanism" class="col-md-3 col-form-label">How water gets to pond:<select class="form-control" name="water_mechanism" value="<?php echo $water_mechanism; ?>">
    <option value="<?php echo $water_mechanism; ?>"><?php echo $water_mechanism; ?></option>
    <option value="Gravity Fed">Gravity Fed</option>
    <option value="Pumping">Pumping</option>
    </select></label>

    <label for="farm_equipments" class="col-md-3 col-form-label">Essential equipments on farm:<select class="form-control" name="farm_equipments" value="<?php echo $farm_equipments; ?>">
    <option value="<?php echo $farm_equipments; ?>"><?php echo $farm_equipments; ?></option>
    <option value="Weighing scale">Weighing scale</option>
    <option value="Thermometre">Thermometre</option>
    <option value="Seine Net">Seine Net</option>
    <option value="Other">Other</option>
    </select></label>
    </div>

    <div class="form-group row">
    <label for="staff_total" class="col-md-3 col-form-label">Total Number of Staff :<input class="form-control" name="staff_total" type="number" id="staff_total" value="<?php echo $staff_total; ?>" required="" ></label>
    <!-- <label for="staff_fulltime" class="col-md-3 col-form-label">No. of fulltime staff:<input class="form-control" name="staff_fulltime" type="text" id="staff_fulltime" value="<?php echo $staff_fulltime; ?>" required="" ></label>
    <label for="staff_parttime" class="col-md-3 col-form-label">No. of parttime staff:<input class="form-control" name="staff_parttime" type="text" id="staff_parttime" value="<?php echo $staff_parttime; ?>" required="" ></label> -->
    <label for="farm_greenhouse" class="col-md-3 col-form-label">Do you farm fish in a greenhouse?<select class="form-control" name="farm_greenhouse" >
    <option value="<?php echo $farm_greenhouse; ?>"><?php echo $farm_greenhouse; ?></option>
    <option value="Yes">Yes</option>
    <option value="No">No</option>
    </select></label>
    <label for="has_security" class="col-md-3 col-form-label">Is there security on the farm?..............
    Yes <input type="radio" onclick="javascript:yesnoCheck();" name="has_security" value="<?php echo $has_security; ?>" id="yesCheck"> No <input type="radio" onclick="javascript:yesnoCheck();" name="has_security" value="<?php echo $has_security; ?>" id="noCheck"></label>
    <div id="ifYes" style="visibility:hidden">
    <label for="security_types" class="col-md-3 col-form-label">What type of security?<select class="form-control" id ="security_types" name="security_types">
    <option value="<?php echo $security_types; ?>"><?php echo $security_types; ?></option>
    <option value="Guard">Guard</option>
    <option value="Fencing">Fencing</option>
    </select></label>
    </div>
    <div class="form-group row">
    
    </div>
    </div>
    <div class="form-group row">
    <label for="customers" class="col-md-3 col-form-label">Whom do you sell the fish to?<select class="form-control" name="customers">
    <option value="<?php echo $customers; ?>"><?php echo $customers; ?></option>
    <option value="Local Consumers">Local Consumers</option>
    <option value="Traders/Middlemen">Traders/Middlemen</option>
    <option value="Hotels">Hotels</option>
    <option value="Others">Others</option>
    </select></label>

    <!-- <label for="customers_others" class="col-md-3 col-form-label">Other Customers:<input class="form-control" name="customers_others" type="text" id="customers_others" value="<?php echo $customers_others; ?>" required="" ></label> -->
    
   </div>

   <div class="form-group row" >
   <label for="challenges" class="col-md-6 col-form-label">Challenges faced in fish farming:<textarea class="form-control" name="challenges" type="text" id="challenges" required="" ><?php echo $challenges; ?></textarea> </label>
   </div>
   <div class="form-group row" >
   <label for="five_year_target" class="col-md-6 col-form-label">Target of your fish farming enterprise in 5 years in size and turnover:<textarea class="form-control" name="five_year_target" type="text" id="five_year_target"  required="" ><?php echo $five_year_target; ?></textarea> </label>
   <label for="needs_to_reach_target" class="col-md-6 col-form-label">What do you need to accomplish this target?<textarea class="form-control" name="needs_to_reach_target" type="text" id="needs_to_reach_target"  required="" ><?php echo $needs_to_reach_target; ?></textarea> </label>
   </div>
   <div class="form-group row" >
   <label for="receive_updates" class="col-md-3 col-form-label">Would you like to receive updates from Farm Africa Aquaculture project?<select class="form-control" name="receive_updates">
  <option value="<?php echo $receive_updates; ?>" ><?php echo $receive_updates; ?></option>
  <option value="Yes">Yes</option>
  <option value="No">No</option>
  </select></label>
  <label for="can_host_trainings" class="col-md-3 col-form-label">Are you willing to host practical trainings on your farm?<select class="form-control" name="can_host_trainings">
  <option value="<?php echo $can_host_trainings; ?>"><?php echo $can_host_trainings; ?></option>
  <option value="Yes">Yes</option>
  <option value="No">No</option>
  </select></label>

  </div>

</form>
 </tbody>

</table>
<div class="form-group row">
 <p align="center"> 
  <input name="btnAddUser" type="button"   class="button" id="btnAddUser" value="Save (✔)" onClick="checkeditExtdataForm()" class="box">
  &nbsp;&nbsp;<input name="btnCancel" type="button" id="btnCancel" class="button"  value="Cancel (x)" onClick="window.location.href='edit.php?v=editfarmer';" class="box">  
 </p>
 </div>
</div>
<?php 

}//while
}else {
?>
<p> There was an error in processing the request.</p>
<div class="form-group " >
 <p align="center"> 
  &nbsp;&nbsp;<input name="btnCancel" type="button" id="btnCancel" class="button"  value="Back" onClick="window.location.href='view.php?v=Farmer';" class="box">  
 </p>
 </div>
<?php 
} 
?>
</div>

