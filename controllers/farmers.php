<?php
require_once './tables/farmers.php';
require_once './inc/data.php';
class FarmersController{

	private $model;

	public function __construct() {

    } 

    public function index(){
        $userfilter = isset($_POST['filter']) ? $_POST['filter'] : array(); // what the user has filtered

        $data = Data::getData('farms', '', $userfilter);

        $data['pagedata'] = array(
            'view' => 'index',
            'action' => 'index',
            'userinput' => array(),
        );
        $data['html']['filters'] = Data::renderFilters('farms');

        return $data;
    }

    public function view(){
    	$id = $_GET['id'];
    	$data = Data::getData('farms', $userfields = array(), $userfilters = array(), $ids = array($id));
        //$data['assessments'] = Data::getData('farms_assessments', $userfields = array(), $userfilters = array('farm_id' => [$id]), $ids = array());
        $data['pagedata'] = array(
            'view' => 'view',
            'action' => 'view',
            'userinput' => array(),
        );

        return $data;
    }

    public function edit(){
        $id = $_GET['id'];
        $data = [];
        if (!empty($_POST)){
            // TODO: post data to database
            // then show a success or failure message 
            $data['messages'][] = ['type' => 'success','text' => 'Successfully saved to the database'];
            // echo '<pre>';
            // print_r($_POST);
            // echo '</pre>';
        }
        //$data['dataset'] = Data::getData('farms', $userfields = array(), $userfilters = array(), $ids = array($id));
        $data['edit_farms'] = Data::renderEditForm('farms', [], [$id]);
        $data['edit_owners'] = Data::renderEditForm('farms_owners', $userfilters = array('farm_id' => [$id]), $ids = []);
        $data['edit_assessments'] = Data::renderEditForm('farms_assessments', $userfilters = array('farm_id' => [$id]), $ids = []);
        $data['edit_contact_persons'] = Data::renderEditForm('farms_contact_persons', $userfilters = array('farm_id' => [$id]), $ids = []);
        $data['edit_extended_data'] = Data::renderEditForm('farms_extended_data', $userfilters = array('farm_id' => [$id]), $ids = []);
        $data['edit_ponds'] = Data::renderEditForm('farms_ponds', $userfilters = array('farm_id' => [$id]), $ids = []);
        $data['edit_production_cycles'] = Data::renderEditForm('farms_production_cycles', $userfilters = array('farm_id' => [$id]), $ids = []);
        $data['edit_staff'] = Data::renderEditForm('farms_staff', $userfilters = array('farm_id' => [$id]), $ids = []);

        $data['pagedata'] = array(
            'view' => $_GET['view'],
            'action' => 'edit',
            'userinput' => array(),
        );

        return $data;
    }

    public function create(){
        
        
    }

    public function save($id = null){

    }

    public function delete($id){

    }


	
}