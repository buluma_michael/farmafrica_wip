<?php
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = "";


if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
  $f_id = (int)$_GET['id'];
} else {
  header('Location: ../index.php');
}
// $fid = "SELECT farmer_id  FROM farms_owners where f_id='$f_id'";
// $result1 = dbQuery($fid);
// while($row = dbFetchAssoc($result1)) {
//   extract($row);

//   }

$sql = "SELECT o.registration,o.recruitment_source,o.farmer_id, o.owner_name, o.owner_telephone,o.date_enrolled,
         o.owner_gender,o.owner_email,o.type_of_enterprise,o.finance_source,o.occupation,o.modified_by,o.date_modified,o.age,o.date_registered,u.name,u.id,r.id,r.type

        FROM farmer_reg_type r inner join farms_owners o on r.id = o.registration inner join users u on u.id=o.modified_by  where f_id='$f_id'";
$result = dbQuery($sql);

// echo '<pre>';
// print_r($farmer_id);
// echo '</pre>';
//echo $farmer_id;
// echo $sql;
?> 


<div class="prepend-1 span-12">
<p align="center"><strong><font color="#660000"><?php echo $errorMessage; ?></font></strong></p>
<?php
if(dbAffectedRows() == 1){
while($row = dbFetchAssoc($result)){
extract($row);

?>

<div class="col-md-9">
<h3>Edit Farmer Details</h3>
<table class="table table-striped table-bordered">
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>farmer/processFarmer.php?action=modify" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
  <!-- <div class="form-group">
  <label for="farm_id" class="col-md-3 col-form-label">Farm ID:<input class="form-control" name="farm_id" type="text" id="farm_id" value="<?php echo $farm_id; ?>" readonly></label>
  </div> -->
  <div class="form-group row">
  <label for="farmer_id" class="col-md-3 col-form-label">Farmer ID:<input class="form-control" name="farmer_id" type="text" id="farmer_id" value="<?php echo $farmer_id; ?>" readonly></label>
  <label for="owner_name" class="col-md-3 col-form-label">Name of farmer:<input class="form-control" name="owner_name" type="text" id="owner_name" value="<?php echo $owner_name; ?>" required ></label>
  <!-- <label for="farm_name" class="col-md-3 col-form-label">Name of farm:<input class="form-control" name="farm_name" type="text" id="farm_name" value="<?php echo $farm_name; ?>" required ></label> -->
  </div>

  <div class="form-group row">
  <label for="type" class="col-md-3 col-form-label">Type:<select class="form-control" name="type" value="<?php echo $type; ?>">
  <option  value="<?php echo $registration; ?>"><?php echo $type; ?></option><?php getRegistrationtype();?></select></label>
  <label for="date_registered" class="col-md-3 col-form-label">Registration Date: <input class="form-control" name="date_registered" type="date" id="date_registered" value="<?php echo $date_registered; ?>" ></label>
  <label for="date_enrolled" class="col-md-3 col-form-label">Date Enrolled: <input class="form-control" name="date_enrolled" type="date" id="date_enrolled" value="<?php echo $date_enrolled; ?>" ></label>
  
  </div>
<div class="form-group row" >
  <label for="age" class="col-md-3 col-form-label">Age:<input class="form-control" name="age" type="text" id="age" value="<?php echo $age; ?>" required="" ></label>
  <label for="owner_email" class="col-md-3 col-form-label">E-mail:<input class="form-control" name="owner_email" type="text" id="owner_email" value="<?php echo $owner_email; ?>" required="" ></label>
  <label for="owner_telephone" class="col-md-3 col-form-label">Telephone:<input class="form-control" name="owner_telephone" type="text" id="owner_telephone" value="<?php echo $owner_telephone; ?>" required="" ></label>
  <label for="owner_gender" class="col-md-3 col-form-label">Gender:<select class="form-control" name="owner_gender"><option  value="<?php echo $owner_gender; ?>"><?php echo $owner_gender; ?></option><?php getGender();?></select></label>
 
</div>

<div class="form-group row" >
  <label for="occupation" class="col-md-3 col-form-label">Occupation:<input class="form-control" name="occupation" type="text" id="occupation" value="<?php echo $occupation; ?>" required="" ></label>
  <label for="finance_source" class="col-sm-3 control-label">Source of Finances:<select class="form-control selectpicker" id="" name="finance_source[]" multiple="multiple"><option  value="<?php echo $finance_source; ?>"><?php echo $finance_source; ?></option><?php getFinancesources();?></select></label>
  <label for="type_of_enterprise" class="col-md-3 col-form-label">Type of enterprise:<select class="form-control" name="type_of_enterprise"><option  value="<?php echo $type_of_enterprise; ?>"><?php echo $type_of_enterprise; ?></option><?php getEnterprise();?></select></label></label>
  <label for="name" class="col-md-3 col-form-label">Name of recruiter:<input class="form-control" name="name" type="text" id="name" value="<?php echo $name; ?>" readonly="" ></label>
  
</div>


</form>

 </tbody>

</table>
<div class="form-group row" >
 <p align="center"> 
  <input name="btnAddUser" type="button"   class="button" id="btnAddUser" value="Save (✔)" onClick="checkEditProfileForm()" class="box">
  &nbsp;&nbsp;<input name="btnCancel" type="button" id="btnCancel" class="button"  value="Back" onClick="window.location.href='view.php?v=Farmer';" class="box">  
 </p>
 </div>
</div>

<?php 

}//while
}else {
?>
<p> There was an error in processing the request.</p>
<div class="form-group " >
 <p align="center"> 
  &nbsp;&nbsp;<input name="btnCancel" type="button" id="btnCancel" class="button"  value="Back" onClick="window.location.href='view.php?v=Farmer';" class="box">  
 </p>
 </div>
<?php 
} 
?>
</div>