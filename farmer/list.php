<?php
if (!defined('WEB_ROOT')) {
  exit;
}



$sql = "SELECT o.f_id,f.farmer_id,o.farmer_id, o.owner_name,o.registration,o.owner_telephone,o.owner_gender,o.owner_telephone,o.finance_source,f.modified_by,o.date_modified,f.farm_county,f.farm_subcounty,f.farm_village,f.farm_name,r.id,r.type,u.id,u.name
 from users u inner join farms f on u.id=f.modified_by inner join  farms_owners o on f.farmer_id=o.farmer_id  inner join farmer_reg_type r on o.registration=r.id
 ORDER BY f_id";

$result = dbQuery($sql);


?> 

<div class="prepend-1 span-17">
<form action="processFarmer.php?action=add" method="post"  name="frmListUser" id="frmListUser">
<div class="col-md-12">
<div class="table-responsive">
<table class="table table-striped table-bordered">
<thead>
  <tr>
   <td><b>ID</td>
   <td><b>Name of Farmer</td>
   <td><b>Name of Farm</td>
   <td><b>Gender</td>
   <td><b>County</td>
   <td><b>SubCounty</td>
   <td><b>Village</td>
   <td><b>Telephone</td>
   <td><b>Registration</td>
   <td><b>Source of Finance</td>
   <td><b>Modified By</td>
   <td><b>Date modified</td>
   <td><b>Delete</td>
  </tr>
</thead>
<tbody>
<?php
while($row = dbFetchAssoc($result)) {
  extract($row);
  
  if ($i%2) {
    $class = 'row1';
  } else {
    $class = 'row2';
  }
  
?>
  <tr class="<?php echo $class; ?>"> 
   <td><a href="javascript:editprofile(<?php echo $f_id; ?>);"><?php echo $farmer_id; ?></a></td>
   <td><?php echo $owner_name; ?></td>
   <td><?php echo $farm_name; ?></td>
   <td><?php echo $owner_gender; ?></td>
   <td><?php echo $farm_county; ?></td>
   <td><?php echo $farm_subcounty; ?></td>
   <td><?php echo $farm_village; ?></td>
   <td><?php echo $owner_telephone; ?></td>
   <td><?php echo $type; ?></td>
   <td><?php echo $finance_source; ?></td>
   <td><?php echo $name; ?></td>
   <td><?php echo $date_modified; ?></td>

   <td align="center"><a href="javascript:delete(<?php echo $id; ?>);">Delete</a></td>
  </tr>
<?php

} // end while

?>
  <tr> 
   <td colspan="14">&nbsp;#</td>
  </tr>
  <tr> 
   <td colspan="14" align="right"><input name="btnAddUser" type="button" id="btnAddUser" value="Add Farmer (+)" class="button" onClick="addfarmer()"></td>
  </tr>
 </tbody>
</table>
</div>
</div>
</form>
</div>