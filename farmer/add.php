<?php
if (!defined('WEB_ROOT')) {
  exit;
}


$errorMessage = (isset($_GET['error']) && $_GET['error'] != '') ? $_GET['error'] : '&nbsp;';

$sql = "SELECT id, type FROM farmer_reg_type";
$result = dbQuery($sql);
?> 

<h2 class="catHead">Enrol Farmer</h2>
<div class="prepend-1 span-12">
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-md-12">
<table class="table table-striped table-bordered">
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>farmer/processFarmer.php?action=add" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
  <div class="form-group row" >
  <label for="farmer_id" class="col-md-3 col-form-label">Recommended Farmer ID:<input class="form-control" name="farmer_id" type="text" id="farmer_id" value="" required ></label>
  <label for="registration" class="col-md-3 col-form-label">Registration Type:<select class="form-control" name="registration"><?php getRegistrationtype();?></select></label></label>
</div>
<div class="form-group row">
  <label for="txtFname" class="col-md-3 col-form-label">First Name:<input class="form-control" name="txtFname" type="text" id="txtFname" value="" required ></label>
  <label for="txtLname" class="col-md-3 col-form-label">Last Name: <input class="form-control" name="txtLname" type="text" id="txtLname" value="" required ></label>
  <label for="owner_telephone" class="col-md-3 col-form-label">Telephone:<input class="form-control" name="owner_telephone" type="text" id="owner_telephone" value="" required="" ></label>
  </div>
<div class="form-group row" >
  <label for="owner_email" class="col-md-3 col-form-label">Email:<input class="form-control" name="owner_email" type="email" id="owner_email" value="" ></label>
  <label for="owner_gender" class="col-md-3 col-form-label">Gender:<select class="form-control" name="owner_gender"><?php getGender();?></select></label>
  <label for="age" class="col-md-3 col-form-label">Age:<input class="form-control" name="age" type="number" id="age" value=""  ></label>
 
</div>

 <div class="form-group row" >
  <label for="type_of_enterprise" class="col-md-3 col-form-label">Type of Enterprise:<select class="form-control" name="type_of_enterprise"><?php getEnterprise();?></select></label></label>
  <label for="occupation" class="col-md-3 col-form-label">Occupation:<input class="form-control" name="occupation" type="text" id="occupation" value="" ></label>

  <label for="finance_source" class="col-sm-3 control-label">Source of Finances:<select class="form-control selectpicker" id="" name="finance_source[]" multiple="multiple"><?php getFinancesources();?></select></label>
  
  
  
  
</div>

 <p align="left"> 
<input name="btnAddUser" type="button" class="button" id="btnAddUser" value="Save (✔)" onClick="checkAddFarmerForm()" class="box">
  &nbsp;&nbsp;<input name="btnCancel" type="button" id="btnCancel" class="button"  value="Cancel" onClick="window.location.href='view.php?v=Farmer';" class="box">  
 </p>
</form>
 </tbody>
</table>
</div>


</div>