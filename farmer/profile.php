<?php
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = "";
if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
  $f_id = (int)$_GET['id'];
} else {
  header('Location: ../index.php');
}
$fid = "SELECT farmer_id  FROM farms_owners where f_id='$f_id'";

$result1 = dbQuery($fid);
while($row = dbFetchAssoc($result1)) {
  extract($row);
  }
$sqlpond="SELECT p.farm_id,p.pond_number,p.pond_area,p.catfish_no,p.tilapia_no,p.tilapia_fingerling_stocked,p.tilapia_stocking_date,p.catfish_fingerling_stocked,p.catfish_stocking_date,
  f.farmer_id, f.id,f.farm_name,o.farmer_id, o.f_id FROM farms_owners o inner join farms f on o.farmer_id=f.farmer_id inner join farms_ponds p on f.id=p.farm_id where f_id='16' ";

$resultpond = dbQuery($sqlpond);

$sqlfarm="SELECT f.farm_name,f.farm_county,f.farm_subcounty,f.farm_village,f.farm_landmark,f.keeps_records,f.farm_longitude,f.farm_latitude,f.record_keeping,f.records_kept,f.has_business_plan,f.business_plan_last_update,f.ponds_number,f.ponds_stocked,f.pond_sample_temperature,f.pond_sample_time,f.farmer_id, f.id,o.farmer_id, o.f_id FROM farms f inner join farms_owners o on f.farmer_id=o.farmer_id where f_id='7' ";

$resultfarm = dbQuery($sqlfarm);

$sql = "SELECT o.registration,o.recruitment_source,o.farmer_id, o.owner_name, o.owner_telephone,o.date_enrolled,
         o.owner_gender,o.owner_email,o.type_of_enterprise,o.finance_source,o.occupation,o.modified_by,o.date_modified,o.age,o.date_registered,r.id,r.type
        FROM farmer_reg_type r inner join farms_owners o on r.id = o.registration  where f_id='22'";
$result = dbQuery($sql);

?>
<div class="prepend-1 span-17">
<form action="processFarmer.php?action=add" method="post"  name="frmListUser" id="frmListUser">
<div class="col-md-12">
<div class="table-responsive">
  <h3>Farmer Profile</h3>
<table class="table table-striped table-bordered">

<thead>
  <tr>
   <td><b>ID</td>
   <td><b>Name</td>
   <td><b>Gender</td>
   <td><b>Age</td>
   <td><b>Telephone</td>
   <td><b>Email</td>
   <td><b>Registration</td>
   <td><b>Date registered</td>
   <td><b>Date Enrolled</td>
   <td><b>Occupation</td>
   <td><b>Source of Finance</td>
   <td><b>Type of Enterprise</td>
  </tr>
</thead>
<tbody>
<?php
while($row = dbFetchAssoc($result)) {
  extract($row);
  
  if ($i%2) {
    $class = 'row1';
  } else {
    $class = 'row2';
  }
  //echo $farmer_id;
?>

  <tr class="<?php echo $class; ?>">
   <td><?php echo $farmer_id; ?></td>
   <td><?php echo $owner_name; ?></td>
   <td><?php echo $owner_gender; ?></td>
   <td><?php echo $age; ?></td>
   <td><?php echo $owner_telephone; ?></td>
   <td><?php echo $owner_email; ?></td>
   <td><?php echo $type; ?></td>
   <td><?php echo $date_registered; ?></td>
   <td><?php echo $date_enrolled; ?></td>
   <td><?php echo $occupation; ?></td>
   <td><?php echo $finance_source; ?></td>
   <td><?php echo $type_of_enterprise; ?></td>
  </tr>
<?php
} // end while

?>
  
 </tbody>
</table>
</div>
</div>
</form>
</div>
<?php
if(dbAffectedRows() == 1){
while($row = dbFetchAssoc($resultfarm)){
extract($row);
?>
<div class="col-md-3">
<div class="prepend-1 span-17">
<form action="processFarmer.php?action=add" method="post"  name="frmListUser" id="frmListUser">
<div class="table-responsive">
  <h4>Farm information</h4>
<table class="table table-striped table-bordered">
<tbody>
  <tr><td><b>Name of Farm</td><td><?php echo $farm_name; ?></td></tr>
  <tr><td><b>County</td><td><?php echo $farm_county; ?></td></tr>
  <tr><td><b>Sub County</td><td><?php echo $farm_subcounty; ?></td></tr>
  <tr><td><b>Village</td><td><?php echo $farm_village; ?></td></tr>
  <tr><td><b>Total no. of ponds</td><td><?php echo $ponds_number; ?></td></tr>
  <tr><td><b>Ponds stocked</td><td><?php echo $ponds_stocked; ?></td></tr>
  <tr><td><b>Do they keep records?</td><td><?php echo $keeps_records;?></td></tr>
  <tr><td><b>Types of records Kept</td><td><?php echo $records_kept; ?></td></tr>
  <tr><td><b>How are records kept</td><td><?php echo $record_keeping; ?></td></tr>
  <tr><td><b>Do they have a Business plan?</td><td><?php echo $has_business_plan; ?></td></tr>
  <tr><td><b>Last time business plan was updated</td><td><?php echo $business_plan_last_update; ?></td></tr>
</tbody>
</table>
</div>
</form>
</div>
</div>
<?php
}//while
}else {
?>
<p>Profile for selected farmer does not exist.</p>
<div class="form-group " >
 <p align="center"> 
  &nbsp;&nbsp;<input name="btnCancel" type="button" id="btnCancel" class="button"  value="Back" onClick="window.location.href='view.php?v=Farmer';" class="box">  
 </p>
 </div>
<?php 
}

?>
<div class="col-md-9">
<div class="prepend-1 span-17">
<form action="processFarmer.php?action=add" method="post"  name="frmListUser" id="frmListUser">
<div class="table-responsive">
  <h4>Pond information</h4>
<table class="table table-striped table-bordered">

<thead>
  <tr>
   <td><b>ID</td>
   <td><b>Area(M2)</td>
   <td><b>Catfish stocked</td>
   <td><b>Current No.</td>
   <td><b>Stocking date</td>
   <td><b>Tilapia Stocked</td>
   <td><b>Current No.</td>
   <td><b>Stocking date</td>
  
  </tr>
</thead>
<tbody>
<?php
while($row = dbFetchAssoc($resultpond)) {
  extract($row);
  if ($i%2) {
    $class = 'row1';
  } else {
    $class = 'row2';
  }
 
?>

  <tr class="<?php echo $class; ?>">
   <td><?php echo $pond_number; ?></td>
   <td><?php echo $pond_area; ?></td>
   <td><?php echo $catfish_fingerling_stocked; ?></td>
   <td><?php echo $catfish_no; ?></td>
   <td><?php echo $catfish_stocking_date; ?></td>
   <td><?php echo $tilapia_fingerling_stocked; ?></td>
   <td><?php echo $tilapia_no; ?></td>
   <td><?php echo $tilapia_stocking_date; ?></td>
  </tr>
<?php
} // end while

?>
  
 </tbody>
</table>
</div>
</form>
</div>
</div>