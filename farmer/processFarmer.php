<?php
error_reporting(-1);
require_once '../library/config.php';
require_once '../library/functions.php';

checkUser();

$action = isset($_GET['action']) ? $_GET['action'] : '';

switch ($action) {
	
	case 'add' :
		addFarmer();
		break;
		
	case 'modify' :
		editprofile();
		break;
		
	case 'delete' :
		deleteHardware();
		break;
    

	default :
	    // if action is not defined or unknown
		// move to main user page
		header('Location: ../index.php');
}

/*
Function used to add entry in tbl_hardwares table.
*/
function addFarmer()
{
   $farmer_id=$_POST['farmer_id']; 
   $txtFname=$_POST['txtFname'];
   $txtLname=$_POST['txtLname'];
   $registration=$_POST['registration'];  
   $owner_telephone=$_POST['owner_telephone']; 
   $owner_gender=$_POST['owner_gender']; 
   $type_of_enterprise=$_POST['type_of_enterprise']; 
   $finance_source=$_POST['finance_source']; 
   $insert_finance = json_encode($finance_source);
   $occupation=$_POST['occupation'];
   $modified_by=$_SESSION['user_id']; 
   $age=$_POST['age']; 
   $owner_email=$_POST['owner_email'];
   
	$sql = "SELECT farmer_id
	        FROM farms_owners
			WHERE farmer_id = '$farmer_id'";
	$result = dbQuery($sql);
	
	if (dbNumRows($result) == 1) {
		header('Location: ../add.php?v=addfarmer&error=' . urlencode('Farmer ID exists. Assign another'));	
	} else {			
           $sql   = "INSERT INTO farms_owners (registration,recruitment_source,farmer_id, owner_name, owner_telephone,
         owner_gender,owner_email,type_of_enterprise,finance_source,occupation,modified_by,date_modified,age,date_registered)
          VALUES ('$registration','$modified_by','$farmer_id', '$txtFname $txtLname', '$owner_telephone', '$owner_gender', '$owner_email','$type_of_enterprise','$insert_finance','$occupation','$modified_by',NOW(),$age,NOW())";
	
		dbQuery($sql);
		header('Location: ../add.php?v=addfarm');	
	}
}

//Edit farmer profile

function editprofile()
{
	$farmer_id = $_POST["farmer_id"];
	$owner_name = $_POST["owner_name"];
    $registration = $_POST['type'];
	$date_registered = $_POST['date_registered'];
	$date_enrolled = $_POST['date_enrolled'];
	$age = $_POST['age'];
	$owner_email = $_POST['owner_email'];
	$owner_telephone = $_POST['owner_telephone'];
	$owner_gender = $_POST["owner_gender"];
    $occupation = $_POST['occupation'];
	$finance_source = $_POST['finance_source'];
	$insert_finance = json_encode($finance_source);
	$type_of_enterprise = $_POST['type_of_enterprise'];
	$modified_by=$_SESSION['user_id']; 
	//echo $id;
    //exit();
	//$utype = 'USER';
	//$did = (int)$_POST['did'];
	
		$sql   = "UPDATE farms_owners
		   SET  owner_name = '$owner_name',
			    registration='$registration',
			    date_registered='$date_registered',
			    age='$age',
			    date_enrolled='$date_enrolled',
			    owner_email='$owner_email',
			    owner_telephone='$owner_telephone',
			    owner_gender='$owner_gender',
			    occupation='$occupation',
			    finance_source='$insert_finance',
			    type_of_enterprise='$type_of_enterprise',
			    modified_by='$modified_by',
			    date_modified=NOW()


				WHERE farmer_id = '$farmer_id'";
	
		dbQuery($sql);
		header('Location: ../view.php?v=Farmer');	
	
}

/*
	Remove a Hardware
*/
function deleteHardware()
{
	if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
		$id = (int)$_GET['id'];
	} else {
		header('Location: index.php');
	}
	
	
	$sql = "DELETE FROM tbl_hardwares
	        WHERE id = $id";
	dbQuery($sql);
	
	header('Location: ../menu.php?v=HRWR');
}
?>