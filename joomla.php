<?php
define('_JEXEC', 1);
define('JPATH_BASE', dirname(__FILE__));
define('DS', DIRECTORY_SEPARATOR);

require_once(JPATH_BASE.DS.'inc'.DS.'defines.php');
require_once(JPATH_BASE.DS.'inc'.DS.'framework.php');

require_once 'vendor/autoload.php';

//$app = JFactory::getApplication('site');
//$app->initialise();